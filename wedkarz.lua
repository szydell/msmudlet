

--Nagle dostrzegasz, ze zanurzony w wodzie sznurek dlugiej lekkiej wedki napina sie!
-->zatnij rybe na wedce
--Wyciagasz zlapana rybe na powierzchnie.
--> zawies rybke na wedce
--Zawieszasz niewielka rybke na kotwiczce u dlugiej lekkiej wedki.
--> zarzuc wedke
--Bierzesz dluga lekka wedka zamach i zarzucasz ja daleko w wode.
--
--Sznurek dlugiej lekkiej wedki opada swobodnie na wode, zapewne zlapanej nan rybie udalo sie zerwac.
--Slyszysz suchy trzask i dostrzegasz, ze zdobycz zerwala sie z dlugiej lekkiej wedki, lamiac ja przy tym.
--
-- Sprawnie wyciagasz z wody siec rybacka, wyjmujac z niej pojedyncza zlapana rybe.
-- Sprawnie wyciagasz z wody siec rybacka, wyjmujac z niej dziesiec zlapanych ryb.
-- Sprawnie wyciagasz z wody siec rybacka, wyjmujac z niej szesc zlapanych ryb.

-- A na czym chcesz zawiesic przynete? Na kiju?


Msmudlet.wedkarz = Msmudlet.wedkarz or {
	triggery = {},
	rybki = 0,
	aliasy = {},
	status= "off",
	ver=1,
	szum = {"stan","i","zerknij"}
}

function Msmudlet.wedkarz:init()

	if self.aliasy.wedkuj then killAlias(self.aliasy.wedkuj) end
	-- alias do wlaczania
	self.aliasy.wedkuj = tempAlias("^/wedkuj (.*)$", function() Msmudlet.wedkarz:on(matches[2]) end)

	if self.aliasy.wedkarz then killAlias(self.aliasy.wedkarz) end
	-- help
	self.aliasy.wedkarz = tempAlias("^/wedkarz$", function() Msmudlet.wedkarz:help() end)
end

function Msmudlet.wedkarz:on(rybki)
	scripts:print_log("Rozpoczynam lowienie ryb. Poczatkowa ilosc rybek -> ".. rybki)
	scripts:print_log("Jezeli chcesz skonczyc lowic - zmien lokacje, lub wpisz /niewedkuj")
	self.status = "on"
	self.handler = scripts.event_register:force_register_event_handler(self.handler, "amapNewLocation", function(event, loc)
		
		Msmudlet.wedkarz:off()
  end)
	-- aliasy
	if self.aliasy.niewedkuj then killAlias(self.aliasy.niewedkuj) end
	self.aliasy.niewedkuj = tempAlias("^/niewedkuj$", function() Msmudlet.wedkarz:off() send("wyciagnij siec") end)

	-- triggery
	if self.triggery.bierze then killTrigger(self.triggery.bierze) end
	self.triggery.bierze = tempRegexTrigger("Nagle dostrzegasz, ze zanurzony w wodzie sznurek .* napina sie!$", function() alias_func_skrypty_misc_attack_beep_test() alias_func_skrypty_misc_attack_beep_test() Msmudlet.autko:kolejkuj("zatnij rybe", "normal", "zatnij rybe na wedce", 1, true) end)

	if self.triggery.siec then killTrigger(self.triggery.siec) end
	self.triggery.siec = tempRegexTrigger("Sprawnie wyciagasz z wody siec rybacka, wyjmujac z niej ([a-z]+) .*", function() Msmudlet.wedkarz:licz_rybki(matches[2]) end)

	if self.triggery.niemaryb then killTrigger(self.triggery.niemaryb) end
	self.triggery.niemaryb = tempTrigger("Sprawnie wyciagasz z wody siec rybacka, niestety nie zlapala sie w nia ani jedna ryba.", function() Msmudlet.wedkarz:licz_rybki(0) end)

	if self.triggery.zerwane then killTrigger(self.triggery.zerwane) end
	self.triggery.zerwane = tempRegexTrigger("Sznurek .* wedki opada swobodnie na wode, zapewne zlapanej nan rybie udalo sie zerwac.", function() alias_func_skrypty_misc_attack_beep_test() alias_func_skrypty_misc_attack_beep_test()  Msmudlet.autko:kolejkuj("wyciagasz po zerwaniu", "normal", function() Msmudlet.wedkarz:wyciagasz(true) end, 1, true) end)

	if self.triggery.zlamane then killTrigger(self.triggery.zlamane) end
	self.triggery.zlamane = tempRegexTrigger("Slyszysz suchy trzask i dostrzegasz, ze zdobycz zerwala sie z .* wedki, lamiac ja przy tym.", function() alias_func_skrypty_misc_attack_beep_test() alias_func_skrypty_misc_attack_beep_test()  Msmudlet.autko:kolejkuj("zlamana wedka", "normal", function() Msmudlet.wedkarz:off() end, 1, true) end) 

	if self.triggery.wyciagasz then killTrigger(self.triggery.wyciagasz) end
	self.triggery.wyciagasz = tempTrigger("Wyciagasz zlapana rybe na powierzchnie.", function() alias_func_skrypty_misc_attack_beep_test() Msmudlet.autko:kolejkuj("wyciagasz zlowione", "regular", function() Msmudlet.wedkarz:wyciagasz(false) end, 1, true) end)

	if self.triggery.zawieszasz then killTrigger(self.triggery.zawieszasz) end
	self.triggery.zawieszasz = tempRegexTrigger("Zawieszasz niewielka rybke na kotwiczce u .* wedki.$", function() self.rybki = self.rybki - 1 scripts:print_log("\nStan rybek: ".. self.rybki.."\n") end)

	if self.triggery.brak_kotwiczek then killTrigger(self.triggery.brak_kotwiczek) end
	self.triggery.brak_kotwiczek = tempRegexTrigger("A na czym chcesz zawiesic przynete? Na kiju?", function() scripts:print_log("Brak kotwiczek. Zaraz zakoncze wedkarza.") alias_func_skrypty_misc_attack_beep_test() Msmudlet.autko:kolejkuj("wyciagnij siec", "normal", function() send("wyciagnij siec") Msmudlet.wedkarz:off() end, 1, true) end)

	if self.triggery.za_ciezka then killTrigger(self.triggery.za_ciezka) end
	self.triggery.za_ciezka = tempRegexTrigger(".* ryba jest zbyt ciezka.", function() alias_func_skrypty_misc_attack_beep_test() alias_func_skrypty_misc_attack_beep_test() scripts:print_log("Przeciazenie. Zaraz zakoncze wedkarza.") Msmudlet.autko:kolejkuj("wyciagnij siec i wedke", "normal", function() sendAll("wyciagnij siec","wyciagnij wedke") Msmudlet.wedkarz:off() end, 1, true) end)

	self.rybki = tonumber(rybki)
	if tonumber(self.rybki) > 0 then
		send("zawies kotwiczke na wedce;zawies rybke na wedce;zarzuc wedke")
	else
		Msmudlet.autko:kolejkuj("wyciagasz zlowione", "slow", function() Msmudlet.wedkarz:wyciagasz(false) end, 1, true)
	end
	
	if tonumber(self.rybki) < 3 then
		send("zarzuc siec")
	end

	Msmudlet.autko:kolejkuj("ogarnij_siec", "slow", function() Msmudlet.wedkarz:ogarnij_siec() end, 1, true)
end

function Msmudlet.wedkarz:wyciagasz(zerwane)
	if not zerwane then
		local czypakowac = math.random(10)
		if czypakowac > 4 then
			Msmudlet.autko:kolejkuj("zapakuj ryby", "regular", function() scripts.inv:put_into_bag({"ryby"},"other", 1) end, 1, true)
		end
	end

	if self.rybki > 0 then
		send("zawies kotwiczke na wedce;zawies rybke na wedce;zarzuc wedke")
	else
		Msmudlet.autko:kolejkuj("wyciagasz zlowione", "slow", function() Msmudlet.wedkarz:wyciagasz(false) end, 1, true)
	end
  -- szum
	local ile_for = math.random(10)
	if ile_for < 4 then
		for i=1,ile_for do
			Msmudlet.autko:kolejkuj("szum wedkarza", "regular", function() send(Msmudlet.wedkarz.szum[math.random(table.size(Msmudlet.wedkarz.szum))]) end, 4 , false)
		end
	end
end

function Msmudlet.wedkarz:licz_rybki(ile_rybek)
	if ile_rybek == "pojedyncza" then
		ile_rybek = 1
	elseif tonumber(ile_rybek) == 0 then
		ile_rybek = 0
	else
		ile_rybek = scripts.string_to_liczebnik[ile_rybek]
	end
	self.rybki = self.rybki + tonumber(ile_rybek)

	scripts:print_log("Stan rybek: ".. self.rybki)

end


function Msmudlet.wedkarz:ogarnij_siec()
	if self.status == "off" then
		return
	end
	if ( tonumber(self.rybki) < 4 ) then
		sendAll("wyciagnij siec","zarzuc siec")
	end
	Msmudlet.autko:kolejkuj("ogarnij_siec", "slow", function() Msmudlet.wedkarz:ogarnij_siec() end, 1, true)
end


function Msmudlet.wedkarz:off()
	scripts:print_log("Koncze lowic ryby.")
	self.status = "off"
	scripts.event_register:kill_event_handler(self.handler)
	self.handler = nil
	self.rybki = 0
	if table.size(self.triggery) > 0 then
		for k, v in pairs(self.triggery) do
			killTrigger(v)
			Msmudlet.wedkarz.triggery[k]=nil
		end
	end
end


function Msmudlet.wedkarz:help()
    cecho("+-------------------------- <green>Arkadia Wedkarz, ver " .. string.sub(self.ver .. "   ", 0, 5) .. "<grey> ---------------------------+\n")
    cecho("|                                                                                |\n")
    cecho("| <light_slate_blue>/wedkarz<grey> - ta pomoc.                                                           |\n")
    cecho("| <light_slate_blue>/wedkuj X<grey> - rozpocznij wedkowanie, majac X rybek                               |\n")
    cecho("|                                                                                |\n")
		cecho("|   Do poprawnego dzialania automatu do lowienia, musisz miec przy sobie         |\n")
		cecho("| wedke, siec, kotwiczki i 0 lub wiecej rybek.                                   |\n")
		cecho("|                                                                                |\n")
		cecho("+--------------------------------------------------------------------------------+\n")
end

Msmudlet.wedkarz:init()

