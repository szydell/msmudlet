# Instrukcje dla projektu msmudlet

## Dokumentacja
- Dokumentacja Mudlet: https://wiki.mudlet.org/w/Manual:Contents
- Dokumentacja funkcji sieciowych: https://wiki.mudlet.org/w/Manual:Networking_Functions
- Dokumentacja funkcji: https://wiki.mudlet.org/w/Manual:Functions
- Dokumentacja Geyser: https://wiki.mudlet.org/w/Manual:Geyser

## Struktura projektu
- `init.lua` - główny plik inicjalizujący, wczytuje wszystkie komponenty
- `autko.lua` - implementuje system automatów wspierających walkę, śledzenie, zbieranie przedmiotów i inne akcje
- `dane.lua` - przechowuje dane konfiguracyjne i stany systemu
- `devpack.sh` - skrypt pomocniczy do developmentu
- `dupsy.lua` - zarządza systemem duplikatów przedmiotów
- `eksploracja.lua` - funkcje wspomagające eksplorację świata gry
- `events.lua` - system obsługi i reagowania na zdarzenia w grze
- `exptarget.lua` - system celów do zdobywania doświadczenia
- `gcs.lua` - zarządza komunikacją z platformą Gnome Communication Services, odpowiada za wysyłanie i odbieranie wiadomości między graczami
- `grabiezca.lua` - system automatycznego zbierania przedmiotów
- `herbs.lua` - system zbierania i identyfikacji ziół
- `keybinds.lua` - definicje skrótów klawiszowych
- `keypad.lua` - obsługa klawiatury numerycznej
- `klucznicy.lua` - system zarządzania kluczami i zamkami
- `kowal.lua` - system naprawy i ulepszania przedmiotów u kowala
- `mapper.lua` - zawiera funkcjonalność związaną z mapowaniem i poruszaniem się po świecie gry
- `mapy.lua` - przechowuje dane map i lokacji
- `obexp.lua` - system liczenia doświadczenia
- `oslib.lua` - biblioteka funkcji systemowych
- `pomoc.lua` - system pomocy i dokumentacji
- `postac.lua` - zarządzanie statystykami i cechami postaci
- `siedliszcze.lua` - system zarządzania bazą/mieszkaniem
- `sledzie.lua` - system śledzenia celów i NPCów
- `sound.lua` - system dźwięków i powiadomień
- `toolbar.lua` - zarządza paskiem narzędzi w interfejsie, zawiera przyciski funkcyjne i wskaźniki statusu, w tym lampkę GCS
- `transport.lua` - system automatycznego transportu i podróży
- `triggers.lua` - definicje wyzwalaczy (triggerów) reagujących na komunikaty z gry
- `updater.lua` - system aktualizacji skryptów
- `utils.lua` - zbiór funkcji pomocniczych używanych w całym projekcie
- `version.lua` - informacje o wersji skryptów, podbijane w trakcie deploymentu za pomocą devpack.sh
- `wedkarz.lua` - system automatycznego wędkowania
- `wiedza.lua` - system zarządzania wiedzą o świecie gry
- `wybierz.lua` - system wyboru celów i interakcji
- `zaslony.lua` - system automatycznej obrony i zasłaniania

## Konwencje nazewnictwa
- Prefiks `Msmudlet` dla wszystkich modułów
- Funkcje w notacji wielbłądziej (camelCase)
- Moduły organizowane w zagnieżdżone tabele (np. `Msmudlet.toolbar`, `Msmudlet.gcs`)
- Zmienne prywatne z podkreśleniem na początku (np. `_privateVar`)
- Stałe pisane wielkimi literami (np. `DEFAULT_TIMEOUT`)

## Konwencje kodowania
- Każdy moduł powinien przechowywać identyfikatory swoich zasobów (triggerów, aliasów, handlerów, timerów) w dedykowanych tablicach
- Przykład: `Msmudlet.gcs.aliasy = {}`, `Msmudlet.gcs.handlers = {}`
- Każdy moduł powinien implementować funkcję `cleanupResources()` do usuwania swoich zasobów
- Przed ponowną inicjalizacją modułu należy zawsze wywołać `cleanupResources()`
- Przy tworzeniu nowych zasobów, zawsze przechowuj ich ID: `self.aliasy.nazwa = tempAlias(...)`
- Dzięki temu unikamy duplikacji przy wielokrotnym ładowaniu skryptów
- Przykładowa implementacja funkcji czyszczącej:

```lua
function Msmudlet.modul:cleanupResources()
    -- Usuń wszystkie aliasy
    for name, alias_id in pairs(self.aliasy) do
        if alias_id then
            killAlias(alias_id)
            self.aliasy[name] = nil
        end
    end

    -- Usuń wszystkie handlery zdarzeń
    for name, handler_id in pairs(self.handlers) do
        if handler_id then
            killAnonymousEventHandler(handler_id)
            self.handlers[name] = nil
        end
    end

    -- Usuń wszystkie triggery
    for name, trigger_id in pairs(self.triggers) do
        if handler_id then
            killTrigger(trigger_id)
            self.triggers[name] = nil
        end
    end

    -- Usuń wszystkie timery
    for name, timer_id in pairs(self.timers) do
        if timer_id then
            killTimer(timer_id)
            self.timers[name] = nil
        end
    end
end
```
- Komunikaty z danego modułu wyświetlamy cecho zgodnie z formatem: "<CadetBlue>(sgw moduł)<color>: wiadomość"  

### Przestrzeń nazw:  

Wszystkie moduły są umieszczane w przestrzeni nazw Msmudlet (np. Msmudlet.dane, Msmudlet.gcs)
Każdy moduł jest inicjowany wzorcem Msmudlet.modul = Msmudlet.modul or {}

### Zarządzanie zasobami:

Każdy moduł przechowuje swoje zasoby (aliasy, triggery, handlery, timery) w dedykowanych tablicach
Zasoby są nazywane: self.aliasy, self.triggers, self.handlers, self.timers
Moduł powinien zawierać funkcję cleanupResources() do prawidłowego usuwania swoich zasobów

### Format komunikatów:

Komunikaty są wyświetlane w formacie: <CadetBlue>(sgw nazwa_modułu)<kolor>: Treść wiadomości
Kolor komunikatu wskazuje jego typ:
<green> - sukces, informacja pozytywna
<tomato> lub <red> - błąd, ostrzeżenie
<grey> - informacja neutralna, debug
<white> - wyróżnienia w tekście (nazwy, wartości)

### Obsługa błędów:

Komunikaty błędów zawierają instrukcje jak zgłosić problem (https://gitlab.com/szydell/msmudlet/-/issues/new)
Treść błędu jest wyświetlana w kolorze białym wewnątrz czerwonego komunikatu

### Asynchroniczność:

Funkcje HTTP/sieciowe są obsługiwane asynchronicznie poprzez handlery zdarzeń
Handlery są rejestrowane przed wykonaniem operacji HTTP
Stare handlery są usuwane przed zarejestrowaniem nowych

### Inicjalizacja i konfiguracja:

Każdy moduł ma funkcję init() wywoływaną podczas ładowania lub restartu
Jeśli moduł wymaga konfiguracji, posiada dodatkową funkcję setup()

### Aliasy:

Aliasy do funkcji modułu mają prefix nazwy modułu (np. /gcs_setup, /dane_sprawdz)
Aliasy tworzone są zawsze za pomocą tempAlias() i są przechowywane w tablicy self.aliasy

### Pomocnicze funkcje wewnętrzne:

Funkcje pomocnicze mają nazwy zaczynające się od czasownika (np. getPlayerIdentifier())
Funkcje dostępne z zewnątrz mają deskryptywne nazwy (np. reportDiscovery())

### Zapisywanie danych:

Trwałe dane przechowywane są w formacie JSON
Operacje na plikach zawsze sprawdzają błędy otwarcia i parsowania

### Rekurencja i timery:

Funkcje rekurencyjne (np. polling) używają timerów o krótkim czasie (0.1-0.5s)
Ponowne próby po błędach używają dłuższych czasów (5-10s)

## Najważniejsze funkcje Mudlet

### Wyświetlanie tekstu i formatowanie
- `echo()` - podstawowa funkcja wyświetlająca tekst
- `cecho()` - wyświetlanie tekstu z kolorami (format: <kolor>tekst)
- `decho()` - wyświetlanie tekstu z kolorami (format RGB)
- `hecho()` - wyświetlanie tekstu z kolorami (format hex)
- `setUnderline()`, `setBold()`, `setItalics()` - formatowanie tekstu
- `resetFormat()` - resetowanie formatowania
- `selectCurrentLine()`, `selectString()` - zaznaczanie tekstu
- `insertLink()`, `setLink()` - tworzenie klikalnych linków

### Wyzwalacze (Triggers)
- `tempTrigger()` - tworzy tymczasowy wyzwalacz
- `tempRegexTrigger()` - tworzy tymczasowy wyzwalacz bazujący na wyrażeniu regularnym
- `permTrigger()`, `permRegexTrigger()` - tworzy trwałe wyzwalacze
- `killTrigger()` - usuwa wyzwalacz
- `disableTrigger()`, `enableTrigger()` - wyłącza/włącza wyzwalacz

### Aliasy
- `tempAlias()` - tworzy tymczasowy alias
- `tempRegexAlias()` - tworzy tymczasowy alias bazujący na wyrażeniu regularnym
- `killAlias()` - usuwa alias
- `disableAlias()`, `enableAlias()` - wyłącza/włącza alias

### Timery i opóźnienia
- `tempTimer()` - tworzy tymczasowy timer
- `killTimer()` - usuwa timer
- `send()` - wysyła komendę do MUD-a
- `expandAlias()` - wykonuje komendę jakby była wpisana przez użytkownika

### Zdarzenia i Handlery
- `registerAnonymousEventHandler()` - rejestruje handler zdarzenia
- `killAnonymousEventHandler()` - usuwa handler zdarzenia
- `raiseEvent()` - wyzwala zdarzenie

### Przydatne narzędzia
- `getTime()` - zwraca aktualny czas jako tabelę
- `getTimestamp()` - zwraca aktualny znacznik czasu
- `table.save()`, `table.load()` - zapisuje/wczytuje tabelę do/z pliku
- `yajl.to_string()`, `yajl.to_value()` - konwersja między tablicami a JSON

### GMCP (Game Master Client Protocol)
- `registerAnonymousGMCPEvent()` - rejestruje handler dla zdarzeń GMCP
- `killAnonymousGMCPEvent()` - usuwa handler zdarzeń GMCP

### Geyser (GUI)
- `Geyser.Container:new()` - tworzy nowy kontener
- `Geyser.Label:new()` - tworzy nową etykietę
- `Geyser.MiniConsole:new()` - tworzy nową minikonsolę
- `setStyleSheet()` - ustawia styl CSS dla elementu
- `setFgColor()`, `setBgColor()` - ustawia kolory elementu


# Dokumentacja api serwera GCS:
# GCS API Documentation

The GCS server provides several endpoints for authentication, long polling, sending messages, and managing discoveries. Below is a detailed description of each endpoint.

## Authentication Endpoint

**URL:** `/api/auth`  
**Method:** `POST`  
**Description:** Authenticates a user using their GitLab token and generates a session token.

**Request Body:**
```json
{
  "gitlab_token": "string",
  "character_name": "string"
}
```

**Response:**
```json
{
  "token": "string"
}
```

## Long Poll Endpoint

**URL:** `/api/poll`  
**Method:** `GET`  
**Description:** Long polls for events. The client must include the `X-Player-ID` header with their character name.

**Headers:**
```
X-Player-ID: string
```

**Response:**
```json
{
  "type": "string",
  "sender": "string",
  "content": "string",
  "timestamp": "integer",
  "message": "string"
}
```

## Send Message Endpoint

**URL:** `/api/send`  
**Method:** `POST`  
**Description:** Sends a message to specified recipients.

**Request Body:**
```json
{
  "sender": "string",
  "recipients": ["string"],
  "message": "string"
}
```

**Response:**
```json
{
  "success": "boolean",
  "error": "string"
}
```

## Discovery Endpoint

**URL:** `/api/discovery`  
**Method:** `POST`  
**Description:** Reports a discovery. The client must include the `X-Session-Token` header with their session token.

**Headers:**
```
X-Session-Token: string
```

**Request Body:**
```json
{
  "apo_id": "string",
  "location_id": "string",
  "description": "string"
}
```

**Response:**
```json
{
  "id": "string",
  "message": "string"
}
```

## Get Discoveries Endpoint

**URL:** `/api/discoveries`  
**Method:** `GET`  
**Description:** Retrieves discoveries for a specific ApoID.

**Query Parameters:**
```
apo_id: string
```

**Response:**
```json
[
  {
    "id": "string",
    "character_name": "string",
    "apo_id": "string",
    "location_id": "string",
    "description": "string",
    "created_at": "string"
  }
]
```
