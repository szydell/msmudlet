Msmudlet.exptarget = Msmudlet.exptarget or {
    triggers = {},
    aliases = {},
    handlers = {},
    ignore = 0,
    status = {}
}

function Msmudlet.exptarget:update(amount)
    if tonumber(self.status.target) < 1 then
        return
    end
    if self.status.done == nil then
        self.status.done = 0
    end

    self.status.done = self.status.done + amount

    Msmudlet.exptarget:save()
    Msmudlet.exptarget:show()
end

function Msmudlet.exptarget:update_target(amount)

    self.status.done = 0
    self.status.target = amount

    Msmudlet.exptarget:save()
    Msmudlet.exptarget:show()
end

function Msmudlet.exptarget:update_all(target, done)

    self.status.done = done
    self.status.target = target

    Msmudlet.exptarget:save()
    Msmudlet.exptarget:show()
end

function Msmudlet.exptarget:show()
    if self.status.target ~= nil and self.status.done ~= nil and tonumber(self.status.target) > 0 then

        local procent = self.status.done / self.status.target * 100

        cecho("\n<CadetBlue>(msmudlet)<yellow> Exptarget: " .. self.status.done .. "/" .. self.status.target .. " (" ..
                  string.format("%3.2f", procent) .. "%)\n\n")

    else
        cecho("\n<CadetBlue>(msmudlet)<yellow> Brak zapisów o celu do wyexpienia. Ustaw przez /exptarget <ile>")
    end
end

function Msmudlet.exptarget:save()
    scripts.state_store:set("msmudlet_exptarget", self.status)
end

function Msmudlet.exptarget:load()
    local exptarget = scripts.state_store:get("msmudlet_exptarget")
    if exptarget then
        self.status = exptarget
    else
        self.status.target = 0
        self.status.done = 0
        Msmudlet.exptarget:save()
    end
end

function Msmudlet.exptarget:init()

    Msmudlet.exptarget:load()

    if Msmudlet.exptarget.handlers["improve_level"] then
        killAnonymousEventHandler(Msmudlet.exptarget.handlers["improve_level"])
    end

    self.handlers["improve_level"] = scripts.event_register:force_register_event_handler(self.handlers["improve_level"],
        "improveLevelGained", function(_,amountGained)
			Msmudlet.exptarget:update(amountGained)
        end)

    if self.triggers["dodane_z_formy"] then
        killTrigger(self.triggers["dodane_z_formy"])
    end
    self.triggers["dodane_z_formy"] = tempTrigger(
        "Twoja wysoka forma pozwala ci na skokowy rozwoj cech fizycznych i mentalnych.", function()
            if scripts.character.state.form ~= 3 then
                Msmudlet.exptarget:update(1)
            end
        end)

    if self.aliases["cechy"] then
        killAlias(self.aliases["cechy"])
    end

    if self.aliases["exptarget_status"] then
        killAlias(self.aliases["exptarget_status"])
    end
    self.aliases["exptarget_status"] = tempAlias("^/exptarget$", function()
        Msmudlet.exptarget:show()
    end)

    if self.aliases["exptarget_set"] then
        killAlias(self.aliases["exptarget_set"])
    end
    self.aliases["exptarget_set"] = tempAlias("^/exptarget (\\d+)$", function()
        Msmudlet.exptarget:update_target(matches[2])
    end)

    if self.aliases["exptarget_set_all"] then
        killAlias(self.aliases["exptarget_set_all"])
    end
    self.aliases["exptarget_set_all"] = tempAlias("^/exptarget (\\d+) (\\d+)$", function()
        Msmudlet.exptarget:update_all(matches[2], matches[3])
    end)
end

Msmudlet.exptarget:init()
