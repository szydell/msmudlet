Oto pogrubione strzałki w prawo w UTF-8:

- ➡️ (U+27A1) - Czarna pogrubiona strzałka w prawo
- ➜ (U+279C) - Gruba strzałka w prawo skręcająca 
- ⇒ (U+21D2) - Podwójna gruba strzałka w prawo (implikacja)
- ▶ (U+25B6) - Czarny trójkąt wskazujący w prawo
- ⟹ (U+27F9) - Długa podwójna strzałka w prawo
- ⟾ (U+27FE) - Długa pogrubiona strzałka w prawo

Najbardziej wyrazista i uniwersalna pogrubiona strzałka to: ➡️