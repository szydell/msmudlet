Msmudlet.dane = Msmudlet.dane or {
  data_dir = getMudletHomeDir() .. "/sgw-dane",
  data_file = "sgw-dane.zip",
  url = "https://git.sgw.equipment/api/v4/projects/sterowiec%2Fsgw-dane/releases",
  handlers = {},
  version = "",
  header = "",
  aliasy = {}
}




function Msmudlet.dane:check(reinstall)
  if self.header == "" then
    return
  end

  if self.handlers.error then killAnonymousEventHandler(self.handlers.error) end
  if self.handlers.done then killAnonymousEventHandler(self.handlers.done) end

  self.handlers.done = registerAnonymousEventHandler('sysGetHttpDone', function(event, rurl, response)
    if rurl == Msmudlet.dane.url then Msmudlet.dane:handle(response, reinstall) else return true end
  end, true)

  self.handlers.error = registerAnonymousEventHandler('sysGetHttpError', function(event, response, rurl)
    if rurl == Msmudlet.dane.url then
      if response == "Host requires authentication" then
        cecho(
          "<red> Aby ściągnąć sgw-dane, musisz mieć zainicjalizowany dostęp do danych. Sprawdź /msmudlet_pomoc i stronę https://gitlab.com/szydell/msmudlet/-/wikis/SGW%20ONLY.\n")
      else
        cecho(
          "<red> Wystapił błąd przy ściąganiu listy dostępnych danych. Wraz z poniższym reponse zgłoś go tutaj: https://gitlab.com/szydell/msmudlet/-/issues/new\n")
        echo("Response:\n" .. response .. "\n--- end of response---\n")
      end
    else
      return true
    end
  end, true)

  getHTTP(self.url, Msmudlet.dane.header)
end

function Msmudlet.dane:handle(releases, reinstall)
  if self.handlers.error then killAnonymousEventHandler(self.handlers.error) end
  if self.handlers.done then killAnonymousEventHandler(self.handlers.done) end

  if releases then
    local content = yajl.to_value(releases)
    if content == nil then
      return
    end
    local tag = content[1].tag_name
    if reinstall then
      Msmudlet.dane:update(tag, content[1].assets.links[1].direct_asset_url)
      return
    end
    if Msmudlet.dane.version ~= tag then
      echo("\n")
      cecho("<CadetBlue>(sgw dane)<tomato>: Dostępna jest aktualizacja <white>DANYCH<tomato>. Kliknij ")
      cechoLink("<green>tutaj",
        function() Msmudlet.dane:update(tag, content[1].assets.links[1].direct_asset_url) end, "Aktualizuj", true)
      cecho(" <tomato>aby pobrać wersję <white>" .. tag .. "<reset>.\n")
      cecho("<white>Zmiany:<reset>\n")
      local i = 1
      while content[i].tag_name ~= Msmudlet.dane.version do
        cecho("<white> " .. content[i].tag_name .. "<reset>\n" .. content[i].description .. "\n")
        i = i + 1
      end
      cecho("<white>---<reset>\n")
    else
      cecho("<CadetBlue>(sgw dane)<white> Masz ściągniętą najnowszą wersję danych SGW (" ..
        Msmudlet.dane.version .. ")\n")
    end
  end
end

function Msmudlet.dane:update(tag, url)
  if self.handlers.done_upd then killAnonymousEventHandler(self.handlers.done_upd) end

  self.handlers.done_upd = registerAnonymousEventHandler("sysDownloadDone", function(_, file)
    if file ~= Msmudlet.dane.data_dir .. "/" .. Msmudlet.dane.data_file then
      return true
    end
    Msmudlet.dane:handle_update(file)
  end, true)


  if not lfs.isdir(self.data_dir) then
    lfs.mkdir(self.data_dir)
  end

  downloadFile(self.data_dir .. "/" .. self.data_file, url)
  cecho("<CadetBlue>(sgw dane)<grey> Pobieram dane w wersji " .. tag .. " (" .. url .. ")\n")
end

function Msmudlet.dane:handle_update(zip)
  cecho("<CadetBlue>(sgw dane)<grey> Plik z danymi został pobrany.\n")
  if self.handlers.done_upd then killAnonymousEventHandler(self.handlers.done_upd) end
  if self.handlers.unzipSuccessHandler then killAnonymousEventHandler(self.handlers.unzipSuccessHandler) end
  if self.handlers.unzipFailureHandler then killAnonymousEventHandler(self.handlers.unzipFailureHandler) end
  self.handlers.unzipSuccessHandler = registerAnonymousEventHandler("sysUnzipDone", "Msmudlet.dane:handle_zip")
  self.handlers.unzipFailureHandler = registerAnonymousEventHandler("sysUnzipError", "Msmudlet.dane:handle_zip")
  unzipAsync(zip, self.data_dir)
end

function Msmudlet.dane:handle_zip(event, ...)
  local args = { ... }
  local zipName = args[1]
  local unzipLocation = args[2]
  if zipName ~= self.data_dir .. "/" .. self.data_file then
    return
  end
  if event == "sysUnzipDone" then
    local vf, s, contents = io.open(self.data_dir .. "/version.json")
    if vf then
      local tmp = yajl.to_value(vf:read("*a"))
      if tmp == nil then
        cecho(string.format(
          "<CadetBlue>(sgw dane)<grey> <firebrick>Odczytanie pliku z wersją danych nie powiodło się! Zgłoś błąd (https://gitlab.com/szydell/msmudlet/-/issues/new)."))
        return
      end
      self.version = tmp.version
      io.close(vf)
    else
      cecho(string.format(
        "<CadetBlue>(sgw dane)<grey> <firebrick>Odczytanie pliku z wersją danych nie powiodło się! Zgłoś błąd (https://gitlab.com/szydell/msmudlet/-/issues/new). Podaj w mim też to info -> %s to %s\n",
        zipName, unzipLocation))
    end
    cecho("<CadetBlue>(sgw dane)<grey> Plik z danymi w wersji " .. self.version .. " został poprawnie rozpakowany.\n")
    -- Reinicjalizuj dane
    Msmudlet.dane:init()

    os.remove(zipName) -- kasujemy archiwum z danymi.
  elseif event == "sysUnzipError" then
    cecho(string.format(
      "<CadetBlue>(sgw dane)<grey> <firebrick>Rozpakowywanie danych nie powiodło się! Zgłoś błąd (https://gitlab.com/szydell/msmudlet/-/issues/new). Podaj w mim też to info -> Unzip %s to %s\n",
      zipName, unzipLocation))
  end
end

function Msmudlet.dane:loadSgwEnvColors()
  local secf = io.open(self.data_dir .. "/sgwEnvColors.json")
  if secf then
    local tmp = yajl.to_value(secf:read("*a"))
    io.close(secf)
    if tmp then
      for k, v in pairs(tmp.sgwEnvColors) do
        setCustomEnvColor(k, v[1], v[2], v[3], v[4])
      end
    end
  end
end

function Msmudlet.dane:loadSgwCustom()
  local scf = io.open(getMudletHomeDir() .. "/sgw_custom.json")
  if scf then
    local tmp = yajl.to_value(scf:read("*a"))
    io.close(scf)
    if tmp then
      cecho("<CadetBlue>(sgw dane)<grey> Wykryto plik z customowymi ustawieniami. Ładuję.\n")
      Msmudlet.custom = tmp
    end
  end
end

function Msmudlet.dane:init()
  local vf = io.open(self.data_dir .. "/version.json")
  if vf then
    local tmp = yajl.to_value(vf:read("*a"))
    if tmp == nil then
      cecho(string.format(
        "<CadetBlue>(sgw dane)<grey> <firebrick>Odczytanie pliku z wersją danych nie powiodło się! Zgłoś błąd (https://gitlab.com/szydell/msmudlet/-/issues/new)."))
      return
    end
    self.version = tmp.version
    io.close(vf)
  else
    self.version = "1.0.0"
  end


  local f = io.open(getMudletHomeDir() .. "/sgw_mapper.json", "r")
  if f ~= nil then
    io.close(f)
    local ok, message = os.rename(getMudletHomeDir() .. "/sgw_mapper.json", getMudletHomeDir() .. "/sgw.json")
    if ok then
      cecho("\n\n<CadetBlue>(sgw dane)<yellow>: Poprawnie wykonano migrację pliku z tokenem.\n\n")
    else
      cecho("\n\n<CadetBlue>(sgw dane)<red>: Migracja pliku z tokenem zakończona błędem:" .. message .. "\n\n")
    end
  end

  local sgw_config = io.open(getMudletHomeDir() .. "/sgw.json")
  if sgw_config then
    local config_raw = sgw_config:read("*a")
    if config_raw ~= "" then
      local tmp = yajl.to_value(config_raw)
      if tmp and tmp.token then
        self.header = { ['PRIVATE-TOKEN'] = tmp.token }
      else
        self.header = ""
      end
    end
    sgw_config:close()
  else
    self.version = "1.0.0"
    self.header = ""
  end

  herbs.data_file_path = self.data_dir .. "/herbs_data.json"
  Msmudlet.herbs:init_data()

  if self.aliasy.sprawdz then killAlias(self.aliasy.sprawdz) end
  self.aliasy.sprawdz = tempAlias("^/sgw_dane_sprawdz$", function()
    Msmudlet.dane:check(false)
  end)
  if self.aliasy.pobierz then killAlias(self.aliasy.pobierz) end
  self.aliasy.pobierz = tempAlias("^/sgw_dane_pobierz$", function()
    Msmudlet.dane:check(true)
  end)
  if self.aliasy.inicjalizuj then killAlias(self.aliasy.inicjalizuj) end
  self.aliasy.inicjalizuj = tempAlias("^/sgw_inicjalizuj (.+)$", function()
    local token = matches[2]
    Msmudlet.dane:initialize(token)
  end)

  Msmudlet.keypad:init()           -- init keypad data
  Msmudlet.eksploracja:load()      -- reload dane z eksploracji
  scripts.misc.knowledge:init()    -- reload dane dotyczące wiedzy z książek/bibliotek
  Msmudlet.dane:loadSgwEnvColors() -- reload danych dotyczących kolorów
  Msmudlet.dane:loadSgwCustom()    -- reload customowych ustawień
end

tempTimer(7, function() Msmudlet.dane:init() end)

function Msmudlet.dane:start()
  Msmudlet.dane:check(false)
end

tempTimer(10, function() Msmudlet.dane:start() end)


function Msmudlet.dane:initialize(token)
  local file = io.open(getMudletHomeDir() .. "/sgw.json", "w")
  if file then
    file:write(yajl.to_string({ token = token }))
    file:close()
    self:init()
    cecho("<CadetBlue>(sgw dane)<green> Inicjalizacja dostępu do danych SGW ONLY zakończona. Zrestartuj Mudleta.")
  else
    cecho(
      "<firebrick>Inicjalizowanie dostępu do danych SGW ONLY nie powiodło się! Zgłoś błąd (https://gitlab.com/szydell/msmudlet/-/issues/new).")
  end
end

-- nadpisz funkcje herbs_data_downloaded w celu odblokowania odswiezania danych z repozytorum arkadia-data

---@diagnostic disable-next-line: lowercase-global
function herbs_data_downloaded(resume_coroutine_id, decoded_data)
  coroutine.resume(resume_coroutine_id)
  coroutine.resume(scripts.utils.download_all_data_coroutine_id)
end
