Msmudlet.mapy = Msmudlet.mapy or {
	follow = {},
	blockers = {}
}

function Msmudlet.mapy:set_bind(room_id, bind_str)
	local room_to_set = amap.db:get_room_id(room_id)

	if not room_to_set or not bind_str then
		error("Wrong input")
	end

	local binds = string.split(bind_str, "#")
	local bind_printable = ""

	---@diagnostic disable-next-line: param-type-mismatch
	for k, v in pairs(binds) do
		local bind = string.split(v, "*")
		if table.size(bind) > 2 or bind == nil then
			amap:print_log("Cos nie tak w parsowaniu")
			error("Wrong input")
		end

		bind_printable = bind_printable .. bind[1] .. ";"
	end

	setRoomUserData(room_to_set, "bind", bind_str)
	setRoomUserData(room_to_set, "bind_printable", bind_printable)
end

function Msmudlet.mapy:add_follow(room_id, from_link, to_link)
	local room_to_set = amap.db:get_room_id(room_id)

	if not room_to_set or not from_link or not to_link then
		error("Wrong input")
	end

	local curr_data = getRoomUserData(room_to_set, "team_follow_link")
	if curr_data ~= "" then
		local new_data = curr_data .. "#" .. from_link .. "*" .. to_link
		setRoomUserData(room_to_set, "team_follow_link", new_data)
	else
		setRoomUserData(room_to_set, "team_follow_link", from_link .. "*" .. to_link)
	end
end

function Msmudlet.mapy:startlocki()
	-- Startlocki
	-- Szałas w Górach Sinych
	if self.szalas_sine_long then killTrigger(self.szalas_sine_long) end
	self.szalas_sine_long = tempTrigger(
	"Nieliczne skalne lachy i grupy glazow niczym wyspy wyrastaja sposrod gesto pokrywajacej te zbocza kosodrzewiny. Jej zwarte galezie i wiecznie zielone igly stanowia bariere praktycznie nie do pokonania",
		function()
			amap:print_log("GPS po nazwie/wyjsciach lokacji: Szałas z Górach Sinych'", true)
			amap:set_position(8242, true)
		end)

	if self.szalas_sine_short then killTrigger(self.szalas_sine_short) end
	if self.szalas_sine_wyjscia then killTrigger(self.szalas_sine_wyjscia) end
	self.szalas_sine_short = tempTrigger("Przy solidnym szalasie.", function()
		self.szalas_sine_wyjscia = tempTrigger(
		"Sa tutaj trzy widoczne wyjscia: polnocny-wschod, polnocny-zachod i poludniowy-wschod.", function()
			amap:print_log("GPS po nazwie/wyjsciach lokacji: Szałas z Górach Sinych'", true)
			amap:set_position(8242, true)
		end, 1)
	end)

	self.handler = scripts.event_register:force_register_event_handler(self.handler, "amapNewLocation",
		function(event, loc)
			if Msmudlet.mapy.szalas_sine_wyjscia then
				killTrigger(Msmudlet.mapy.szalas_sine_wyjscia)
				Msmudlet.mapy.szalas_sine_wyjscia = nil
			end
		end)

	if self.kamienne_serce_mahakamu then killTrigger(self.kamienne_serce_mahakamu) end
	self.kamienne_serce_mahakamu = tempTrigger(
	"Niewielka rownina rozciaga sie tuz pod traktem przecinajacym pasmo Tantullskich Wierchow, kamienne serce Mahakamu.",
		function()
			amap:print_log("GPS po longu: Mahakam, góry blisko Mons Arx'", true)
			amap:set_position(8938, true)
		end)

	if self.osada_gornicza_tilea then killTrigger(self.osada_gornicza_tilea) end
	self.osada_gornicza_tilea = tempTrigger(
	"Znajdujesz sie w jednym z wiekszych barakow sypialnych tego obozu. Cala jego powierzchnia wypelniona jest prostymi, pietrowymi lozkami z surowego drewna, na ktore niedbale rzucono stare materace",
		function()
			amap:print_log("GPS po longu: Osada górnicza w Tilei", true)
			amap:set_position(11059, true)
		end)


	if self.galeon_novi then killTrigger(self.galeon_novi) end
	self.galeon_novi = tempTrigger(
	"Okret sunie przez wody Pontaru, zas novigradzki port staje sie coraz wiekszy i wyrazniejszy.", function()
		amap:print_log("Dopływasz do portu w Novigradzie.", true)
		amap:set_position(7909, true)
	end)

	if self.sterowiec_gks then killTrigger(self.sterowiec_gks) end
	self.sterowiec_gks = tempTrigger("Na dachu warowni.", function()
		amap:print_log("Przystań sterowca w Górach Krańca Świata", true)
		amap:set_position(15149, true)
	end)

	if self.polanka_pustki then killTrigger(self.polanka_pustki) end
	self.polanka_pustki = tempTrigger(
	"Znajdujesz sie na niewielkim skrawku ziemi, czyms na ksztalt polanki, wykarczowanej przez podroznikow w samym sercu kepy gestych krzewow.",
		function()
			amap:print_log("Polanka koło Pustek", true)
			amap:set_position(6398, true)
		end)

	if self.utopce_waskie_schodki_1 then killTrigger(self.utopce_waskie_schodki_1) end
	if self.utopce_waskie_schodki_2 then killTrigger(self.utopce_waskie_schodki_2) end
	self.utopce_waskie_schodki_1 = tempTrigger("Waskie schodki.", function()
		self.utopce_waskie_schodki_2 = tempTrigger("Ciezka stalowa krata zamykajaca przejscie po schodach na dol.",
			function()
				amap:print_log("GPS po nazwie/wyjsciach lokacji: Garnizon Utopców'", true)
				amap:set_position(17887, true)
			end, 1)
	end)
end

Msmudlet.mapy:startlocki()

function Msmudlet.mapy:follows()
	--  za dowódcą drużyny
	if self.follow.wdol then killTrigger(self.follow.wdol) end
	self.follow.wdol = tempRegexTrigger(
	"(Korzystajac z nierownosci i szczelin skalnych, .* zaczyna schodzic po kamiennej scianie na dol\\.)", function()
		scripts.utils.bind_functional_team_follow("zejdz na dol")
	end)
end

Msmudlet.mapy:follows()

function Msmudlet.mapy:blockery()
	-- Do gabinetu maja wstep tylko Mistrz i Zastepca.
	if self.blockers.gabinet_mistrza then killTrigger(self.blockers.gabinet_mistrza) end
	self.blockers.gabinet_mistrza = tempTrigger("Do gabinetu maja wstep tylko Mistrz i Zastepca.", function()
		trigger_func_mapper_blockers_blocker()
	end)
end

Msmudlet.mapy:blockery()
