#!/usr/bin/env python3

# Autor: Leskap (2023.09.20)
# ------------------------------------------
# Changelog:
# 2024.01.04 - szydell:
#  - change input to argz
#  - support 2 item types
# 2024.01.04 - Leskap:
#  - return one solution


import sys

def traverse(mod, prawo, lewo, idx, list_digit, solution):
  next_r = (idx + list_digit[idx]) % mod
  next_l = idx - list_digit[idx]
  if(next_l < 0):
    next_l = mod + next_l
        
  if(list_digit[idx] != 0):
    list_digit[idx] = 0
    
    if(sum(list_digit) == 0):
      globals()["solution"]=solution
      return 1
      
    if(sum(list_digit) != 0):
      solution_r = solution + prawo
      solution_l = solution + lewo
      list_digit_r = list_digit.copy()
      list_digit_l = list_digit.copy()
      if(traverse(mod, prawo, lewo, next_r, list_digit_r, solution_r) == 1):
        return 1
      if(traverse(mod, prawo, lewo, next_l, list_digit_l, solution_l) == 1):
        return 1

arg_cnt=len(sys.argv)-1
if arg_cnt != 2:
  print(f"Two arguments expected, got {arg_cnt}")
  raise SystemExit(1)

item=sys.argv[1]
a=sys.argv[2]

list_of_digits = [int(x) for x in str(a)]
if(item == "pudelko"):
  mod=16
  prawo="obroc igle kompasu w prawo;"
  lewo="obroc igle kompasu w lewo;"
elif(item == "zegar_golemow"):
  mod=12
  prawo="obroc wskazowke w prawo;"
  lewo="obroc wskazowke w lewo;"
else:
  print(f"Two arguments expected, got {item}")
  raise SystemExit(1)

if len(list_of_digits) != mod:
  print(f"List of {mod} numbers expeted, got {len(list_of_digits)}")
  raise SystemExit(1)

idx = 0
solution = ""


traverse(mod, prawo, lewo, idx, list_of_digits.copy(), solution)
if solution != "":
  print(solution)
else:
  print("Brak rozwiazania. Sprawdz poprawnosc wprowadzonych wierzcholkow.")

