Msmudlet.autko = Msmudlet.autko or {
  kolejka = {},
  aliasy = {},
  triggery = {},
  status = "off",
  sledz_status = "off",
  wspieraj_status = "off",
  zaslaniaj_status = "off",
  walcz_status = "off",
  grab_status = "off",
  bindy = {},
  sledz_triggery = {},
  sledz_handlery = {},
  walcz_handlery = {},
  wspieraj_triggery = {},
  wspieraj_handlery = {},
  atak_cooldown = 0.1,
  grab_triggery = {},
  sledz_komenda = "",
  autoInput = false
}

function Msmudlet.autko:on()
  resumeNamedTimer("Msmudlet", "Autko")
  self.status = "on"
  Msmudlet.toolbar:autkoOn()
  scripts:print_log("Autko zostało uruchomione.")
end

function Msmudlet.autko:dontidle()
  if self.sledz_status ~= "on" and self.wspieraj_status ~= "on" and self.grab_status ~= "on" and self.walcz_status ~= "on" and self.zaslaniaj_status ~= "on" then
    self.current_idle_time = self.max_idle_time
    Msmudlet.toolbar:indicateIdleStateOff()
    return
  end
  self.current_idle_time = self.current_idle_time - 1
  Msmudlet.toolbar:indicateIdleState(self.current_idle_time, self.max_idle_time)
  if self.current_idle_time > 0 then
    Msmudlet.autko:kolejkuj("dontidle", 600, function()
      Msmudlet.autko:dontidle()
    end, 1, false)
  else
    Msmudlet.autko:print_log("Wyłączam się z powodu zbyt długiej nieaktywności")
    if Msmudlet.autko.sledz_status == "on" then
      Msmudlet.autko:sledz_off()
    end
    if Msmudlet.autko.wspieraj_status == "on" then
      Msmudlet.autko:wspieraj_off()
    end
    if Msmudlet.autko.grab_status == "on" then
      Msmudlet.autko:grab_off()
    end
    if Msmudlet.autko.zaslaniaj_status == "on" then
      Msmudlet.autko:zaslaniaj_off()
    end
    if Msmudlet.autko.walcz_status == "on" then
      Msmudlet.autko:walcz_off()
    end
    self.current_idle_time = 0
    Msmudlet.toolbar:indicateIdleStateOff()
  end
end

function Msmudlet.autko:resetidle()
  self.current_idle_time = self.max_idle_time
  Msmudlet.autko:usun_z_kolejki("dontidle")
  if self.sledz_status ~= "on" and self.wspieraj_status ~= "on" and self.grab_status ~= "on" and self.walcz_status ~= "on" and self.zaslaniaj_status ~= "on" then
    Msmudlet.toolbar:indicateIdleStateOff()
    return
  end

  Msmudlet.toolbar:indicateIdleState(self.current_idle_time, self.max_idle_time)
  Msmudlet.autko:kolejkuj("dontidle", 600, function()
    Msmudlet.autko:dontidle()
  end, 1, false)
end

function Msmudlet.autko:controlInput(command)
  if self.autoInput then
    self.autoInputTimer = tempTimer(0.2, function() Msmudlet.autko.autoInput = false end)
    if self.autoInputTimer ~= nil then
      killTimer(self.autoInputTimer)
      self.autoInputTimer = nil
    end
    -- % szansa na anulowanie komendy. Im dłuższy idl tym większa szansa.
    local chance = self.current_idle_time / self.max_idle_time
    local x = math.random() - 0.25
    if (x > chance) then
      local t = table.size(Msmudlet.autko.komunikaty_idle)
      if (t == nil) then
        t = 1
      end
      local r = math.random(t)
      Msmudlet.autko:print_log(Msmudlet.autko.komunikaty_idle[r], 2, "red")
      alias_func_skrypty_misc_attack_beep_test()
      denyCurrentSend()
    end
  else
    Msmudlet.autko:resetidle()
  end
end

function Msmudlet.autko:off()
  stopNamedTimer("Msmudlet", "Autko")
  self.status = "off"
  Msmudlet.toolbar:autkoOff()
  for k, v in pairs(self.triggery) do
    killTrigger(v)
  end
  if Msmudlet.autko.sledz_status == "on" then
    Msmudlet.autko:sledz_off()
  end
  if Msmudlet.autko.wspieraj_status == "on" then
    Msmudlet.autko:wspieraj_off()
  end
  if Msmudlet.autko.grab_status == "on" then
    Msmudlet.autko:grab_off()
  end
  if Msmudlet.autko.zaslaniaj_status == "on" then
    Msmudlet.autko:zaslaniaj_off()
  end
  if Msmudlet.autko.walcz_status == "on" then
    Msmudlet.autko:walcz_off()
  end
  Msmudlet.toolbar:indicateIdleStateOff()
  scripts:print_log("Autko zostało zgaszone.")
end

function Msmudlet.autko:kolejkuj(nazwa, opoznienie, akcja, occurences, clearOnNewLocation)
  -- fast : 0.8s - 2.5s
  -- slow : 30.1s - 100s
  -- very slow : 50.1s - 500s
  -- idle : 30.1s - 1530s
  -- normal : 0.8 to 5s
  -- regular : 0.7 to 10s

  if opoznienie == "fast" then
    opoznienie = math.random(14) + 4
  elseif opoznienie == "normal" then
    local tmp = math.random(100)
    if tmp == 100 then                 -- 1%
      opoznienie = 40 + math.random(10)
    elseif tmp < 99 and tmp > 95 then  -- 4%
      opoznienie = 20 + math.random(20)
    elseif tmp <= 95 and tmp > 80 then -- 15 %
      opoznienie = 13 + math.random(20)
    elseif tmp <= 80 and tmp > 50 then -- 30 %
      opoznienie = 10 + math.random(20)
    elseif tmp <= 50 and tmp > 30 then -- 20 %
      opoznienie = 13 + math.random(10)
    else                               -- 30 %
      opoznienie = 9 + math.random(15)
    end
  elseif opoznienie == "regular" then
    local tmp = math.random(100)
    if tmp == 100 then                 -- 1%
      opoznienie = 50 + math.random(50)
    elseif tmp < 100 and tmp > 95 then -- 4%
      opoznienie = 30 + math.random(70)
    elseif tmp <= 95 and tmp > 80 then -- 15 %
      opoznienie = 30 + math.random(40)
    elseif tmp <= 80 and tmp > 50 then -- 30 %
      opoznienie = 7 + math.random(10) + math.random(10) + math.random(10) + math.random(10)
    elseif tmp <= 50 and tmp > 30 then -- 20 %
      opoznienie = 6 + math.random(94)
    else                               -- 30 %
      opoznienie = 6 + math.random(25)
    end
  elseif opoznienie == "idle" then
    opoznienie = 500 + math.random(15000)
  elseif opoznienie == "slow" then
    opoznienie = 350 + math.random(930)
  elseif opoznienie == "very slow" then
    opoznienie = 500 + math.random(4500)
  elseif opoznienie == "zaslanianie" then
    opoznienie = 40 + math.random(90)
  end

  local count = 0
  for _, v in pairs(self.kolejka) do
    -- echo("nazwa: " .. nazwa .. " v.nazwa:" .. v.nazwa .. "\n")
    if v.nazwa == nazwa then
      count = count + 1
    end
  end

  -- echo("zadanie: " .. nazwa .. ", count: " .. count .. ", occurences: " .. occurences .. "\n")

  if (count >= occurences) then
    return
  end

  table.insert(self.kolejka, {
    nazwa = nazwa,
    opoznienie = opoznienie,
    akcja = akcja,
    occurences = occurences,
    clearOnNewLocation = clearOnNewLocation
  })
end

function Msmudlet.autko:autko_switch()
  if self.status == "off" then
    Msmudlet.autko:on()
  else
    Msmudlet.autko:off()
  end
end

function Msmudlet.autko:rozladuj_kolejke()
  for k, v in pairs(self.kolejka) do
    self.autoInput = true
    send(v.akcja)
    self.kolejka[k] = nil
  end
end

function Msmudlet.autko:usun_z_kolejki(nazwa)
  local kolejkaCopy = self.kolejka
  self.kolejka = {}
  for k, v in pairs(kolejkaCopy) do
    if v.nazwa ~= nazwa then
      table.insert(self.kolejka, kolejkaCopy[k])
    end
  end
end

function Msmudlet.autko:pop()
  local kolejkaCopy = self.kolejka
  self.kolejka = {}

  for k, v in pairs(kolejkaCopy) do
    if v.opoznienie < 1 then
      self.autoInput = true
      if self.autoInputTimer ~= nil then
        killTimer(self.autoInputTimer)
        self.autoInputTimer = nil
      end
      self.autoInputTimer = tempTimer(0.1, function() Msmudlet.autko.autoInput = false end)
      if type(v.akcja) == "function" then
        v.akcja()
      else
        sendAll(v.akcja)
      end
    else
      kolejkaCopy[k].opoznienie = v.opoznienie - 1
      table.insert(self.kolejka, kolejkaCopy[k])
    end
  end
end

function Msmudlet_autko_alias_pop()
  Msmudlet.autko:pop()
end

function Msmudlet.autko:panic()
  if self.triggery.materializuje then
    killTrigger(self.triggery.materializuje)
  end
  self.triggery.materializuje = tempTrigger("materializuje sie gwaltownie w powietrzu", function()
    scripts:print_log("\nAwaryjnie wyłączam AUTKO!!!\n")
    Msmudlet.autko:off()
    alias_func_skrypty_misc_attack_beep_test()
    alias_func_skrypty_misc_attack_beep_test()
    alias_func_skrypty_misc_attack_beep_test()
    alias_func_skrypty_misc_attack_beep_test()
  end)

  if self.triggery.chochlik then
    killTrigger(self.triggery.chochlik)
  end
  self.triggery.chochlik = tempTrigger("chochli.*czarodziej", function()
    scripts:print_log("\nAwaryjnie wyłączam AUTKO!!!\n")
    Msmudlet.autko:off()
    alias_func_skrypty_misc_attack_beep_test()
    alias_func_skrypty_misc_attack_beep_test()
    alias_func_skrypty_misc_attack_beep_test()
    alias_func_skrypty_misc_attack_beep_test()
  end)

  if self.triggery.przenosi then
    killTrigger(self.triggery.przenosi)
  end
  self.triggery.przenosi = tempTrigger("przenosi cie", function()
    scripts:print_log("\nAwaryjnie wyłączam AUTKO!!!\n")
    Msmudlet.autko:off()
    alias_func_skrypty_misc_attack_beep_test()
    alias_func_skrypty_misc_attack_beep_test()
    alias_func_skrypty_misc_attack_beep_test()
    alias_func_skrypty_misc_attack_beep_test()
  end)

  if self.triggery.przenosi2 then
    killTrigger(self.triggery.przenosi2)
  end
  self.triggery.przenosi2 = tempTrigger("Nagle czujesz, jak jakas magiczna sila cie gdzies przenosi.", function()
    scripts:print_log("\nAwaryjnie wyłączam AUTKO!!!\n")
    Msmudlet.autko:off()
    alias_func_skrypty_misc_attack_beep_test()
    alias_func_skrypty_misc_attack_beep_test()
    alias_func_skrypty_misc_attack_beep_test()
    alias_func_skrypty_misc_attack_beep_test()
  end)

  if self.triggery.utopi then
    killTrigger(self.triggery.utopi)
  end
  self.triggery.utopi = tempTrigger("cie kiedys utopi", function()
    scripts:print_log("\nAwaryjnie wyłączam AUTKO!!!\n")
    Msmudlet.autko:off()
    alias_func_skrypty_misc_attack_beep_test()
    alias_func_skrypty_misc_attack_beep_test()
    alias_func_skrypty_misc_attack_beep_test()
    alias_func_skrypty_misc_attack_beep_test()
  end)
end

function Msmudlet.autko:init()
  self.control_handler = scripts.event_register:force_register_event_handler(self.control_handler, "sysDataSendRequest",
    function()
      Msmudlet.autko:controlInput(command)
    end)
  Msmudlet.toolbar:indicateIdleStateOff()
  self.max_idle_time = 15
  self.current_idle_time = self.max_idle_time
  self.autoInput = false
  self.komunikaty_idle = {}
  table.insert(self.komunikaty_idle, "Nie o takie spanie walczylismy!")
  table.insert(self.komunikaty_idle, "Nie spać! Zwiedzać!")
  table.insert(self.komunikaty_idle, "Spanie grozi kasacją i chorobami wenerycznymi!")
  table.insert(self.komunikaty_idle, "Od spania expa przybywa... do czasu!")
  table.insert(self.komunikaty_idle, "Tuko czuwa!")
  table.insert(self.komunikaty_idle, "Rafgart czuwa!")
  table.insert(self.komunikaty_idle, "Ulik tego nie pochwala!")
  table.insert(self.komunikaty_idle, "Nie bujaj w obłokach, to grozi utratą postaci!")
  table.insert(self.komunikaty_idle, "Gandalf nie spał! Bądź jak Gandalf!")
  table.insert(self.komunikaty_idle, "Nie śpij, bo cię z expa okradną!")
  table.insert(self.komunikaty_idle, "Kto śpi, ten traci doświadczenie! Odpoczynek to luksus dla przegranych!")
  table.insert(self.komunikaty_idle, "Zachowaj czujność! Wizowie nie znają litości!")
  table.insert(self.komunikaty_idle, "Kasacja nie poczeka na powrót z kibelka!")
  table.insert(self.komunikaty_idle, "Przerwa na siku to strata czasu! Twoja postać nie potrzebuje toalety!")
  table.insert(self.komunikaty_idle, "Toaleta to luksus! Wizowie nie czekają!")
  table.insert(self.komunikaty_idle, "Skoro gra trwa, nie ma czasu na przerwy!")
  table.insert(self.komunikaty_idle, "Nieprzypilnowane postępy gorzej smakują!")
  table.insert(self.komunikaty_idle, "SGW nie pochwala takiego wstawania od kompa!")
  table.insert(self.komunikaty_idle, "Twój brak zaangażowania jest karygodny!")
  table.insert(self.komunikaty_idle, "No wciśnij coś wreszcie na tej klawiaturze!")
  table.insert(self.komunikaty_idle, "Lepiej mieć mniej nieznaków, niż kasację postaci!")
  table.insert(self.komunikaty_idle, "Twoja niechęć do zakańczania budzi niesmak Majstra!")
  table.insert(self.komunikaty_idle, "Nie śpij, bo cię okradną!")
  table.insert(self.komunikaty_idle, "Grittu idlował jak ty i już go z nami nie ma!")
  table.insert(self.komunikaty_idle, "Kadh kochał automaty i go skasowali!")
  table.insert(self.komunikaty_idle, "Idlowanie przerwało obiecującą karierę Gaba!")
  table.insert(self.komunikaty_idle, "Korsarze zostali skasowani za automaty! Nie bądź jak korsarze!")
  table.insert(self.komunikaty_idle, "Wydział Automatyki Użytkowej bacznie analizuje twoje zachowanie!")
  table.insert(self.komunikaty_idle, "WAU nie pochwala nadużywania automatów!")
  table.insert(self.komunikaty_idle, "Należy grać w stanie permanentnej aktywności!")
  




  self.handler = scripts.event_register:force_register_event_handler(self.handler, "amapNewLocation",
    function()
      local kolejkaCopy = self.kolejka
      self.kolejka = {}
      for k, v in pairs(kolejkaCopy) do
        if (v.clearOnNewLocation == false) then
          table.insert(self.kolejka, kolejkaCopy[k])
        end
      end
    end)
  deleteNamedTimer("Msmudlet", "Autko")
  registerNamedTimer("Msmudlet", "Autko", 0.1, Msmudlet_autko_alias_pop, true)
  Msmudlet.autko:off()
  tempTimer(3, function()
    Msmudlet.autko:on()
  end)

  if self.aliasy.switch then
    killAlias(self.aliasy.switch)
  end
  -- alias do wlaczania/wylaczania
  self.aliasy.switch = tempAlias("^/autko$", function()
    Msmudlet.autko:autko_switch()
  end)

  if self.aliasy.on then
    killAlias(self.aliasy.on)
  end
  self.aliasy.on = tempAlias("^/autko_on$", function()
    Msmudlet.autko:on()
  end)
  if self.aliasy.off then
    killAlias(self.aliasy.off)
  end
  self.aliasy.off = tempAlias("^/autko_off$", function()
    Msmudlet.autko:off()
  end)

  if self.aliasy.sledz_switch then
    killAlias(self.aliasy.sledz_switch)
  end
  -- alias do wlaczania/wylaczania
  self.aliasy.sledz_switch = tempAlias("^/sledz$", function()
    Msmudlet.autko:sledz_switch()
  end)

  if self.aliasy.sledz_on then
    killAlias(self.aliasy.sledz_on)
  end
  self.aliasy.sledz_on = tempAlias("^/sledz_on$", function()
    Msmudlet.autko:sledz_on()
  end)

  if self.aliasy.sledz_off then
    killAlias(self.aliasy.sledz_off)
  end
  self.aliasy.sledz_off = tempAlias("^/sledz_off$", function()
    Msmudlet.autko:sledz_off()
  end)

  if self.aliasy.wspieraj_switch then
    killAlias(self.aliasy.wspieraj_switch)
  end
  -- alias do wlaczania/wylaczania
  self.aliasy.wspieraj_switch = tempAlias("^/wspieraj$", function()
    Msmudlet.autko:wspieraj_switch()
  end)

  if self.aliasy.wspieraj_on then
    killAlias(self.aliasy.wspieraj_on)
  end
  self.aliasy.wspieraj_on = tempAlias("^/wspieraj_on$", function()
    Msmudlet.autko:wspieraj_on()
  end)
  if self.aliasy.wspieraj_off then
    killAlias(self.aliasy.wspieraj_off)
  end
  self.aliasy.wspieraj_off = tempAlias("^/wspieraj_off$", function()
    Msmudlet.autko:wspieraj_off()
  end)

  if self.aliasy.rozladuj then
    killAlias(self.aliasy.rozladuj)
  end
  self.aliasy.rozladuj = tempAlias("^/rozladuj_kolejke$", function()
    Msmudlet.autko:rozladuj_kolejke()
  end)

  if self.aliasy.grab_switch then
    killAlias(self.aliasy.grab_switch)
  end
  -- alias do wlaczania/wylaczania
  self.aliasy.grab_switch = tempAlias("^/grab$", function()
    Msmudlet.autko:grab_switch()
  end)

  if self.aliasy.grab_on then
    killAlias(self.aliasy.grab_on)
  end
  self.aliasy.grab_on = tempAlias("^/grab_on$", function()
    Msmudlet.autko:grab_on()
  end)

  if self.aliasy.grab_off then
    killAlias(self.aliasy.grab_off)
  end
  self.aliasy.grab_off = tempAlias("^/grab_off$", function()
    Msmudlet.autko:grab_off()
  end)

  -- alias zasłanianie on/off
  if self.aliasy.zaslaniaj_switch then
    killAlias(self.aliasy.zaslaniaj_switch)
  end
  self.aliasy.zaslaniaj_switch = tempAlias("^/zaslaniaj$", function()
    Msmudlet.autko:zaslaniaj_switch()
  end)

  if self.aliasy.zaslaniaj_on then
    killAlias(self.aliasy.zaslaniaj_on)
  end
  self.aliasy.zaslaniaj_on = tempAlias("^/zaslaniaj_on$", function()
    Msmudlet.autko:zaslaniaj_on()
  end)

  if self.aliasy.zaslaniaj_off then
    killAlias(self.aliasy.zaslaniaj_off)
  end
  self.aliasy.zaslaniaj_off = tempAlias("^/zaslaniaj_off$", function()
    Msmudlet.autko:zaslaniaj_off()
  end)

  -- alias walka on/off
  if self.aliasy.walcz_switch then
    killAlias(self.aliasy.walcz_switch)
  end
  self.aliasy.walcz_switch = tempAlias("^/walcz$", function()
    Msmudlet.autko:walcz_switch()
  end)

  if self.aliasy.walcz_on then
    killAlias(self.aliasy.walcz_on)
  end
  self.aliasy.walcz_on = tempAlias("^/walcz_on$", function()
    Msmudlet.autko:walcz_on()
  end)

  if self.aliasy.walcz_off then
    killAlias(self.aliasy.walcz_off)
  end
  self.aliasy.walcz_off = tempAlias("^/walcz_off$", function()
    Msmudlet.autko:walcz_off()
  end)

  Msmudlet.autko:panic()
end

Msmudlet.autko:init()

function Msmudlet.autko:wspieraj_switch()
  if self.wspieraj_status == "off" then
    Msmudlet.autko:wspieraj_on()
  else
    Msmudlet.autko:wspieraj_off()
  end
end

function Msmudlet.autko:wspieraj_on()
  if self.status == "off" then
    Msmudlet.autko:on()
  end
  self.wspieraj_status = "on"
  Msmudlet.toolbar:wspierajOn()
  Msmudlet.autko:resetidle()

  self.wspieraj_handlery.wesprzyj = scripts.event_register:force_register_event_handler(self.wspieraj_handlery
    .wesprzyj,
    "ateamAttackingDifferentTarget", function(event, loc)
      local tmp = math.random(100)
      if tmp > 2 and getEpoch() - Msmudlet.autko.atak_cooldown > 10 then
        Msmudlet.autko:kolejkuj("wesprzyj", "normal", function()
          Msmudlet.autko:wczyz()
        end, 3, true)
        if tmp < 3 then
          Msmudlet.autko:kolejkuj("wesprzyj", "normal", function()
            Msmudlet.autko:wczyz()
          end, 3, true)
        end
        Msmudlet.autko.atak_cooldown = getEpoch()
      end
    end)

  self.wspieraj_handlery.ateamEnemyKilled = scripts.event_register:force_register_event_handler(
    self.wspieraj_handlery.ateamEnemyKilled, "ateamEnemyKilled", function(event, loc)
      ---@diagnostic disable-next-line: undefined-field
      if table.size(ateam.team) < 2 then
        return
      end
      local tmp = math.random(100)
      if tmp > 2 and getEpoch() - Msmudlet.autko.atak_cooldown > 3 and loc > 0 then
        Msmudlet.autko:kolejkuj("wesprzyj", "normal", function()
          Msmudlet.autko:wczyz()
        end, 3, true)
        if tmp < 4 then
          Msmudlet.autko:kolejkuj("wesprzyj", "normal", function()
            Msmudlet.autko:wczyz()
          end, 3, true)
        end
        if tmp == 3 then
          Msmudlet.autko:kolejkuj("wesprzyj", "regular", function()
            Msmudlet.autko:wczyz()
          end, 3, true)
        end
        Msmudlet.autko.atak_cooldown = getEpoch()
      end
    end)

  self.wspieraj_handlery.atak = scripts.event_register:force_register_event_handler(self.wspieraj_handlery.atak,
    "ateamToAttackTarget", function(event, loc)
      local tmp = math.random(100)
      if tmp > 2 then
        if getEpoch() - Msmudlet.autko.atak_cooldown > 2 then
          Msmudlet.autko:kolejkuj("atakuj_wskazanie", "normal", function()
            send(ateam:get_attack_string() .. "cel ataku")
          end, 3, true)
        end
        if getEpoch() - Msmudlet.autko.atak_cooldown > 4 then
          if tmp < 4 then
            Msmudlet.autko:kolejkuj("atakuj_wskazanie", "normal", function()
              send(ateam:get_attack_string() .. "cel ataku")
            end, 3, true)
          end
          if tmp == 3 then
            Msmudlet.autko:kolejkuj("atakuj_wskazanie", "normal", function()
              send(ateam:get_attack_string() .. "cel ataku")
            end, 3, true)
          end
        end
        Msmudlet.autko.atak_cooldown = getEpoch()
      end
    end)

  scripts:print_log("Od teraz bede wspierac dowodce druzyny.")
  Msmudlet.autko:resetidle()
end

function Msmudlet.autko:wspieraj_off()
  self.wspieraj_status = "off"
  Msmudlet.toolbar:wspierajOff()
  ---@diagnostic disable-next-line: undefined-field
  if table.size(self.wspieraj_triggery) > 0 then
    for k, v in pairs(self.wspieraj_triggery) do
      killTrigger(v)
      self.wspieraj_triggery[k] = nil
    end
  end
  if table.size(self.wspieraj_handlery) > 0 then
    for k, v in pairs(self.wspieraj_handlery) do
      scripts.event_register:kill_event_handler(v)
      self.wspieraj_handlery[k] = nil
    end
  end
  Msmudlet.autko:resetidle()
  scripts:print_log("Przestaje wspierac dowodce druzyny.")
end

function Msmudlet.autko:wczyz()
  if not ateam.objs[ateam.my_id].team then -- nie próbuj wspierać gdy nie jesteś w drużynie
    return
  end
  for _, v in pairs(gmcp.objects.nums) do
    if not ateam.objs[v].team and ateam.objs[v].attack_target then
      send(ateam:get_attack_string() .. "cel ataku")
      return
    end
  end
  if table.size(ateam.team_enemies) > 0 then -- wesprzyj jeżeli team ma jakichś wrogów, a nie był wskazany cel ataku
    send(ateam.support_command)
    return
  end
end

function Msmudlet.autko:grab_switch()
  if self.grab_status == "off" then
    Msmudlet.autko:grab_on()
  else
    Msmudlet.autko:grab_off()
  end
end

function Msmudlet.autko:grab_on()
  if self.status == "off" then
    Msmudlet.autko:on()
  end
  self.grab_status = "on"
  Msmudlet.toolbar:grabOn()
  Msmudlet.autko:resetidle()
  scripts:print_log("Od teraz bede grabic samodzielnie. Ustawianie tego co mam grabic: /zbieranie")
end

function Msmudlet.autko:grab_off()
  self.grab_status = "off"
  Msmudlet.toolbar:grabOff()
  if self.grab_triggery then
    for k, v in pairs(self.grab_triggery) do
      killTrigger(v)
    end
  end
  ---@diagnostic disable-next-line: undefined-field
  if table.size(self.grab_handlery) > 0 then
    for k, v in pairs(self.grab_handlery) do
      scripts.event_register:kill_event_handler(v)
      self.grab_handlery[k] = nil
    end
  end
  Msmudlet.autko:resetidle()
  scripts:print_log("Przestaje grabic samodzielnie.")
end

-- follow

function Msmudlet.autko:sledz_switch()
  if self.sledz_status == "off" then
    Msmudlet.autko:sledz_on()
  else
    Msmudlet.autko:sledz_off()
  end
end

function Msmudlet.autko:sledz_on()
  if self.status == "off" then
    Msmudlet.autko:on()
  end
  self.sledz_status = "on"
  Msmudlet.toolbar:sledzOn()
  Msmudlet.autko:resetidle()
  scripts:print_log("Od teraz bede sledzic dowodce druzyny.")

  self.sledz_handlery.ateamTeamFollowBind = scripts.event_register:force_register_event_handler(self.handler,
    "ateamTeamFollowBind", function(...)
      if Msmudlet.autko.sledz_status == "on" then
        local tmp = math.random(100)
        if tmp < 21 then
          Msmudlet.autko:kolejkuj("sledz", "fast", function()
            scripts.utils.execute_functional()
          end, 1, true)
        elseif tmp < 91 and tmp >= 21 then
          Msmudlet.autko:kolejkuj("sledz", "normal", function()
            scripts.utils.execute_functional()
          end, 1, true)
        else
          Msmudlet.autko:kolejkuj("sledz", "regular", function()
            scripts.utils.execute_functional()
          end, 1, true)
        end
      end
    end)
end

function Msmudlet.autko:sledz_off()
  self.sledz_status = "off"
  Msmudlet.toolbar:sledzOff()
  if self.sledz_triggery then
    for k, v in pairs(self.sledz_triggery) do
      killTrigger(v)
    end
  end
  ---@diagnostic disable-next-line: undefined-field
  if table.size(self.sledz_handlery) > 0 then
    for k, v in pairs(self.sledz_handlery) do
      scripts.event_register:kill_event_handler(v)
      self.sledz_handlery[k] = nil
    end
  end
  Msmudlet.autko:resetidle()
  scripts:print_log("Przestaje sledzic dowodce druzyny.")
end

-- Zasłaniaj

function Msmudlet.autko:zaslaniaj_switch()
  if self.zaslaniaj_status == "off" then
    Msmudlet.autko:zaslaniaj_on()
  else
    Msmudlet.autko:zaslaniaj_off()
  end
end

function Msmudlet.autko:zaslaniaj_on()
  if self.status == "off" then
    Msmudlet.autko:on()
  end
  self.zaslaniaj_status = "on"
  Msmudlet.toolbar:zaslaniajOn()
  Msmudlet.autko:resetidle()
  scripts:print_log("Od teraz będę zasłaniać członków drużyny.")

  Msmudlet.autko:kolejkuj("wlacz zaslanianie", "fast", function()
    Msmudlet.autko:zaslaniaj()
  end, 1, false)
end

function Msmudlet.autko:zaslaniaj_off()
  self.zaslaniaj_status = "off"
  Msmudlet.toolbar:zaslaniajOff()
  Msmudlet.autko:resetidle()
  scripts:print_log("Przestaje zasłaniać członków drużyny.")
end

function Msmudlet.autko:zaslaniaj()
  if Msmudlet.autko.zaslaniaj_status == "on" then
    Msmudlet.zaslony:zaslon()
    Msmudlet.autko:kolejkuj("zaslaniaj", "zaslanianie", function()
      Msmudlet.autko:zaslaniaj()
    end, 1, false)
  end
end

-- Walka

function Msmudlet.autko:walcz_switch()
  if self.walcz_status == "off" then
    Msmudlet.autko:walcz_on()
  else
    Msmudlet.autko:walcz_off()
  end
end

function Msmudlet.autko:walcz_on()
  if self.status == "off" then
    Msmudlet.autko:on()
  end
  self.walcz_status = "on"
  Msmudlet.toolbar:walczOn()
  Msmudlet.autko:resetidle()
  scripts:print_log("Włączam automaty pomocnicze - dobywanie, atak kolejnego celu z kolejki")

  self.walcz_handlery.canWieldAfterKnockOff = scripts.event_register:force_register_event_handler(self.handler,
    "canWieldAfterKnockOff", function(...)
      if Msmudlet.autko.walcz_status == "on" then
        local tmp = math.random(100)
        if tmp < 21 then
          Msmudlet.autko:kolejkuj("dobadz broni", "fast", function()
            Msmudlet.autko:print_log("walcz - dobywam broni")
            scripts.utils.execute_functional()
          end, 1, true)
        elseif tmp < 81 and tmp >= 21 then
          Msmudlet.autko:kolejkuj("dobadz broni", "normal", function()
            Msmudlet.autko:print_log("walcz - dobywam broni")
            scripts.utils.execute_functional()
          end, 1, true)
        else
          Msmudlet.autko:kolejkuj("dobadz broni", "regular", function()
            Msmudlet.autko:print_log("walcz - dobywam broni")
            scripts.utils.execute_functional()
          end, 1, true)
        end
      end
    end)
  self.walcz_handlery.canWieldAfterKnockOff = scripts.event_register:force_register_event_handler(self.handler,
    "ateam_next_attack_obj_bind", function(...)
      if Msmudlet.autko.walcz_status == "on" then
        local tmp = math.random(100)
        if tmp < 39 then
          Msmudlet.autko:kolejkuj("wykonuje /nn", "fast", function()
            Msmudlet.autko:print_log("walcz - Wykonuje /nn")
            ateam_execute_next_attack_obj(false)
          end, 1, true)
        elseif tmp < 91 and tmp >= 40 then
          Msmudlet.autko:kolejkuj("wykonuje /nn", "normal", function()
            Msmudlet.autko:print_log("walcz - Wykonuje /nn")
            ateam_execute_next_attack_obj(false)
          end, 1, true)
        else
          Msmudlet.autko:kolejkuj("wykonuje /nn", "regular", function()
            Msmudlet.autko:print_log("walcz - Wykonuje /nn")
            ateam_execute_next_attack_obj(false)
          end, 1, true)
        end
      end
    end)
end

function Msmudlet.autko:walcz_off()
  self.walcz_status = "off"
  Msmudlet.toolbar:walczOff()
  Msmudlet.autko:resetidle()
  scripts:print_log("Wyłączam automaty pomocnicze - dobywanie")
  ---@diagnostic disable-next-line: undefined-field
  if table.size(self.walcz_handlery) > 0 then
    for k, v in pairs(self.walcz_handlery) do
      scripts.event_register:kill_event_handler(v)
      self.walcz_handlery[k] = nil
    end
  end
end

function Msmudlet.autko:print_log(msg, new_line, color)
  local color = color or "tomato"
  if msg ~= nil then
    if new_line then
      echo("\n")
    end
    ---@diagnostic disable-next-line: undefined-field, param-type-mismatch
    for _, line in pairs(string.split(msg, "\n")) do
      cecho(string.format("<CadetBlue>(autko)<%s>: %s\n", color, line))
    end
  end
  resetFormat()
end
