Msmudlet.synchro = Msmudlet.synchro or {
    triggers = {},
    aliases = {},
    handlers = {},
    status = {},
    apoID = nil
}

function Msmudlet.synchro:getApoID()
    if Msmudlet.synchro.handlers["get_apo_id"] then
        killAnonymousEventHandler(Msmudlet.synchro.handlers["get_apo_id"])
    end

    self.handlers["get_apo_id"] = scripts.event_register:force_register_event_handler(self.handlers["get_apo_id"],
        "gmcp.gmcp_msgs", function()
            local btext=gmcp.gmcp_msgs.text
            local text = Msmudlet.lib:base64decode(btext)
            if string.match(text, "Swiat odrodzil sie  :") then
                self.apoID=btext
                killAnonymousEventHandler(Msmudlet.synchro.handlers["get_apo_id"])
            end
        end)
    send("system")
end
