Msmudlet.sledzie = Msmudlet.sledzie or {
  lokalizatory = {}
}

function Msmudlet.sledzie:init()
  -- wejdz / zsiadz za dowódcą drużyny
  if self.lokalizatory.ksstatek_follow then
    killTrigger(self.lokalizatory.ksstatek_follow)
  end
  self.lokalizatory.ksstatek_follow = tempRegexTrigger(
    "(.* wchodzi na (korsarski|skelliganski) (snekkar|drakkar) .* po trapie\\.)", function()
      scripts.utils.bind_functional_team_follow("wejdz na " .. matches[4], false)
    end)

  if self.lokalizatory.prom_follow then
    killTrigger(self.lokalizatory.prom_follow)
  end
  self.lokalizatory.prom_follow = tempRegexTrigger("(.* wchodzi na .* prom\\.$)", function()
    scripts.utils.bind_functional_team_follow("wejdz na prom", false)
  end)

  if self.lokalizatory.tratwa_follow then
    killTrigger(self.lokalizatory.tratwa_follow)
  end
  self.lokalizatory.tratwa_follow = tempRegexTrigger("( wchodzi na.+tratwe\\.$)", function()
    scripts.utils.bind_functional_team_follow("wejdz na tratwe", false)
  end)

  if self.lokalizatory.ksstatek_zejdz then
    killTrigger(self.lokalizatory.ksstatek_zejdz)
  end
  self.lokalizatory.ksstatek_zejdz = tempRegexTrigger("(.* schodzi z.* na brzeg\\.$)", function()
    scripts.utils.bind_functional_team_follow("zejdz ze statku", false)
  end)

  if self.lokalizatory.statki_follow then
    killTrigger(self.lokalizatory.statki_follow)
  end
  self.lokalizatory.statki_follow = tempRegexTrigger(
    " wchodzi na .*(barkas|barke|bryg|buzar|drakkar|feluke|galeon|galere|knare|krype|okret|skeid|statek|szkute)\\.$",
    function()
      scripts.utils.bind_functional_team_follow("wem;kup bilet;wsiadz na statek;wlm", false)
    end)

  if self.lokalizatory.wozy then
    killTrigger(self.lokalizatory.wozy)
  end
  self.lokalizatory.wozy = tempRegexTrigger(" siada (w|na) (.+ (bryczce|wozie|dylizansie))\\.$", function()
    scripts.utils.bind_functional_team_follow("usiadz " .. matches[2] .. " " .. matches[3], false)
  end)

  if self.lokalizatory.wozy_wstan then
    killTrigger(self.lokalizatory.wozy_wstan)
  end
  self.lokalizatory.wozy_wstan = tempRegexTrigger(" (z|wy)siada z .+ (bryczki|wozu|dylizansu)\\.$", function()
    scripts.utils.bind_functional_team_follow("wstan", false)
  end)

  if self.lokalizatory.powoz_bodrog_maribor then
    killTrigger(self.lokalizatory.powoz_bodrog_maribor)
  end
  self.lokalizatory.powoz_bodrog_maribor = tempTrigger(
    " placi siedzacemu na kozle woznicy i wsiada na otwarty stojacy powoz", function()
      scripts.utils.bind_functional_team_follow("wem;wsiadz do powozu;wlm", false)
    end)

  if self.lokalizatory.wozy_dylizansy then
    killTrigger(self.lokalizatory.wozy_dylizansy)
  end
  self.lokalizatory.wozy_dylizansy = tempRegexTrigger(" placi woznicy i wsiada do .+ (dylizansu|wozu)\\.$", function()
    scripts.utils.bind_functional_team_follow("wem;wsiadz do " .. matches[2] .. ";wlm", false)
  end)
end

Msmudlet.sledzie:init()
