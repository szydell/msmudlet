function scripts.utils.execute_functional()
    if scripts.utils.functional_key then
        if type(scripts.utils.functional_key) == "function" then
            scripts.utils.functional_key()
            scripts.utils.bind_functional(function() send("stan") end, true)
        else
---@diagnostic disable-next-line: undefined-field
            local sep = string.split(scripts.utils.functional_key, "[;#]")
            for k, v in pairs(sep) do
                expandAlias(v, true)
            end
            scripts.utils.functional_key = "stan"
        end
    end
end

scripts.utils.functional_key = "stan"