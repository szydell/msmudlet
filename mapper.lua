Msmudlet.mapper = Msmudlet.mapper or {
	aliasy = {}
}

function Msmudlet.mapper:start()

	local tmpAreas=getAreaTable()
	if not tmpAreas then
		return
	end
	if tmpAreas["sterowiec"] ~= nil then
		echo("")
		cecho("\n\n<CadetBlue>(msmudlet)<tomato>: Uwaga! Ważne! Korzystasz ze starych map. Kliknij ")
		cechoLink("<green>tutaj", function() alias_func_skrypty_installer_download_map() end, "Aktualizuj", true)
		cecho(" <tomato>aby pobrać <reset>.\n\n")
	end

end

tempTimer(7, function() Msmudlet.mapper:start() end)

