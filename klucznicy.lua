Msmudlet.klucznicy = Msmudlet.klucznicy or {
  triggers = {},
  aliases = {},
  handlers = {},
  status = {},
  kfn = Msmudlet.dane.data_dir .. "/klucznicy.json",
  ticketUrl = "https://gitlab.com/sterowiec-sgw/sgw-dane/-/issues/new"
}

function Msmudlet.klucznicy:load()
  local df = io.open(self.kfn)
  if df then
    local tmp = yajl.to_value(df:read("*a"))
    io.close(df)
    if tmp then
      self.klucznicy = tmp.klucznicy
    end
  end
end

function Msmudlet.klucznicy:definiuj(klucznik, dane)
  local i = 1
  for _, p in pairs(dane.patterns) do
    local tn = klucznik .. "_" .. i
    if self.triggers[tn] then
      killTrigger(self.triggers[tn])
    end
    self.triggers[tn] = tempTrigger(p, function()
      if dane.lazik == "zatrzymaj" then
        if amap.walker then
          raiseEvent("playBeep")
          amap:print_log("NPC kluczykowy, zatrzymuję łazik.")
          amap:terminate_walker()
        end
      elseif dane.lazik == "wycofaj" then
        if amap.walker then
          raiseEvent("playBeep")
          tempTimer(0.5, function()
            raiseEvent("playBeep")
          end)
          tempTimer(1, function()
            raiseEvent("playBeep")
          end)

          local location_id = List.popright(amap.history)
          local plocation_id = List.peek_last(amap.history)
          amap:terminate_walker()
          if not plocation_id then
            List.push(amap.history, location_id)
            amap:print_log("NPC kluczykowy, zatrzymuję łazik.")

            return
          end
          amap:print_log("Niebezpieczny NPC kluczykowy, WYCHODZĘ Z LOKACJI.")
          local t = 0.6 + math.random() * (1.3 - 0.6);
          tempTimer(t, function()
            raiseEvent("playBeep")
            amap.walker_delay = t
            gotoRoom(plocation_id)
          end)


          -- amap:terminate_walker()
        end
      end
      selectString(matches[1], 1)
      -- W celu zmiany koloru tekstu na customowy, stwórz plik sgw_custom.json w katalogu, który uzyskasz wykonując komendę lua getMudletHomeDir()
      -- W pliku dodaj{ "klucznicy.definiuj.fg":"red" }
      -- Zmieni to kolor podświetlanego npc na czerwony

      if Msmudlet.custom and Msmudlet.custom["klucznicy.definiuj.fg"] then
        fg(Msmudlet.custom["klucznicy.definiuj.fg"])
      else
        fg("green")
      end
      
      replace(matches[1])
      resetFormat()
    end)
    i = i + 1
  end
end

function Msmudlet.klucznicy:cleanTriggers()
  for key, id in pairs(self.triggers) do
    killTrigger(id)
    self.triggers[key] = nil
  end
end

function Msmudlet.klucznicy:kolorujLokacje()
  for k, r in pairs(self.klucznicy) do
    if r.color and r.locations then
      for _, location_id in ipairs(r.locations) do
        setRoomEnv(location_id, r.color) -- Assign an environment ID to the room
        echo("\n")
        cecho("<yellow>" .. k .. " - Ustawiono kolor " .. r.color .. " dla lokacji " .. location_id.."\n")
      end
    end
  end
end

function Msmudlet.klucznicy:init()
  self:load()
  if self.aliases.klucznicy then
    killAlias(self.aliases.klucznicy)
  end
  self.aliases.klucznicy = tempAlias("^/klucznicy$", function()
    Msmudlet.klucznicy:listuj()
  end)
  ---@diagnostic disable-next-line: undefined-field
  if self.triggers and table.size(self.triggers) > 0 then
    self:cleanTriggers()
  end
  if self.klucznicy == nil then
    cecho("\n <orange>Brak listy kluczników. Ściągnij sgw-dane (/sgw_dane_pobierz). Przeładuj mudleta.")
    return
  end
  for k, r in pairs(self.klucznicy) do
    Msmudlet.klucznicy:definiuj(k, r)
  end
  Msmudlet.klucznicy:kolorujLokacje()
end

Msmudlet.klucznicy:init()

function Msmudlet.klucznicy:listuj()
  cecho("+--\n")
  cecho("|\n")
  cecho("| <yellow>KLUCZNICY<grey>\n")
  cecho("|\n")
  local i = 1
  for k, r in spairs(self.klucznicy) do
    if r.opis == nil then
      r.opis = ""
    end
    cecho("| " .. string.sub(i .. "       ", 0, 3) .. " <light_slate_blue>" .. k .. "<grey> - " .. r.opis .. "\n")
    i = i + 1
  end
  cecho("|\n")
  cecho("| <yellow>Widzisz braki? <grey>")
  cechoLink("Zgłoś tutaj!", function()
    openWebPage(self.ticketUrl)
  end, "Otworzy stronę w przeglądarce", true)
  cecho("\n")
  cecho("|\n")
  cecho("+--\n")
end
