# msmudlet
plugin rozszerzający i dopasowujący skrypty https://github.com/tjurczyk/arkadia


# Jak pomóc w rozwoju?
1. Musisz mieć skonfigurowanego git'a. Dodane klucze ssh do profilu na gitlabie itd. Nie wiesz jak? Call szydell.
2. Sklonuj to repozytorium do pasującego ci katalogu (```git clone git@gitlab.com:szydell/msmudlet.git```)
3. Wykonaj w Mudlecie komendę ```lua Msmudlet.updater:dev()```. Skończy się błędem w którym będzie informacja gdzie wrzucić plik msm_dev.json. Zrób to, a następnie popraw w nim ścieżkę do sklonowanego właśnie repozytorium.
4. Wygeneruj paczkę lokalną korzystając ze skryptu ```devpack.sh```. Możesz go uruchamiać na 2 sposoby. Albo w aktywnej sesji ustaw sobie zmienną ```msm_devdir``` tak aby wskazywała na katalog z repozytorium (np. ```export msm_devdir=/home/szydell/dev/msmudlet```). Albo odpalaj ```devpack.sh <ścieżka_do_repozytorium>```.
5. Kolejne uruchomienie komendy ```lua Msmudlet.updater:dev()``` powinno przejść już bezbłędnie. W ten sposób, możesz sobie testować lokalnie własne zmiany do skryptów.
6. Pamiętaj! Nie możesz commitować do głównej gałęzi. Jeżeli tworzysz jakąś poprawkę, zacznij od założenia brancha. Gdy poprawka jest gotowa, wyślij ją do gitlaba i zgłoś Merge Requesta.

**Dzięki za pomoc!**

# Coś nie działa?
Zgłoś to tutaj -> https://gitlab.com/szydell/msmudlet/-/issues/new.  
Postaram się naprawić.
