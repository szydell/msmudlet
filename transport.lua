Msmudlet.transport = Msmudlet.transport or {
  triggers = {},
  aliases = {},
  stopsToExit = 0
}



function Msmudlet.transport:init()
  if self.aliases["statek"] then
    killAlias(self.aliases["statek"])
  end
  self.aliases["statek"] = tempAlias("^/statek(.*)$", function()
    local m = matches[2]
    if m == nil or m == "" then
      Msmudlet.transport:wait4ship(0)
      return
    end
    local w = string.match(m, "^ (%d+)$")
    if w ~= nil or w ~= "" then
      Msmudlet.transport:wait4ship(w)
    end
  end)

  if self.aliases["dylek"] then
    killAlias(self.aliases["dylek"])
  end
  self.aliases["dylek"] = tempAlias("^/dylek(.*)$", function()
    local m = matches[2]
    if m == nil or m == "" then
      Msmudlet.transport:wait4dylek(0)
      return
    end
    local w = string.match(m, "^ (%d+)$")
    if w ~= nil or w ~= "" then
      Msmudlet.transport:wait4dylek(w)
    end
  end)


  if self.aliases["unstatek"] then
    killAlias(self.aliases["unstatek"])
  end
  self.aliases["unstatek"] = tempAlias("^/unstatek$", function()
    Msmudlet.transport:untransport()
  end)

  if self.aliases["undylek"] then
    killAlias(self.aliases["undylek"])
  end
  self.aliases["undylek"] = tempAlias("^/undylek$", function()
    Msmudlet.transport:untransport()
  end)

  if self.aliases["zejdz"] then
    killAlias(self.aliases["zejdz"])
  end

  self.aliases["zejdz"] = tempAlias("^/zejdz (\\d+)$", function()
    local m = tonumber(matches[2])

    if m ~= nil and m ~= "" and m > 0 then
      if m == 1 then
        cecho("\n<CadetBlue>(msmudlet)<yellow> Zsiąde na najbliższym przystanku.\n\n")
      else
        cecho("\n<CadetBlue>(msmudlet)<yellow> Zsiąde za " .. m .. " " .. Msmudlet.transport:przystanki(m) .. ".\n\n")
      end
      Msmudlet.transport:zejdz(m)
    else
      cecho("\n<CadetBlue>(msmudlet)<yellow> Nie rozumiem za ile przystanków mam zejść.\n\n")
    end
  end)
end

function Msmudlet.transport:wait4ship(stops) --
  stops = tonumber(stops)
  if stops == nil or stops == 0 then
    cecho("\n<CadetBlue>(msmudlet)<yellow> Wsiądę na najbliższy statek.\n\n")
  else
    if stops == 1 then
      cecho("\n<CadetBlue>(msmudlet)<yellow> Wsiądę na statek i zsiąde w najbliższym porcie.\n\n")
    else
      cecho("\n<CadetBlue>(msmudlet)<yellow> Wsiądę na najbliższy statek i zsiąde za " ..
        stops .. " " .. Msmudlet.transport:przystanki(stops) ..
        ".\n\n")
    end
  end
  if self.triggers["statek"] then
    killTrigger(self.triggers["statek"])
  end
  self.triggers["statek"] = tempRegexTrigger(
    "(Wszyscy na poklad!.*|([Tt]ratwa|[Pp]rom) przybija do brzegu\\.$)", function()
      cecho("\n<CadetBlue>(msmudlet)<yellow> Widzę statek! Wsiadam niebawem.\n\n")
      Msmudlet.autko:kolejkuj("execute_ship", "regular", function()
        scripts.utils.execute_ship()
      end, 1, false)
      killTrigger(self.triggers["statek"])
      self.triggers["statek"] = nil
    end)
  Msmudlet.transport:zejdz(stops)
end

function Msmudlet.transport:wait4dylek(stops)
  stops = tonumber(stops)
  if stops == nil or stops == 0 then
    cecho("\n<CadetBlue>(msmudlet)<yellow> Wsiądę do najbliższego dyliżansu.\n\n")
  else
    if stops == 1 then
      cecho("\n<CadetBlue>(msmudlet)<yellow> Wsiądę do dyliżansu i wysiądę na najbliszym postoju.\n\n")
    else
      cecho("\n<CadetBlue>(msmudlet)<yellow> Wejdę do dyliżansu i wysiądę za " ..
        stops .. " " .. Msmudlet.transport:przystanki(stops) .. ".\n\n")
    end
  end
  if self.triggers["dylek"] then
    killTrigger(self.triggers["dylek"])
  end
  self.triggers["dylek"] = tempRegexTrigger(
    "(dylizans|powoz) powoli zatrzymuje sie\\.$$", function()
      cecho("\n<CadetBlue>(msmudlet)<yellow> Przyjechał transport! Wsiadam niebawem.\n\n")
      Msmudlet.autko:kolejkuj("execute_ship", "regular", function()
        scripts.utils.execute_ship()
      end, 1, false)
      killTrigger(self.triggers["dylek"])
      self.triggers["dylek"] = nil
    end)
  Msmudlet.transport:zejdz(stops)
end

function Msmudlet.transport:zejdz(stops)
  if stops ~= nil and stops > 0 then
    self.stopsToExit = stops
    if self.triggers["zejdz"] then
      killTrigger(self.triggers["zejdz"])
    end
    self.triggers["zejdz"] = tempRegexTrigger("krzyczy: Doplynelismy .+ Mozna wysiadac!$", function()
      if Msmudlet.transport.stopsToExit == 1 then
        cecho("\n<CadetBlue>(msmudlet)<yellow> Jestem u celu! Zaraz wysiądę!\n\n")
        Msmudlet.autko:kolejkuj("execute_ship", "regular", function() scripts.utils.execute_ship() end, 1, false)
        for key, id in pairs(self.triggers) do
          killTrigger(id)
          self.triggers[key] = nil
        end
      else
        self:decrement()
      end
    end)
    if self.triggers["zejdzDylki"] then
      killTrigger(self.triggers["zejdzDylki"])
    end
    self.triggers["zejdzDylki"] = tempRegexTrigger(
      "(Woz cicho skrzypiac zatrzymuje sie\\.|Monotonne kolysanie ustaje w koncu i woz zatrzymuje sie\\.|Powoli pojazd zaczyna tracic predkosc, by po chwili zatrzymac sie\\.)$",
      function()
        if Msmudlet.transport.stopsToExit == 1 then
          cecho("\n<CadetBlue>(msmudlet)<yellow> Dojechaliśmy do celu! Zaraz wysiądę!\n\n")
          Msmudlet.autko:kolejkuj("execute_ship", "regular", function() scripts.utils.execute_ship() end, 1, false)
          for key, id in pairs(self.triggers) do
            killTrigger(id)
            self.triggers[key] = nil
          end
        else
          self:decrement()
        end
      end)
  end
end

function Msmudlet.transport:untransport()
  for key, id in pairs(self.triggers) do
    if key == "statek" then
      cecho("\n<CadetBlue>(msmudlet)<yellow> Nie wsiądę sam na statek.\n\n")
    elseif key == "zejdz" then
      cecho("\n<CadetBlue>(msmudlet)<yellow> Nie będę samodzielnie zsiadać.\n\n")
    elseif key == "zejdzDylki" then
      cecho("\n<CadetBlue>(msmudlet)<yellow> Nie wsiądę sam do dyliżansu.\n\n")
    end

    killTrigger(id)
    self.triggers[key] = nil
  end
end

function Msmudlet.transport:przystanki(n)
  n = tonumber(n)
  if n == nil then
    return ("przystankow")
  end
  local mstops = math.fmod(n, 10)
  if n == 1 then
    return ("przystanek")
  elseif (n > 1 and n < 5) or (n > 20 and mstops >= 2 and mstops <= 4) then
    return ("przystanki")
  else
    return ("przystanków")
  end
end

function Msmudlet.transport:decrement()
  Msmudlet.transport.stopsToExit = tonumber(Msmudlet.transport.stopsToExit) - 1
  if Msmudlet.transport.stopsToExit == 1 then
    cecho("\n<CadetBlue>(msmudlet)<yellow> Jeszcze nie jestem u celu. Wysiądę na następnym przystanku.\n\n")
  else
    cecho("\n<CadetBlue>(msmudlet)<yellow> Jeszcze nie jestem u celu. Wysiądę za " ..
      Msmudlet.transport.stopsToExit .. " " .. Msmudlet.transport:przystanki(Msmudlet.transport.stopsToExit) .. ".\n\n")
  end
end

Msmudlet.transport:init()
