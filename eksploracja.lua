Msmudlet.eksploracja = Msmudlet.eksploracja or {
	triggers = {},
	aliases = {},
	handlers = {},
	dfn = Msmudlet.dane.data_dir .. "/wiedza.json",
	rodzaje = nil,
	data = nil,
	active = false,
	dataVersion = nil
}



function Msmudlet.eksploracja:load()
	local df = io.open(self.dfn)
	if df then
		local tmp = yajl.to_value(df:read("*a"))
		io.close(df)
		if tmp then
			self.rodzaje = tmp.rodzaje
			self.data = tmp.wiedza
			self.dataVersion = tmp.version
			self.active = true
		end
	else
		self.active = false
	end
end

function Msmudlet.eksploracja:init()
	self:load()
	if self.aliases.help then killAlias(self.aliases.help) end
	self.aliases.help = tempAlias("^/wiedza$", function()
		Msmudlet.eksploracja:help()
	end)
	if self.aliases.o then killAlias(self.aliases.o) end
	self.aliases.o = tempAlias("^/wiedza o (.+)$", function()
		Msmudlet.eksploracja:wiedza(matches[2])
	end)
end

Msmudlet.eksploracja:init()

function Msmudlet.eksploracja:szczegoly(rodzaj)
	if self.handlers.eksploracja then killAnonymousEventHandler(self.handlers.eksploracja) end
	self.handlers.eksploracja = registerAnonymousEventHandler("gmcp.gmcp_msgs",
		function() Msmudlet.eksploracja:parsuj(rodzaj) end, false)
	if self.triggers.gag1 then killTrigger(self.triggers.gag1) end
	if self.triggers.gag2 then killTrigger(self.triggers.gag2) end
	self.triggers.gag1 = tempRegexTrigger("\\* [A-Z]+", function() deleteLine() end)
	self.triggers.gag2 = tempRegexTrigger("^Szczegoly eksploracji:$", function() deleteLine() end, 1)
	tempTimer(0.7, function()
		if self.triggers.gag1 then
			killTrigger(Msmudlet.eksploracja.triggers.gag1)
		end
		if self.triggers.gag2 then
			killTrigger(Msmudlet.eksploracja.triggers.gag2)
		end
	end)
	send("wiedza o " .. rodzaj)
end

function Msmudlet.eksploracja:parsuj(rodzaj)
	local dec = Msmudlet.lib:base64decode(gmcp.gmcp_msgs.text) -- rozpakowany tekst z listą eventów z eksploracji
	if rodzaj == "chaosie i jego tworach" then
---@diagnostic disable-next-line: undefined-field
		rodzaj = string.title(rodzaj)
	end

	if string.match(dec, "Wiedza o " .. rodzaj) then
		if scripts.character.info.gender then
			if scripts.character.info.gender == "male" then
				self.gender = "M"
			else
				self.gender = "F"
			end
		end
		if self.handlers.eksploracja then
			killAnonymousEventHandler(self.handlers.eksploracja)
			self.handlers.eksploracja = nil
		end
		if self.triggers.gag1 then
			killTrigger(self.triggers.gag1)
			self.triggers.gag1 = nil
		end
		if self.triggers.gag2 then
			killTrigger(self.triggers.gag2)
			self.triggers.gag2 = nil
		end

		local f, _ = string.find(dec, "%*")
		local wsad = string.sub(dec, f)
		local znalezioneEventy = {}
		for k in string.gmatch(wsad, "* ([A-Za-z ,%-%']-)%.") do
			table.insert(znalezioneEventy, k)
		end
		local function idRodzaju(listR, ro)
			for _, r in ipairs(listR) do
				if r.przypadki.miejscownik == ro then
					return r.id
				end
			end
		end

		local nrRodzaju = idRodzaju(self.rodzaje, rodzaj)
		if nrRodzaju == nil then
			return
		end



		local eventy = {}
		for _, punkt in pairs(self.data) do
			if punkt.rodzaj == nrRodzaju then
				local e = {}
---@diagnostic disable-next-line: undefined-field
				if table.contains(znalezioneEventy, punkt[self.gender]) then
					e.done = true
				else
					e.done = false
					e.trudnosc = punkt.trudnosc
					e.opis = punkt.opis
				end
				if punkt.locationId then
					e.locationId = punkt.locationId
				end
---@diagnostic disable-next-line: undefined-field
				if punkt.zoneId and not table.is_empty(punkt.zoneId) then
					e.zoneId = punkt.zoneId
				end
				e.isInDb = true
				eventy[punkt[self.gender]] = e
			end
		end

		local function isInEventy(ev, pu)
			for p, _ in pairs(ev) do
				if pu == p then
					return true
				end
			end
			return false
		end

		for _, punkt in pairs(znalezioneEventy) do
			if not isInEventy(eventy, punkt) then
				local e = {}
				e.done = true
				e.isInDb = false
				eventy[punkt] = e
			end
		end
		-- odkryte
		local odkryte = {}
		for n in pairs(eventy) do
			if eventy[n].done then
				table.insert(odkryte, n)
			end
		end
		table.sort(odkryte)
		-- braki
		local braki = {}
		for n in pairs(eventy) do
			if not eventy[n].done then
				table.insert(braki, n)
			end
		end
		table.sort(braki)

		cecho("Odkryte:\n")
		for i, event in ipairs(odkryte) do
			cecho(string.format("    <white>%2i: %s", i, event))
			if not eventy[event].isInDb == true then
				cecho("<grey> - brak w bazie SGW - ")
				scripts:print_url("<deep_sky_blue>zgłoś",
					function() openUrl("https://gitlab.com/sterowiec-sgw/sgw-dane/-/issues/new?issuable_template=dodaj-wiedze&issue[title]=Dodaj odkrycie: "
							.. event)
					end, "Zgłoś to odkrycie dla innych gnomów!")
				cecho("<grey> proszę")
			end
			echo("\n")
		end
		if braki and not table.is_empty(braki) then
			cecho("Brakujące:\n")
			for i, event in ipairs(braki) do
				local trudnosc = ""
				if eventy[event].trudnosc then
					trudnosc = eventy[event].trudnosc
				end
				local opis = ""
				if eventy[event].opis then
					opis = eventy[event].opis
				end
				cecho(string.format("    <white>%2i: ", i))
				cechoLink(string.format("[%3s]", trudnosc), "", "Poziom trudności", true)
				cechoLink(string.format(" <white>%s", event), "", opis, true)
				if eventy[event].locationId then
					if getPath(amap.curr.id, eventy[event].locationId) then
---@diagnostic disable-next-line: undefined-field
						local distance = table.size(speedWalkPath)
						if distance and distance > 0 then
							cechoLink(string.format(" - odległość <white>%s", distance), function()
								amap.path_display:start(eventy[event].locationId)
							end, "Pokaż ścieżkę do lokacji " .. eventy[event].locationId, true)
						end
					end
				end
				if eventy[event].zoneId and not table.is_empty(eventy[event].zoneId) then
					local inArea = false
					local currArea = getRoomArea(amap.curr.id)
					for _, z in ipairs(eventy[event].zoneId) do
						if z == currArea then
							inArea = true
							break
						end
					end
					if inArea then
						cechoLink(" [<green>R<grey>]", "", "Wiedzę znajdziesz w obecnym regionie mapy", true)
					else
						echo(" [ ")
						Areas = getAreaTableSwap()
						for _, v in pairs(eventy[event].zoneId) do
							cecho(Areas[v] .. " ")
						end
						Areas = nil
						echo("]")
					end
				end
				echo("\n")
			end
		end
	end
end

function Msmudlet.eksploracja:wiedza(rodzaj)
	Msmudlet.eksploracja:szczegoly(rodzaj)
end

function Msmudlet.eksploracja:help()
	cecho("+------------------------------------------------------------------------------+\n")
	cecho("|                                                                              |\n")
	cecho("| <yellow>SGW WIEDZA<grey>                                                                   |\n")
	cecho("|                                                                              |\n")
	cecho("| <light_slate_blue>/wiedza o <rodzaju wiedzy><grey> - status eksploracji danego <rodzaju wiedzy>.     |\n")
	cecho("|                                                                              |\n")
	cecho("|                                                                              |\n")
	cecho("|                                                                              |\n")
	cecho("+------------------------------------------------------------------------------+\n")
end
