---@diagnostic disable: undefined-field
Msmudlet.keypad = Msmudlet.keypad or {
  numeryk = {},
  keypadType = nil,
  aliases = {},
  keypadDefinitions = nil
}

function Msmudlet.keypad:init()
  Msmudlet.keypad:select_json() -- select and then load keypad definitions
  Msmudlet.keypad:load()
  for _, a in pairs(self.aliases) do killAlias(a) end

  Msmudlet.keypad:set_keys()

  local p = ""
  local i = 0
  for d, _ in pairs(Msmudlet.keypad.keypadDefinitions) do
    p = p .. d .. "|"
    i = i + 1
  end
  if i < 1 then
    cecho(
      "<CadetBlue>(sgw keypad)<grey> <firebrick>Brak definicji keypada! Nie mam jak zdefiniować klawiszy do poruszania się!\n")
    cecho(
    "<CadetBlue>(sgw keypad)<yellow> Pobierz sgw-dane komendą: /sgw_dane_pobierz, a następnie zrestartuj Mudleta.\n")
    return
  else
    p = p .. "off"
  end

  self.aliases.ustaw = tempAlias("^/msmudlet_keypad (" .. p .. ")$", function()
    self.keypadType = matches[2]
    Msmudlet.keypad:save()
    Msmudlet.keypad:set_keys()
    cecho(">> <yellow>KEYPAD<grey> - Obecny status: <light_slate_blue>" .. self.keypadType .. " <grey>\n\n")
  end)

  self.aliases.pokaz = tempAlias("^/msmudlet_keypad$", function()
    cecho("\n\n>> <yellow>KEYPAD<grey> - Obecny status: <light_slate_blue>" .. self.keypadType .. " <grey>\n\n")
    cecho("<yellow> Możesz zmienić ustawienie klawiszy poruszania się na następujące rodzaje:\n")
    local i = 1
    for d, o in pairs(Msmudlet.keypad.keypadDefinitions) do
      echo("  " .. i .. ". " .. d .. " - " .. o.description .. "\n")
      i = i + 1
    end
    echo("  --\n  Użycie: /msmudlet_keypad `typ`\n")
    echo("  Możesz też wyłączyć klawisze chodzenia ze skryptów SGW komendą: /msmudlet_keypad off\n\n")
  end)
end

function Msmudlet.keypad:select_json()
  self.keypadDefinitions = nil
  local default_keypads = Msmudlet.dane.data_dir .. "/keypads.json"
  local user_keypads = getMudletHomeDir() .. "/sgw_keypads.json"

  local uf, _, _ = io.open(user_keypads)
  if uf then
    Msmudlet.keypad:load_json(uf)
    return
  end

  local kf, s, contents = io.open(default_keypads)
  if kf then
    Msmudlet.keypad:load_json(kf)
  else
    cecho(string.format(
      "<CadetBlue>(sgw keypad)<grey> <firebrick>Otworzenie pliku z definicjami keypada nie powiodło się! Zgłoś błąd (https://gitlab.com/szydell/msmudlet/-/issues/new). Zawrzyj w nim to info -> %s to %s\n",
      s, contents))
  end
end

function Msmudlet.keypad:load_json(kf)
  local tmp = yajl.to_value(kf:read("*a"))
  io.close(kf)
  if tmp == nil then
    cecho(string.format(
      "<CadetBlue>(sgw keypad)<grey> <firebrick>Załadowanie pliku z definicjami keypada nie powiodło się!\n"))
    return
  end
  self.keypadDefinitions = tmp.keypads
end

function Msmudlet.keypad:is_keypad_defined(kn)
  for d, _ in pairs(Msmudlet.keypad.keypadDefinitions) do
    if d == kn then
      return true
    end
  end
  return false
end

function Msmudlet.keypad:set_keys()
  if Msmudlet.keypad.numeryk then
    for _, k in pairs(Msmudlet.keypad.numeryk) do killKey(k) end
  end
  Msmudlet.keypad.numeryk = nil
  Msmudlet.keypad.numeryk = {}

  if not Msmudlet.keypad:is_keypad_defined(self.keypadType) or self.keypadType == "off" then
    return
  end

  -- move
  for d, k in pairs(Msmudlet.keypad.keypadDefinitions[self.keypadType].move) do
    local m = 0
    if k.modifier_keypad then
      m = m + mudlet.keymodifier.Keypad
    end
    if k.modifier_alt then
      m = m + mudlet.keymodifier.Alt
    end
    if k.modifier_ctrl then
      m = m + mudlet.keymodifier.Control
    end

    local x = tempKey(m, mudlet.key[k.key], function() Msmudlet.keypad:move(d) end)
    table.insert(self.numeryk, x)
  end

  for d, k in pairs(Msmudlet.keypad.keypadDefinitions[self.keypadType].sneaky_move) do
    local m = 0
    if k.modifier_keypad then
      m = m + mudlet.keymodifier.Keypad
    end
    if k.modifier_alt then
      m = m + mudlet.keymodifier.Alt
    end
    if k.modifier_ctrl then
      m = m + mudlet.keymodifier.Control
    end

    local x = tempKey(m, mudlet.key[k.key], function() Msmudlet.keypad:sneaky_move(d) end)
    table.insert(self.numeryk, x)
  end

  for d, k in pairs(Msmudlet.keypad.keypadDefinitions[self.keypadType].sneaky_team_move) do
    local m = 0
    if k.modifier_keypad then
      m = m + mudlet.keymodifier.Keypad
    end
    if k.modifier_alt then
      m = m + mudlet.keymodifier.Alt
    end
    if k.modifier_ctrl then
      m = m + mudlet.keymodifier.Control
    end

    local x = tempKey(m, mudlet.key[k.key], function() Msmudlet.keypad:sneaky_team_move(d) end)
    table.insert(self.numeryk, x)
  end

end

function Msmudlet.keypad:save()
  scripts.state_store:set("keypad_type", self.keypadType)
end

function Msmudlet.keypad:load()
  local keypadType = scripts.state_store:get("keypad_type")
  if keypadType then
    if keypadType == "numon" or keypadType == "numoff" or keypadType == "off" then
      self.keypadType = keypadType
    else
      self.keypadType = "numoff"
      Msmudlet.keypad:save()
      Msmudlet.keypad:set_keys()
    end
  else
    self.keypadType = "numoff"
    Msmudlet.keypad:save()
  end
end

function Msmudlet.keypad:move(direction)
  if direction == "special1" then
    compass_click("special1")
    return
  end

  -- -- snekkar zejście
  -- if getPlayerRoom() == Msmudlet.mapy.statki.snekkar.wejscie and direction == "north" then
  -- 	send("zejdz ze statku")
  -- 	return
  -- end
  -- -- drakkar zejście
  -- if getPlayerRoom() == Msmudlet.mapy.statki.drakkar.wejscie and direction == "north" then
  -- 	send("zejdz ze statku")
  -- 	return
  -- end
  -- okręty wejscie
  if getPlayerRoom() == Msmudlet.mapy.siedliszcze.lokacje.nabrzeze_7 and direction == "west" then
    send("wejdz na okret")
    return
  end
  -- okręty wejscie
  if getPlayerRoom() == Msmudlet.mapy.siedliszcze.lokacje.nabrzeze_6 and direction == "south" then
    send("wejdz na okret")
    return
  end
  -- okręty wejscie
  if getPlayerRoom() == Msmudlet.mapy.siedliszcze.lokacje.nabrzeze_5 and direction == "south" then
    send("wejdz na okret")
    return
  end
  -- okręty wejscie
  if getPlayerRoom() == Msmudlet.mapy.siedliszcze.lokacje.nabrzeze_4 and direction == "south" then
    send("wejdz na okret")
    return
  end
  -- okręty wejscie
  if getPlayerRoom() == Msmudlet.mapy.siedliszcze.lokacje.nabrzeze_2 and direction == "south" then
    send("wejdz na okret")
    return
  end
  -- okręty wejscie
  if getPlayerRoom() == Msmudlet.mapy.siedliszcze.lokacje.nabrzeze_1 and direction == "south" then
    send("wejdz na okret")
    return
  end
  -- z Varieno furtka
  if getPlayerRoom() == 6030 and direction == "east" then
    send("przejdz przez furtke")
    return
  end
  -- do Varieno furtka
  if getPlayerRoom() == 10831 and direction == "west" then
    send("przejdz przez furtke")
    return
  end
  -- pastwiska Varieno na bagna
  if getPlayerRoom() == 10837 and direction == "down" then
    send("zejdz na dol")
    return
  end
  -- bagna Varieno na pastwisko
  if getPlayerRoom() == 17961 and direction == "up" then
    send("wejdz na gore")
    return
  end

  if Msmudlet.wedkarz.status == "on" then
    send("wyciagnij siec")
  end


  amap:keybind_pressed(direction)
end

function Msmudlet.keypad:sneaky_move(direction)
  local walk_mode = amap.walk_mode
  amap.walk_mode = 2
  amap:keybind_pressed(direction)
  amap.walk_mode = walk_mode
end

function Msmudlet.keypad:sneaky_team_move(direction)
  local walk_mode = amap.walk_mode
  ---@diagnostic disable-next-line: undefined-field
  if table.size(ateam.team) > 1 and ateam.is_leader(scripts.character_name) then
    amap.walk_mode = 3
  else
    amap.walk_mode = 2
  end
  amap:keybind_pressed(direction)
  amap.walk_mode = walk_mode
end

Msmudlet.keypad:init()
