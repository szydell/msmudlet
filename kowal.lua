function scripts.inv:kowal_try_bind_end()
	if not scripts.inv.kowal_working then
		if Msmudlet.autko.status == "on" then
			cecho("\n<" .. scripts.ui:get_bind_color_backward_compatible() .. ">Autko -> <" .. scripts.ui:get_bind_color_backward_compatible() .. "> Ubieram sie po naprawie\n")
			Msmudlet.autko:kolejkuj("ubieranie po kowalu", "regular", function() scripts.inv:put_into_bag({ "monety" }, "money", 1) send("zaloz tarcze;".. scripts.inv.weapons.wield ..";zaloz wszystkie zbroje") end, 1, true)
		else
			scripts.utils.bind_functional("wlm;zaloz tarcze;".. scripts.inv.weapons.wield ..";zaloz wszystkie zbroje",false, true)
		end
	end
	scripts.inv.kowal_timet_set = false
end


function trigger_func_skrypty_inventory_kowal_konczy_prace()
	if scripts.inv.kowal_working then
			scripts.inv.kowal_waiting = nil
			if Msmudlet.autko.status == "on" then
				cecho("\n<" .. scripts.ui:get_bind_color_backward_compatible() .. ">Autko -> <" .. scripts.ui:get_bind_color_backward_compatible() .. "> Naprawiam dalej\n")
				Msmudlet.autko:kolejkuj("kowal", "regular", "naostrz wszystkie bronie;napraw wszystkie zbroje", 1, true)
			else
				scripts.utils.bind_functional("naostrz wszystkie bronie;napraw wszystkie zbroje",false, true)
			end
			scripts.inv.kowal_working = false
			scripts.inv.kowal_timet_set = false
	end
end