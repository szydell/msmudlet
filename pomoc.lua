-- Pomocy!

Msmudlet.pomoc = Msmudlet.pomoc or {
	aliases = {}
}


function Msmudlet.pomoc:show()
	cecho("+---------------------------- <green>plugin msmudlet, " ..
		string.sub(Msmudlet.version .. "   ", 0, 7) .. "<grey> -----------------------------+\n")
	if Msmudlet.dane.version and Msmudlet.dane.version ~= "1.0.0" then
		cecho("+------------------------------- <green>SGW dane, " ..
			string.sub(Msmudlet.dane.version .. "   ", 0, 7) .. "<grey> ---------------------------------+\n")
	end
	cecho("|                                                                                   |\n")
	cecho("| <yellow>OGOLNE<grey>                                                                            |\n")
	cecho("|                                                                                   |\n")
	cecho("| <light_slate_blue>/buk<grey> - napij się z bukłaka.                                                       |\n")
	cecho("| <light_slate_blue>/cial<grey> - pokaz co jest we wszystkich cialach na lokacji.                           |\n")
	cecho("| <light_slate_blue>/dpl <co><grey> - wloz rzeczy do odpowiedniego pojemnika.                               |\n")
	cecho("| <light_slate_blue>/kokony<grey> - rozrywa wszystkie kokony.                                               |\n")
	cecho("| <light_slate_blue>/plecak<grey> - sprawdź zawartość plecaka.                                              |\n")
	cecho("| <light_slate_blue>/porownaj <z_kim><grey> - porównaj bojówki.                                             |\n")
	cecho("| <light_slate_blue>/torba<grey> - sprawdź zawartość torby.                                                 |\n")
	cecho("| <light_slate_blue>/sakiewka<grey> - sprawdź zawartość sakiewki.                                           |\n")
	cecho("| <light_slate_blue>/sakwa<grey> - sprawdź zawartość sakwy.                                                 |\n")
	cecho("| <light_slate_blue>/wedkarz<grey> - pomoc do lowienia ryb.                                                 |\n")
	cecho("| <light_slate_blue>/wybierz <co><grey> - przy szafie, czy stojakach znajdzie najlepszy sprzet.             |\n")
	cecho("|                <grey> składnia tego co szukamy analogiczna do komendy przejrzyj         |\n")
	cecho("|                <grey> Przykładowo:                                                      |\n")
	cecho("|                <grey> /wybierz jednoreczne mloty                                        |\n")
	cecho("|                <grey> /wybierz ciezkie zbroje chroniace korpus                          |\n")
	cecho("| <light_slate_blue>/zbierz <co><grey> - zbierz <co> ze wszystkich cial.                                    |\n")
	cecho("| <light_slate_blue>/zpl <co><grey> - wez rzeczy z odpowiedniego pojemnika.                                 |\n")
	cecho("|                                                                                   |\n")
	cecho("| <yellow>WAiS<grey>                                                                              |\n")
	cecho("|                                                                                   |\n")
	cecho("| <light_slate_blue>/kompas<grey> - sprawdź kierunek.                                                       |\n")
	cecho("| <light_slate_blue>/pudelko 1234567890123456<grey> - rozwiąż problem pudełka (podaj 16 wierzchołków).      |\n")
	cecho("| <light_slate_blue>/obexp <co><grey> - nabij obexpa poprzez ogladanie roznych rzeczy na lokacji.           |\n")
	cecho("| <light_slate_blue>wiedza<grey> - stan zdobytej wiedzy rozszerzony o dokładny %.                           |\n")
	if Msmudlet.dane.version and Msmudlet.dane.version ~= "1.0.0" then
		cecho("| <light_slate_blue>/wiedza o <rodzaju wiedzy><grey> - status eksploracji danego <rodzaju wiedzy>.          |\n")
	end
	cecho("| <light_slate_blue>/wiedza_ustaw <typ>=<wartość><grey> - ustaw ile % wiedzy masz.                          |\n")
	cecho("| <light_slate_blue>/zegar_golemow 123456789012<grey> - rozwiąż problem zegara (podaj 12 wierzchołków).     |\n")
	cecho("| <light_slate_blue>/zio1<grey> - spakuj wszystkie ziola do woreczkow.                                      |\n")
	cecho("| <light_slate_blue>/zio0<grey> - wypakuj wszystkie ziola z woreczkow.                                      |\n")
	if Msmudlet.dane.version and Msmudlet.dane.version ~= "1.0.0" then
		cecho("| <light_slate_blue>/ziola_raport<grey>   - wiecej o twoich ziolach                                         |\n")
	  cecho("| <light_slate_blue>/ziola_sprzedaj<grey> - sprzedaj niepotrzebne ziola. Wczesniej koniecznie /ziola_buduj. |\n")
	  cecho("| <light_slate_blue>/ziola_przygotuj<grey> - wyjmij niepotrzebne ziola. Wczesniej koniecznie /ziola_buduj.  |\n")
	  cecho("|      Indywidualne preferencje sprzedazy ustawisz w /ziola_raport.                 |\n")
  end
	cecho("|                                                                                   |\n")
	cecho("| <yellow>WAU<grey>                                                                               |\n")
	cecho("|                                                                                   |\n")
	cecho("| <light_slate_blue>/autko_pomoc<grey> - pomoc do automatów                                                 |\n")
	cecho("| <light_slate_blue>/ethel<grey> - wyświetlenie wszystkich punktów wiedzy do zasilenia rankingu             |\n")
	cecho("| <light_slate_blue>/exptarget<grey> - wyświetl obecny stan nieznaków do celu                               |\n")
	cecho("| <light_slate_blue>/exptarget <ile><grey> - ustaw ile nieznaków mam odliczyć                               |\n")
	cecho("| <light_slate_blue>/exptarget <ile> <zrobionych><grey> - ile nieznakow chcesz odliczyć i ile z nich masz   |\n")
	cecho("| <light_slate_blue>/gnzbuduj<grey> - pomoc do hurtowej produkcji wynalazków                                |\n")
	cecho("| <light_slate_blue>/klucznicy<grey> - lista znanych kluczników                                             |\n")
	cecho("| <light_slate_blue>/kufer_pokaz<grey> - wypakuj kufer z flakami (oprowadzanie)                             |\n")
	cecho("| <light_slate_blue>/kufer_spakuj<grey> - spakuj kufer z flakami                                            |\n")
	cecho("|                                                                                   |\n")
	cecho("| <light_slate_blue>/sgw_inicjalizuj <token><grey> - dostęp do danych SGW ONLY.                             |\n")
	if Msmudlet.dane.version and Msmudlet.dane.version ~= "1.0.0" then
		cecho("| <light_slate_blue>/sgw_dane_sprawdz<grey> - sprawdź czy jest update danych.                               |\n")
		cecho("| <light_slate_blue>/sgw_dane_pobierz<grey> - ściągnij i załaduj aktualne dane.                             |\n")
	end
	cecho("|                                                                                   |\n")
	cecho("| <light_slate_blue>/statek<grey> - wsiądź na najbliższy statek/prom/tratwe.                                |\n")
	cecho("| <light_slate_blue>/statek 2<grey> - wsiądź na najbliższy statek i zejdz na drugim przystanku.             |\n")
	cecho("| <light_slate_blue>/dylek<grey> - poczekaj i wsiądź do dyliżansu.                                          |\n")
	cecho("| <light_slate_blue>/dylek 3<grey> - wsiądź na dyliżans i wysiądź na 3 przystanku.                          |\n")
	cecho("| <light_slate_blue>/zejdz 1<grey> - gdy jesteś na statku/w dyliżansie, zejdź na najbliższym przystanku.    |\n")
	cecho("| <light_slate_blue>/unstatek<grey> - zrezygnuj z wsiadania i/lub zsiadania.                                |\n")
	cecho("| <light_slate_blue>/undylek<grey> - zrezygnuj z wsiadania i/lub zsiadania.                                 |\n")
	cecho("|                                                                                   |\n")
	cecho("| <light_slate_blue>/msmudlet_keypad numon<grey> - ruszaj się z włączonym numlockiem.                       |\n")
	cecho("| <light_slate_blue>/msmudlet_keypad numoff<grey> - ruszaj się z wyłączonym numlockiem.                     |\n")
	cecho("| <light_slate_blue>/msmudlet_keypad off<grey> - nie binduj klawiszy na klawiaturze numerycznej.            |\n")
	cecho("| <light_slate_blue>/msmudlet_keypad<grey> - wyświetl obecnie ustawiony sposób bindowania.                  |\n")
	cecho("|                                                                                   |\n")
	cecho("| <light_slate_blue>/msmudlet_sprawdz<grey> - sprawdź czy jest update skryptów.                             |\n")
	cecho("|                                                                                   |\n")
	cecho("| <light_slate_blue>/msmudlet_dev<grey> - ładuje skrypty z lokalnego repozytorium                           |\n")
	cecho("|               <grey>(używaj tylko gdy samodzielnie piszesz poprawki).                   |\n")
	cecho("|                                                                                   |\n")
	cecho("+-----------------------------------------------------------------------------------+\n")
end

function Msmudlet.pomoc:autko()
	cecho("+---------------------------- <green>plugin msmudlet, " ..
		string.sub(Msmudlet.version .. "   ", 0, 7) .. "<grey> -----------------------------+\n")
	cecho("|                                                                                   |\n")
	cecho("| <yellow>AUTKO<grey>                                                                             |\n")
	cecho("|                                                                                   |\n")
	cecho("|   Autko jest systemem kolejkowania komend. Każde kolejkowane zadanie ma zdefinio- |\n")
	cecho("| wany zakres czasu w którym ma się wykonać, oraz informację czy ma obowiązywać po  |\n")
	cecho("| przejściu na inną lokację. Możesz wymusić zignorowanie czasu opóźnienia i rozła-  |\n")
	cecho("| dować kolejkę manualnie.                                                          |\n")
	cecho("| Wszystkie automaty wymagają włączenia autka do działania.                         |\n")
	cecho("| W niektórych sytuacjach autko może wyłączyć się samo.                             |\n")
	cecho("|                                                                                   |\n")
	cecho("| <light_slate_blue>/autko<grey>, <light_slate_blue>/autko_on<grey>, <light_slate_blue>/autko_off<grey> - włącz / wyłącz autko                              |\n")
	cecho("| <light_slate_blue>/rozladuj_kolejke<grey> - rozładuj kolejkę manualnie                                    |\n")
	cecho("|                                                                                   |\n")
	cecho("| <light_slate_blue>/sledz<grey>, <light_slate_blue>/sledz_on<grey>, <light_slate_blue>/sledz_off<grey> - włącz / wyłącz śledzenie dowódcy drużyny          |\n")
	cecho("|                                                                                   |\n")
	cecho("| <light_slate_blue>/walcz<grey>, <light_slate_blue>/walcz_on<grey>, <light_slate_blue>/walcz_off<grey> - włącz / wyłącz automaty wspierania walki          |\n")
	cecho("| Walcz obejmuje obecnie funkcje: autodobywanie po wytrąceniu                       |\n")
	cecho("|                                                                                   |\n")
	cecho("| <light_slate_blue>/wspieraj<grey> - włącz / wyłącz wspieranie i atak na rozkaz dowódcy drużyny            |\n")
	cecho("| <light_slate_blue>/wspieraj_on<grey>, <light_slate_blue>/wspieraj_off<grey> - włącz / wyłącz wspieranie                           |\n")
	cecho("|                                                                                   |\n")
	cecho("| <light_slate_blue>/zaslaniaj<grey>, <light_slate_blue>/zaslaniaj_on<grey>, <light_slate_blue>/zaslaniaj_off<grey> - włącz / wyłącz zasłanianie            |\n")

	cecho("| Gdy automat wykryje, że masz zawód legionisty, będzie się również wycofywać.      |\n")
	cecho("|                                                                                   |\n")
	cecho("| <light_slate_blue>/grab<grey>, <light_slate_blue>/grab_on<grey>, <light_slate_blue>/grab_off<grey> - włącz / wyłącz autozbieranie                         |\n")
	cecho("| Co zbierać definiujemy za pomoca komendy <light_slate_blue>/zbieranie<grey>.                              |\n")
	cecho("|                                                                                   |\n")
	cecho("+-----------------------------------------------------------------------------------+\n")
end

function Msmudlet.pomoc:init()
	if self.aliases.pomoc then killAlias(self.aliases.pomoc) end
	self.aliases.pomoc = tempAlias("^/msmudlet_pomoc$", function()
		Msmudlet.pomoc:show()
	end)
	if self.aliases.msmudlet then killAlias(self.aliases.msmudlet) end
	self.aliases.msmudlet = tempAlias("^/msmudlet$", function()
		Msmudlet.pomoc:show()
	end)
	if self.aliases.autko then killAlias(self.aliases.autko) end
	self.aliases.autko = tempAlias("^/autko_pomoc$", function()
		Msmudlet.pomoc:autko()
	end)
end

Msmudlet.pomoc:init()

function Msmudlet.pomoc:start()
	cecho("\n\n<CadetBlue>(msmudlet)<yellow>: Pomoc do pluginu msmudlet - /msmudlet_pomoc\n\n")
end

tempTimer(7, function() Msmudlet.pomoc:start() end)
