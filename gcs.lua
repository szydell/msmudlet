-- gcs.lua - Script for GCS (Gnome Communication Services) for Arkadia
-- Module for communication with GCS server

Msmudlet = Msmudlet or {}

-- First initialize the basic structure if it doesn't exist
Msmudlet.gcs = Msmudlet.gcs or {
    sessionToken = nil, -- Session token
    gitlabToken = "",   -- GitLab token (will be loaded from file)
    characterName = "", -- Character name (will be detected automatically)
    connected = false,  -- Connection status
    discoveries = {},   -- List of discoveries
    handlers = {},      -- Event handler IDs
    aliasy = {},        -- Alias IDs
    triggers = {},      -- Trigger IDs
    timers = {},        -- Timer IDs
    systemData = {}     -- Data from system command
}

-- Static values that should always be updated on script reload
Msmudlet.gcs.host = "https://gcs.sgw.equipment" -- GCS server address
Msmudlet.gcs.apoID = nil                        -- Current apocalypse identifier

-- Initialize containers if they don't exist
Msmudlet.gcs.handlers = Msmudlet.gcs.handlers or {}
Msmudlet.gcs.aliasy = Msmudlet.gcs.aliasy or {}
Msmudlet.gcs.triggers = Msmudlet.gcs.triggers or {}
Msmudlet.gcs.timers = Msmudlet.gcs.timers or {}

-- Function to clean up module resources
function Msmudlet.gcs:cleanupResources()
    -- Make sure all containers are initialized
    self.aliasy = self.aliasy or {}
    self.handlers = self.handlers or {}
    self.triggers = self.triggers or {}
    self.timers = self.timers or {}

    -- Remove all aliases
    for name, alias_id in pairs(self.aliasy) do
        if alias_id then
            killAlias(alias_id)
            self.aliasy[name] = nil
        end
    end

    -- Remove all event handlers
    for name, handler_id in pairs(self.handlers) do
        if handler_id then
            killAnonymousEventHandler(handler_id)
            self.handlers[name] = nil
        end
    end

    -- Remove all triggers
    for name, trigger_id in pairs(self.triggers) do
        if trigger_id then
            killTrigger(trigger_id)
            self.triggers[name] = nil
        end
    end

    -- Remove all timers
    for name, timer_id in pairs(self.timers) do
        if timer_id then
            killTimer(timer_id)
            self.timers[name] = nil
        end
    end
end

-- Displays help for the GCS module
function Msmudlet.gcs:help()
    cecho("<CadetBlue>(sgw gcs)<green>: === Gnome Communication Services - Pomoc ===\n")
    cecho(
        "<CadetBlue>(sgw gcs)<white>: /gcs connect<grey> - Połączenie z serwerem GCS (konfiguracja, uwierzytelnienie, apoID)\n")
    --cecho("<CadetBlue>(sgw gcs)<white>: /gcs list<grey> - Wyświetlenie listy znalezisk\n")
    --cecho("<CadetBlue>(sgw gcs)<white>: /gcs refresh<grey> - Odświeżenie listy znalezisk z serwera\n")
    --cecho("<CadetBlue>(sgw gcs)<white>: /gcs report <opis> w <lokacja><grey> - Zgłoszenie znaleziska\n")
    cecho("<CadetBlue>(sgw gcs)<white>: @<odbiorca> <wiadomość><grey> - Wysłanie wiadomości do gracza\n")
    cecho("<CadetBlue>(sgw gcs)<white>: /gcs disconnect<grey> - Rozłączenie z serwerem GCS\n")
    cecho("<CadetBlue>(sgw gcs)<green>: ========================================\n")
end

-- Load token from config file
function Msmudlet.gcs:loadConfig()
    local sgw_config = io.open(getMudletHomeDir() .. "/sgw.json")
    if sgw_config then
        local config_raw = sgw_config:read("*a")
        sgw_config:close()

        if config_raw ~= "" then
            local tmp = yajl.to_value(config_raw)
            if tmp and tmp.token then
                self.gitlabToken = tmp.token
                -- cecho("<CadetBlue>(sgw gcs)<green>: Wczytano token GitLab z pliku konfiguracyjnego.\n")
                return true
            end
        end
    end

    cecho(
        "<CadetBlue>(sgw gcs)<tomato>: Nie znaleziono tokenu GitLab w pliku konfiguracyjnym. Sprawdź <white>/msmudlet_pomoc<tomato> i stronę <white>https://gitlab.com/szydell/msmudlet/-/wikis/SGW%20ONLY<tomato>.\n")
    return false
end

-- Function to normalize player name (first letter uppercase, rest lowercase)
function Msmudlet.gcs:normalizePlayerName(name)
    if not name or name == "" then
        return name
    end

    -- Convert the entire name to lowercase, then capitalize the first letter
    return name:sub(1, 1):upper() .. name:sub(2):lower()
end

-- Function to get normalized player identifier
function Msmudlet.gcs:getPlayerIdentifier()
    local name = nil

    -- 1. Check scripts.character_name variable (priority)
    if scripts and scripts.character_name and scripts.character_name ~= "" then
        name = scripts.character_name
        -- 2. Check GMCP as fallback
    elseif gmcp and gmcp.char and gmcp.char.info and gmcp.char.info.name then
        name = gmcp.char.info.name
        -- 3. If we have previously saved player name in this module, use it
    elseif self.characterName and self.characterName ~= "" then
        name = self.characterName
        -- 4. Use default name as last resort
    else
        name = "UnknownPlayer"
    end

    -- Normalize name to format with first capital letter
    return self:normalizePlayerName(name)
end

-- Function to connect to GCS server
function Msmudlet.gcs:connect()
    cecho("<CadetBlue>(sgw gcs)<grey>: Łączę z GCS...\n")
    -- Reset connection state
    self.connected = false

    -- Step 1: Get character name automatically
    self.characterName = self:getPlayerIdentifier()

    if self.characterName == "UnknownPlayer" then
        cecho("<CadetBlue>(sgw gcs)<tomato>: Nie udało się automatycznie ustalić imienia postaci.\n")
        cecho("<CadetBlue>(sgw gcs)<tomato>: Zgłoś błąd w dziale pomocy SGW.\n")
        return false
    end

    -- Step 2: Load token from file
    if not self:loadConfig() then
        return false
    end

    -- Step 3: Get apocalypse ID (asynchronously)
    self:getApoID()

    -- Step 4: Authenticate (doesn't wait for apoID)
    self:authenticate()

    Msmudlet.toolbar:updateGcsStatusFromConnectionState()

    return true
end

-- Function to retrieve apocalypse ID
function Msmudlet.gcs:getApoID()
    -- Return early if apoID is already set
    if self.apoID and self.apoID ~= "" then
        -- cecho("<CadetBlue>(sgw gcs)<grey>: Apocalypse ID already known: <white>" .. self.apoID .. "<grey>\n")
        return
    end

    cecho("<CadetBlue>(sgw gcs)<grey>: Wysyłam komendę 'system' do gry, aby ustalić kiedy ostatnio byla apokalipsa...\n")

    -- Remove existing handler if exists
    if self.handlers.get_apo_id then
        killAnonymousEventHandler(self.handlers.get_apo_id)
        self.handlers.get_apo_id = nil
    end

    -- Register handler in standard way, like other functions
    self.handlers.get_apo_id = registerAnonymousEventHandler("gmcp.gmcp_msgs", function()
        local btext = gmcp.gmcp_msgs.text
        local text = Msmudlet.lib:base64decode(btext)

        -- Look for the world rebirth line
        local dateMatch = string.match(text, "Swiat odrodzil sie%s+:%s+([^%\n]+)")

        if dateMatch then
            -- Extract date components
            local dayOfWeek, day, month, year, hour, min, sec =
                string.match(dateMatch, "([^,]+),%s+(%d+)%s+([^%s]+)%s+(%d+),%s+(%d+):(%d+):(%d+)")

            -- Map Roman months to numbers
            local monthMap = { I = 1, II = 2, III = 3, IV = 4, V = 5, VI = 6, VII = 7, VIII = 8, IX = 9, X = 10, XI = 11, XII = 12 }
            local monthNum = monthMap[month] or 1

            -- Create canonical date format: YYYYMMDDHHMMSS
            local canonicalDate = string.format("%04d%02d%02d%02d%02d%02d",
                tonumber(year), monthNum, tonumber(day),
                tonumber(hour), tonumber(min), tonumber(sec))

            -- Save as apoID
            self.apoID = canonicalDate

            -- Display information
            cecho("<CadetBlue>(sgw gcs)<green>: Ustalono czas apokalipsy: <white>" ..
                self.apoID .. "<green>.\n")

            -- Update interface state
            Msmudlet.toolbar:updateGcsStatusFromConnectionState()

            -- End listening - remove handler
            killAnonymousEventHandler(self.handlers.get_apo_id)
            self.handlers.get_apo_id = nil
        end
    end)

    -- Add gagging for system lines
    if self.triggers.system_gag1 then killTrigger(self.triggers.system_gag1) end
    if self.triggers.system_gag2 then killTrigger(self.triggers.system_gag2) end
    if self.triggers.system_gag3 then killTrigger(self.triggers.system_gag3) end
    if self.triggers.system_gag4 then killTrigger(self.triggers.system_gag4) end
    if self.triggers.system_gag5 then killTrigger(self.triggers.system_gag5) end

    self.triggers.system_gag1 = tempRegexTrigger("^system$", function() deleteLine() end, 1)
    self.triggers.system_gag2 = tempRegexTrigger("Swiat odrodzil sie\\s+:\\s+.+", function() deleteLine() end, 1)
    self.triggers.system_gag3 = tempRegexTrigger("Lokalny czas\\s+:\\s+.+", function() deleteLine() end, 1)
    self.triggers.system_gag4 = tempRegexTrigger("Swiat istnieje\\s+:\\s+.+", function() deleteLine() end, 1)
    self.triggers.system_gag5 = tempRegexTrigger("\\d+% swiata zostalo opanowane przez Ciemnosc.",
        function() deleteLine() end, 1)

    -- Automatically disable triggers after 2 seconds
    tempTimer(2, function()
        if self.triggers.system_gag1 then killTrigger(self.triggers.system_gag1) end
        if self.triggers.system_gag2 then killTrigger(self.triggers.system_gag2) end
        if self.triggers.system_gag3 then killTrigger(self.triggers.system_gag3) end
        if self.triggers.system_gag4 then killTrigger(self.triggers.system_gag4) end
        if self.triggers.system_gag5 then killTrigger(self.triggers.system_gag5) end
    end)

    -- Send system command
    send("system")
end

-- Authentication function (without apoID)
function Msmudlet.gcs:authenticate()
    if self.gitlabToken == "" then
        cecho(
            "<CadetBlue>(sgw gcs)<tomato>: Brak tokenu GitLab. Aby ściągnąć sgw-dane, musisz mieć zainicjalizowany dostęp do danych. Sprawdź <white>/msmudlet_pomoc<tomato> i stronę <white>https://gitlab.com/szydell/msmudlet/-/wikis/SGW%20ONLY<tomato>.\n")
        return false
    end

    if self.characterName == "" then
        cecho(
            "<CadetBlue>(sgw gcs)<tomato>: Nie wykryto nazwy postaci. Zgłoś się po pomoc do helpdesku SGW.\n")
        return false
    end

    local payload = {
        gitlab_token = self.gitlabToken,
        character_name = self.characterName
    }

    local url = self.host .. "/api/auth"

    -- Dodaj diagnostykę
    cecho("<CadetBlue>(sgw gcs)<grey>: Próba połączenia z: <white>" .. url .. "<grey>\n")
    -- cecho("<CadetBlue>(sgw gcs)<grey>: Wysyłane dane: <white>" .. yajl.to_string(payload) .. "<grey>\n")

    -- Remove old handlers if they exist
    if self.handlers.auth_done then killAnonymousEventHandler(self.handlers.auth_done) end
    if self.handlers.auth_error then killAnonymousEventHandler(self.handlers.auth_error) end

    -- Register event handlers
    self.handlers.auth_done = registerAnonymousEventHandler("sysPostHttpDone", function(event, rurl, response)
        if rurl ~= url then return true end

        local data = yajl.to_value(response)
        if data and data.token then
            Msmudlet.gcs.sessionToken = data.token
            Msmudlet.gcs.connected = true
            cecho("<CadetBlue>(sgw gcs)<green>: Połączono z serwerem GCS! Token sesji utworzony.\n")

            Msmudlet.toolbar:updateGcsStatusFromConnectionState()

            -- Start listening
            Msmudlet.gcs:startPolling()

            -- Wait for apoID before getting data
            -- tempTimer(1, function()
            --     if Msmudlet.gcs.apoID and Msmudlet.gcs.apoID ~= "" then
            --         Msmudlet.gcs:fetchDiscoveries()
            --     else
            --         cecho("<CadetBlue>(sgw gcs)<yellow>: Czekam na ustalenie ID apokalipsy przed pobraniem danych...\n")
            --         -- Próbuj ponownie za 1 sekundę
            --         tempTimer(10, function()
            --             if Msmudlet.gcs.apoID and Msmudlet.gcs.apoID ~= "" then
            --                 Msmudlet.gcs:fetchDiscoveries()
            --             else
            --                 cecho(
            --                     "<CadetBlue>(sgw gcs)<tomato>: Nie udało się ustalić ID apokalipsy. Dane mogą być niekompletne.\n")
            --             end
            --         end)
            --     end
            -- end)
        else
            cecho("<CadetBlue>(sgw gcs)<tomato>: Nieprawidłowa odpowiedź serwera podczas uwierzytelniania.\n")
            Msmudlet.gcs.connected = false
        end
    end, true)

    self.handlers.auth_error = registerAnonymousEventHandler("sysPostHttpError", function(event, error, rurl)
        if rurl ~= url then return true end

        cecho("<CadetBlue>(sgw gcs)<tomato>: Błąd uwierzytelniania: <white>" .. error .. "<tomato>.\n")
        Msmudlet.gcs.connected = false
    end, true)

    -- Send authentication request
    postHTTP(yajl.to_string(payload), url)

    return true
end

-- Starts listening for messages from server (long polling)
function Msmudlet.gcs:startPolling()
    if not self.connected then
        cecho("<CadetBlue>(sgw gcs)<tomato>: Nie połączono z serwerem. Uruchom najpierw <white>/gcs connect<tomato>.\n")
        return
    end

    local url = self.host .. "/api/poll"
    local headers = {
        ["X-Player-ID"] = self.characterName,
        ["X-Session-Token"] = self.sessionToken
    }

    -- Remove old handler if exists
    if self.handlers.poll_done then killAnonymousEventHandler(self.handlers.poll_done) end
    if self.handlers.poll_error then killAnonymousEventHandler(self.handlers.poll_error) end

    -- Register event handlers
    self.handlers.poll_done = registerAnonymousEventHandler("sysGetHttpDone", function(event, rurl, response)
        if rurl ~= url then return true end

        local data = yajl.to_value(response)
        if data then
            -- Handle different event types
            if data.type == "message" then
                cecho("<CadetBlue>(sgw gcs)<cyan>: [" .. data.sender .. "]: " .. data.content .. "\n")
            elseif data.type == "discovery" then
                cecho("<CadetBlue>(sgw gcs)<light_green>: [Znalezisko]: " .. data.content .. "\n")
            elseif data.type == "welcome" then
                cecho("<CadetBlue>(sgw gcs)<green>: " .. data.message .. "\n")
            end
            -- Don't display heartbeat, it's just to keep connection alive
        end

        -- Continue polling after brief pause
        Msmudlet.gcs.timers.poll = tempTimer(0.5, function()
            Msmudlet.gcs:startPolling()
        end)
    end, true)

    self.handlers.poll_error = registerAnonymousEventHandler("sysGetHttpError", function(event, error, rurl)
        if rurl ~= url then return true end

        cecho("<CadetBlue>(sgw gcs)<tomato>: Błąd pobierania wiadomości: <white>" .. error .. "<tomato>.\n")
        -- Retry after 5 seconds
        Msmudlet.gcs.timers.poll_retry = tempTimer(5, function()
            Msmudlet.gcs:startPolling()
        end)
    end, true)

    -- Send polling request
    getHTTP(url, headers)
end

-- Function to disconnect from GCS server
function Msmudlet.gcs:disconnect()
    -- Stop polling and remove timers
    for name, timer_id in pairs(self.timers) do
        if timer_id then
            killTimer(timer_id)
            self.timers[name] = nil
        end
    end

    -- Remove handlers related to connection
    for name, handler_id in pairs(self.handlers) do
        if handler_id and (string.find(name, "poll_") or string.find(name, "auth_")) then
            killAnonymousEventHandler(handler_id)
            self.handlers[name] = nil
        end
    end

    -- Reset connection state
    self.connected = false
    self.sessionToken = nil
    self.apoID = nil

    Msmudlet.toolbar:updateGcsStatusFromConnectionState()

    cecho("<CadetBlue>(sgw gcs)<green>: Rozłączono z serwerem GCS.\n")

    return true
end

-- Send message to other players
function Msmudlet.gcs:sendMessage(recipients, message)
    if not self.connected then
        cecho("<CadetBlue>(sgw gcs)<tomato>: Nie połączono z serwerem. Uruchom najpierw <white>/gcs connect<tomato>.\n")
        return false
    end

    if type(recipients) == "string" then
        recipients = { recipients } -- Convert single recipient to array
    end

    -- Normalize all recipient names
    for i, recipient in ipairs(recipients) do
        recipients[i] = self:normalizePlayerName(recipient)
    end


    local payload = {
        sender = self.characterName,
        recipients = recipients,
        message = message
    }

    local url = self.host .. "/api/send"
    local headers = {
        ["X-Session-Token"] = self.sessionToken
    }

    -- Remove old handlers if exist
    if self.handlers.send_done then killAnonymousEventHandler(self.handlers.send_done) end
    if self.handlers.send_error then killAnonymousEventHandler(self.handlers.send_error) end

    -- Register event handlers
    self.handlers.send_done = registerAnonymousEventHandler("sysPostHttpDone", function(event, rurl, response)
        if rurl ~= url then return true end

        local data = yajl.to_value(response)
        if data and data.success then
            cecho("<CadetBlue>(sgw gcs)<green>: Wiadomość wysłana pomyślnie.\n")
        else
            local errorMsg = "nieznany błąd"
            if data and type(data) == "table" then
                -- Try to find any field that might contain error information
                errorMsg = data.error or data.message or data.errorMessage or "nieznany błąd"
            end
            cecho("<CadetBlue>(sgw gcs)<tomato>: Problem z wysłaniem wiadomości: <white>" .. errorMsg .. "<tomato>.\n")
        end
    end, true)

    self.handlers.send_error = registerAnonymousEventHandler("sysPostHttpError", function(event, error, rurl)
        if rurl ~= url then return true end

        cecho("<CadetBlue>(sgw gcs)<tomato>: Błąd podczas wysyłania wiadomości: <white>" .. error .. "<tomato>.\n")
    end, true)

    -- Send request
    postHTTP(yajl.to_string(payload), url, headers)

    return true
end

-- Report discovery to server
function Msmudlet.gcs:reportDiscovery(locationID, description)
    if not self.connected then
        cecho("<CadetBlue>(sgw gcs)<tomato>: Nie połączono z serwerem. Uruchom najpierw <white>/gcs connect<tomato>.\n")
        return false
    end

    if not self.apoID then
        cecho("<CadetBlue>(sgw gcs)<tomato>: Brak identyfikatora apokalipsy. Użyj <white>/gcs connect<tomato>.\n")
        return false
    end

    local payload = {
        apo_id = self.apoID,
        location_id = locationID,
        description = description
    }

    local url = self.host .. "/api/discovery"
    local headers = {
        ["X-Session-Token"] = self.sessionToken
    }

    -- Remove old handlers if exist
    if self.handlers.discovery_done then killAnonymousEventHandler(self.handlers.discovery_done) end
    if self.handlers.discovery_error then killAnonymousEventHandler(self.handlers.discovery_error) end

    -- Register event handlers
    self.handlers.discovery_done = registerAnonymousEventHandler("sysPostHttpDone", function(event, rurl, response)
        if rurl ~= url then return true end

        local data = yajl.to_value(response)
        if data and data.id then
            cecho("<CadetBlue>(sgw gcs)<green>: Znalezisko zgłoszone pomyślnie (ID: <white>" .. data.id .. "<green>).\n")
        else
            cecho("<CadetBlue>(sgw gcs)<tomato>: Problem ze zgłoszeniem znaleziska.\n")
        end
    end, true)

    self.handlers.discovery_error = registerAnonymousEventHandler("sysPostHttpError", function(event, error, rurl)
        if rurl ~= url then return true end

        cecho("<CadetBlue>(sgw gcs)<tomato>: Błąd podczas zgłaszania znaleziska: <white>" .. error .. "<tomato>.\n")
    end, true)

    -- Send request
    postHTTP(yajl.to_string(payload), url, headers)

    return true
end

-- Fetch discoveries list for current apoID
function Msmudlet.gcs:fetchDiscoveries()
    if not self.connected then
        cecho("<CadetBlue>(sgw gcs)<tomato>: Nie połączono z serwerem. Uruchom najpierw <white>/gcs connect<tomato>.\n")
        return
    end

    if not self.apoID or self.apoID == "" then
        cecho("<CadetBlue>(sgw gcs)<tomato>: Nie ustalono ID apokalipsy. Nie można pobrać danych.\n")
        return false
    end

    local url = self.host .. "/api/discoveries?apo_id=" .. self.apoID
    local headers = {
        ["X-Session-Token"] = self.sessionToken
    }

    -- Remove old handlers if exist
    if self.handlers.fetch_done then killAnonymousEventHandler(self.handlers.fetch_done) end
    if self.handlers.fetch_error then killAnonymousEventHandler(self.handlers.fetch_error) end

    -- Register event handlers
    self.handlers.fetch_done = registerAnonymousEventHandler("sysGetHttpDone", function(event, rurl, response)
        if rurl ~= url then return true end

        -- Add response debugging
        local success, data = pcall(yajl.to_value, response)

        if not success then
            cecho("<CadetBlue>(sgw gcs)<tomato>: Błąd parsowania odpowiedzi JSON: " .. data .. "\n")
            Msmudlet.gcs.discoveries = {}
            return
        end

        -- Check data validity
        if type(data) == "table" and data then
            local count = 0
            -- If data is an array, count elements
            if data[1] ~= nil then
                for _ in pairs(data) do count = count + 1 end
                Msmudlet.gcs.discoveries = data
                cecho("<CadetBlue>(sgw gcs)<green>: Pobrano <white>" ..
                    count .. "<green> znalezisk dla apoID <white>" .. Msmudlet.gcs.apoID .. "<green>.\n")
            else
                -- Probably received an object instead of array or empty object
                Msmudlet.gcs.discoveries = {}
                local msg = "Brak znalezisk lub niepoprawny format odpowiedzi"
                if data.error then
                    msg = data.error
                elseif data.message then
                    msg = data.message
                end
                cecho("<CadetBlue>(sgw gcs)<tomato>: " .. msg .. " dla apoID <white>" ..
                    Msmudlet.gcs.apoID .. "<tomato>.\n")
            end
        else
            -- Invalid response
            Msmudlet.gcs.discoveries = {}
            cecho("<CadetBlue>(sgw gcs)<tomato>: Nieprawidłowa odpowiedź serwera. Typ: <white>" ..
                type(data) .. "<tomato>.\n")
            display(data)
        end
    end, true)

    self.handlers.fetch_error = registerAnonymousEventHandler("sysGetHttpError", function(event, error, rurl)
        if rurl ~= url then return true end

        cecho("<CadetBlue>(sgw gcs)<tomato>: Błąd podczas pobierania znalezisk: <white>" .. error .. "<tomato>.\n")
    end, true)

    -- Send request
    getHTTP(url, headers)
end

-- Display found items
function Msmudlet.gcs:listDiscoveries()
    if #self.discoveries == 0 then
        cecho(
            "<CadetBlue>(sgw gcs)<tomato>: Brak znalezisk. Użyj <white>/gcs refresh<tomato> aby pobrać aktualne dane.\n")
        return
    end

    cecho("<CadetBlue>(sgw gcs)<green>: ======= Znaleziska dla apoID <white>" .. self.apoID .. "<green> =======\n")
    for i, discovery in ipairs(self.discoveries) do
        local date = os.date("%Y-%m-%d %H:%M", discovery.created_at)
        cecho("<CadetBlue>(sgw gcs)<light_green>: " .. i .. ". [" .. date .. "] " .. discovery.character_name ..
            " znalazł(a) " .. discovery.description ..
            " na lokacji " .. discovery.location_id .. "\n")
    end
    cecho("<CadetBlue>(sgw gcs)<green>: =============================================\n")
end

-- Test connection function
function Msmudlet.gcs:testConnection()
    local url = self.host .. "/api/discoveries?apo_id=test"

    cecho("<CadetBlue>(sgw gcs)<grey>: Testowanie połączenia z serwerem: <white>" .. url .. "<grey>\n")

    if self.handlers.test_done then killAnonymousEventHandler(self.handlers.test_done) end
    if self.handlers.test_error then killAnonymousEventHandler(self.handlers.test_error) end

    self.handlers.test_done = registerAnonymousEventHandler("sysGetHttpDone", function(event, rurl, response)
        if rurl ~= url then return true end

        cecho("<CadetBlue>(sgw gcs)<green>: Połączenie działa! Odpowiedź otrzymana.\n")
    end, true)

    self.handlers.test_error = registerAnonymousEventHandler("sysGetHttpError", function(event, error, rurl)
        if rurl ~= url then return true end

        cecho("<CadetBlue>(sgw gcs)<tomato>: Test połączenia nieudany: <white>" .. error .. "<tomato>\n")
    end, true)

    getHTTP(url)
end

-- Script initialization
function Msmudlet.gcs:init()
    -- First clean up resources if they exist
    self:cleanupResources()

    -- Register login handler to reset apoID
    if self.handlers.login_handler then
        killAnonymousEventHandler(self.handlers.login_handler)
    end

    self.handlers.login_handler = registerAnonymousEventHandler("loginSuccessful", function()
        if self.connected then
            cecho("<CadetBlue>(sgw gcs)<grey>: Przelogowanie. Sprawdzam czy nie bylo apokalipsy.\n")
            Msmudlet.gcs.apoID = nil
            Msmudlet.toolbar:updateGcsStatusFromConnectionState()
            Msmudlet.gcs:getApoID()
        end
    end)

    -- Create aliases for convenient usage
    -- Help alias (no parameters)
    self.aliasy.help = tempAlias("^/gcs$", function()
        Msmudlet.gcs:help()
    end)

    -- Standard aliases with parameters
    self.aliasy.connect = tempAlias("^/gcs connect$", function()
        Msmudlet.gcs:connect()
    end)

    self.aliasy.listDiscoveries = tempAlias("^/gcs list$", function()
        Msmudlet.gcs:listDiscoveries()
    end)

    self.aliasy.fetchDiscoveries = tempAlias("^/gcs refresh$", function()
        Msmudlet.gcs:fetchDiscoveries()
    end)

    self.aliasy.reportDiscovery = tempAlias("^/gcs report (.+) w (.+)$", function()
        local description = matches[2]
        local locationID = matches[3]
        Msmudlet.gcs:reportDiscovery(locationID, description)
    end)

    self.aliasy.sendMessage = tempAlias("^@([^ ]+) (.+)$", function()
        local recipient = matches[2]
        local message = matches[3]
        Msmudlet.gcs:sendMessage(recipient, message)
    end)

    self.aliasy.disconnect = tempAlias("^/gcs disconnect$", function()
        Msmudlet.gcs:disconnect()
    end)

    -- Inicjalizacja zakończona
    cecho("<CadetBlue>(sgw gcs)<green>: Moduł komunikacji Gnome Communication Services zainicjalizowany.\n")
    cecho("<CadetBlue>(sgw gcs)<green>: Użyj <white>/gcs<green> aby zobaczyć dostępne komendy.\n")
end

-- Wywołaj inicjalizację
Msmudlet.gcs:init()
