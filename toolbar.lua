Msmudlet.toolbar = {
  buttons = {},
  containers = {},
  styles = {},
  gcs_status_color = "red" -- Default GCS status color (zmienione z lamp_color)
}

function Msmudlet.toolbar:init()
  self.styles.autkoButtonOff = [[
    QLabel{
    border: 1px solid green;          /* UserWindow border style */
      background-color: #202020;
    font: bold 15pt "Arial";          /* Font of the titleBar */
    color: rgb(0,0,150);              /* Font color */
      text-align: center;
  }]]

  self.styles.autkoButtonOn = [[
    QLabel{
      border: 1px solid black;
      background-color: green;
      font: bold 15pt "Arial";
      color: rgb(0,0,150);
      text-align: center;
    }]]

  self.styles.indicator_green = [[
    QLabel{
      border: 1px solid green;          /* UserWindow border style */
        background-color: green;
        border-radius: 15px;
    font: bold "Arial";
      fgColor = "white";
      text-align: center;
  }]]

  self.styles.indicator_yellow = [[
  QLabel{
    border: 1px solid yellow;          /* UserWindow border style */
    background-color: yellow;
    border-radius: 15px;
    font: bold "Arial";
      fgColor = "black";
      text-align: center;
  }]]

  self.styles.indicator_red = [[
  QLabel{
    border: 1px solid red;          /* UserWindow border style */
    background-color: red;
    border-radius: 15px;
    font: bold "Arial";
      fgColor = "white";
      text-align: center;
  }]]

  self.styles.indicator_time_off = [[
    QLabel{
      border: 2px solid red;          /* UserWindow border style */
      background-color: black;
      border-radius: 15px;
    }]]

  self.styles.indicator_black = [[
    QLabel{
      border: 2px solid black;          /* UserWindow border style */
      background-color: black;
      border-radius: 15px;
    }]]

  self.styles.gcs_status_green = [[
    QLabel{
      border: 1px solid green;
      background-color: green;
      text-align: center;
      font: bold 12pt "Arial";
    }]]

  self.styles.gcs_status_yellow = [[
    QLabel{
      border: 1px solid yellow;
      background-color: yellow;
      text-align: center;
      font: bold 12pt "Arial";
    }]]

  self.styles.gcs_status_red = [[
    QLabel{
      border: 1px solid red;
      background-color: red;
      text-align: center;
      font: bold 12pt "Arial";
    }]]

  setBorderTop(50)
  self.containers.mainContainer = Geyser.Container:new({
    name = "ms_toolbar",
    x = 0,
    y = 0,
    width = "100%",
    height = "50",
  })

  self.idle_indicator = Geyser.Label:new({
    name = "indicator",
    x = 10,
    y = 10,
    width = "30px",
    height = "30px",
    message = ""
  }, self.containers.mainContainer)
  self.idle_indicator:setStyleSheet(self.styles.indicator_black)
  self.idle_indicator:setFontSize(12)
  self.idle_indicator:setAlignment("c")

  self.buttons.autko = Geyser.Label:new({
    name = "autko",
    x = 52,
    y = "0",
    width = "50px",
    height = "50px",
    message = [[AUTKO]]
  }, self.containers.mainContainer)

  self.containers.autkoBox = Geyser.VBox:new({
    name = "autkoBox",
    x = 104,
    y = 0,
    width = "20%",
    height = "100%",
  }, self.containers.mainContainer)

  self.containers.autkoBoxRow1 = Geyser.HBox:new({
    name = "autkoBoxRow1",
  }, self.containers.autkoBox)

  self.containers.autkoBoxRow2 = Geyser.HBox:new({
    name = "autkoBoxRow2",
  }, self.containers.autkoBox)

  self.buttons.wspieraj = Geyser.Label:new({
    name = "wspieraj",
    message = [[<center>WSPIERAJ</center>]]
  }, self.containers.autkoBoxRow1)

  self.buttons.zaslaniaj = Geyser.Label:new({
    name = "zaslaniaj",
    message = [[<center>ZASŁANIAJ</center>]]
  }, self.containers.autkoBoxRow1)

  self.buttons.walcz = Geyser.Label:new({
    name = "walcz",
    message = [[<center>WALCZ</center>]]
  }, self.containers.autkoBoxRow1)

  self.buttons.sledz = Geyser.Label:new({
    name = "sledz",
    message = [[<center>ŚLEDŹ</center>]]
  }, self.containers.autkoBoxRow2)

  self.buttons.grab = Geyser.Label:new({
    name = "grab",
    message = [[<center>GRAB</center>]]
  }, self.containers.autkoBoxRow2)

  for _, b in pairs(self.buttons) do
    b:setStyleSheet(self.styles.autkoButtonOff)
    b:setClickCallback("Msmudlet.autko:" .. b.name .. "_switch")
  end

  -- Add the GCS status in the top right corner
  local window_width = getMainWindowSize()
  self.gcs_status = Geyser.Label:new({
    name = "gcs_status",
    x = window_width - 50,
    y = 10,
    width = "30px",
    height = "30px",
    message = [[<center>GCS</center>]],
    fgColor = "white"
  }, self.containers.mainContainer)
  self.gcs_status:setStyleSheet(self.styles.gcs_status_red)
  self.gcs_status:setAlignment("center")
  self.gcs_status:setClickCallback(function() 
    Msmudlet.toolbar:handleGcsStatusClick() 
  end)

  -- Function to toggle GCS status color
  function Msmudlet.toolbar:toggleGcsStatus(color)
    if color == "red" or color == "yellow" or color == "green" then
      self.gcs_status:setStyleSheet(self.styles["gcs_status_" .. color])
      
      -- Dostosuj kolor tekstu zależnie od tła
      if color == "yellow" then
        self.gcs_status:echo("<center><font color='black'>GCS</font></center>")
      else
        self.gcs_status:echo("<center><font color='white'>GCS</font></center>")
      end
      
      self.gcs_status_color = color
    else
      -- Jeśli nie podano koloru, używamy stanu połączenia
      self:updateGcsStatusFromConnectionState()
    end
  end

  -- Function to update GCS status position on window resize
  function Msmudlet.toolbar:updateGcsStatusPosition()
    local window_width = getMainWindowSize()
    self.gcs_status:move(window_width - 50, 10)
  end

  -- Register event handler for window resize
  registerAnonymousEventHandler("sysWindowResizeEvent", function() Msmudlet.toolbar:updateGcsStatusPosition() end)

  -- Initialize GCS status state
  Msmudlet.toolbar:updateGcsStatusFromConnectionState()
end

-- New function to update GCS status based on actual connection state
function Msmudlet.toolbar:updateGcsStatusFromConnectionState()
  -- Defensive check - make sure gcs module exists
  if not Msmudlet.gcs then
    -- GCS module not available yet, just set the red state
    self.gcs_status:setStyleSheet(self.styles.gcs_status_red)
    self.gcs_status:echo("<center><font color='white'>GCS</font></center>")
    self.gcs_status_color = "red"
    return
  end

  -- Check the current connection state
  if not Msmudlet.gcs.connected then
    -- Not connected - red
    self.gcs_status:setStyleSheet(self.styles.gcs_status_red)
    self.gcs_status:echo("<center><font color='white'>GCS</font></center>")
    self.gcs_status_color = "red"
  elseif Msmudlet.gcs.connected and not Msmudlet.gcs.apoID then
    -- Connected, but no apoID - yellow
    self.gcs_status:setStyleSheet(self.styles.gcs_status_yellow)
    self.gcs_status:echo("<center><font color='black'>GCS</font></center>")
    self.gcs_status_color = "yellow"
  else
    -- Fully connected with apoID - green
    self.gcs_status:setStyleSheet(self.styles.gcs_status_green)
    self.gcs_status:echo("<center><font color='white'>GCS</font></center>")
    self.gcs_status_color = "green"
  end
end

-- New function to handle GCS status click - uses state, not color!
function Msmudlet.toolbar:handleGcsStatusClick()
  -- Defensive check - make sure gcs module exists
  if not Msmudlet.gcs then
    cecho("<CadetBlue>(sgw toolbar)<tomato>: Moduł GCS nie jest dostępny. Poczekaj aż wszystkie skrypty się załadują.\n")
    return
  end

  -- Check if connect function exists
  if not Msmudlet.gcs.connect then
    cecho("<CadetBlue>(sgw toolbar)<tomato>: Funkcje modułu GCS nie są jeszcze dostępne. Poczekaj chwilę.\n")
    return
  end

  if not Msmudlet.gcs.connected then
    -- Not connected - try to connect
    cecho("<CadetBlue>(sgw toolbar)<grey>: Próba połączenia z serwerem GCS...\n")
    Msmudlet.gcs:connect()
  elseif Msmudlet.gcs.connected and not Msmudlet.gcs.apoID then
    -- Connected, but no apoID - try to get it
    cecho("<CadetBlue>(sgw toolbar)<grey>: Próba ustalenia ID apokalipsy...\n")
    Msmudlet.gcs:getApoID()
  else
    -- Fully connected - disconnect
    cecho("<CadetBlue>(sgw toolbar)<grey>: Rozłączanie z serwerem GCS...\n")
    Msmudlet.gcs:disconnect()
  end
end

function Msmudlet.toolbar:autkoOn()
  self.buttons.autko:setStyleSheet(self.styles.autkoButtonOn)
end

function Msmudlet.toolbar:autkoOff()
  self.buttons.autko:setStyleSheet(self.styles.autkoButtonOff)
end

function Msmudlet.toolbar:wspierajOn()
  self.buttons.wspieraj:setStyleSheet(self.styles.autkoButtonOn)
end

function Msmudlet.toolbar:wspierajOff()
  self.buttons.wspieraj:setStyleSheet(self.styles.autkoButtonOff)
end

function Msmudlet.toolbar:zaslaniajOn()
  self.buttons.zaslaniaj:setStyleSheet(self.styles.autkoButtonOn)
end

function Msmudlet.toolbar:zaslaniajOff()
  self.buttons.zaslaniaj:setStyleSheet(self.styles.autkoButtonOff)
end

function Msmudlet.toolbar:walczOn()
  self.buttons.walcz:setStyleSheet(self.styles.autkoButtonOn)
end

function Msmudlet.toolbar:walczOff()
  self.buttons.walcz:setStyleSheet(self.styles.autkoButtonOff)
end

function Msmudlet.toolbar:sledzOn()
  self.buttons.sledz:setStyleSheet(self.styles.autkoButtonOn)
end

function Msmudlet.toolbar:sledzOff()
  self.buttons.sledz:setStyleSheet(self.styles.autkoButtonOff)
end

function Msmudlet.toolbar:grabOn()
  self.buttons.grab:setStyleSheet(self.styles.autkoButtonOn)
end

function Msmudlet.toolbar:grabOff()
  self.buttons.grab:setStyleSheet(self.styles.autkoButtonOff)
end

function Msmudlet.toolbar:indicateIdleState(current, max)
  if current / max > 0.75 then
    self.idle_indicator:setStyleSheet(self.styles.indicator_green)
    self.idle_indicator:echo(current, "white", "cb")
  elseif current / max < 0.75 and current / max > 0 then
    self.idle_indicator:setStyleSheet(self.styles.indicator_yellow)
    self.idle_indicator:echo(current, "black", "cb")
  else
    self.idle_indicator:setStyleSheet(self.styles.indicator_red)
    self.idle_indicator:echo("")
  end
end

function Msmudlet.toolbar:indicateIdleStateTimeOff()
  self.idle_indicator:setStyleSheet(self.styles.indicator_time_off)
  self.idle_indicator:echo("")
end

function Msmudlet.toolbar:indicateIdleStateOff()
  self.idle_indicator:setStyleSheet(self.styles.indicator_black)
  self.idle_indicator:echo("")
end

Msmudlet.toolbar:init()

-- Na końcu pliku, po init, dodaj nasłuchiwacz na załadowanie GCS
registerAnonymousEventHandler("sysLoadEvent", function(_, name)
  if name == "gcs.lua" then
    -- GCS został załadowany, zaktualizuj stan
    tempTimer(0.5, function() Msmudlet.toolbar:updateGcsStatusFromConnectionState() end)
  end
end)
