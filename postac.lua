Msmudlet.postac = Msmudlet.postac or {
	zawod = ""
}


-- lua display(gmcp.char.info.guild_occ)

function Msmudlet.postac:init()
	if self.zawod ~= "" then
		return
	end
	if not gmcp or not gmcp.char or not gmcp.char.info then
		Msmudlet.autko:kolejkuj("postac:init", 1, function() Msmudlet.postac:init() end, 1, false)
		return
	end
	if gmcp.char.info.guild_occ then
		self.zawod = gmcp.char.info.guild_occ
	else
		Msmudlet.autko:kolejkuj("postac:init", 1, function() Msmudlet.postac:init() end, 1, false)
	end

	if self.nowy_zawod then killTrigger(self.nowy_zawod) end
	self.nowy_zawod = tempTrigger("Rozpoczynasz trening w zawodzie ", function()
		Msmudlet.postac.zawod = ""
		Msmudlet.postac:init()
	end)
end

Msmudlet.postac:init()
