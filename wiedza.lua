-- Dowiadujesz sie czegos wiecej o smokach i smokowatych.
-- Wydaje ci sie, ze twoja wiedza o nieumarlych wzrosla nieznacznie. 1%
-- `echo Wydaje ci sie, ze twoja wiedza o nieumarlych wzrosla nieznacznie.
-- lua feedTriggers("\nWydaje ci sie, ze twoja wiedza o nieumarlych wzrosla nieznacznie.\n")
-- function scripts.state_store:set(key, value)  (od razu robi save)
-- function scripts.state_store:delete(key)
-- function scripts.state_store:get(key)

Msmudlet.wiedza = Msmudlet.wiedza or {
	triggers = {},
	aliases = {},
	poziomy = {{nazwa="znikoma",min=1,max=11}, {nazwa="niewielka",min=12,max=23}, {nazwa="czesciowa",min=24,max=33}, {nazwa="niezla",min=34,max=44}, {nazwa="dosc dobra",min=45,max=55},
	 {nazwa="dobra",min=56,max=66}, {nazwa="bardzo dobra",min=67,max=77}, {nazwa="doskonala",min=78,max=88}, {nazwa="prawie pelna",min=89,max=99},{nazwa="pelna",min=100,max=100}},
	rodzaje = {"Chaos i jego twory","goblinoidy","golemy","istoty demoniczne","jaszczuroludzie","magia i jej twory","nieumarli","pajaki i pajakowate",
	 "ryboludzie","smoki i smokowate","starsze rasy","stwory pokoniunkcyjne","szczuroludzie","wampiry"},
	slownik = {["Chaosie i jego tworach"]="Chaos i jego twory",goblinoidach="goblinoidy",golemach="golemy",["istotach demonicznych"]="istoty demoniczne",jaszczuroludziach="jaszczuroludzie",["magii i jej tworach"]="magia i jej twory",
	nieumarlych="nieumarli",["pajakach i pajakowatych"]="pajaki i pajakowate",ryboludziach="ryboludzie",["smokach i smokowatych"]="smoki i smokowate",["starszych rasach"]="starsze rasy",
	["stworach pokoniunkcyjnych"]="stwory pokoniunkcyjne",szczuroludziach="szczuroludzie",wampirach="wampiry"},
	stan = {}
}



function Msmudlet.wiedza:load()
	local wiedza=scripts.state_store:get("msmudlet_wiedza")
	if wiedza then
		self.stan=wiedza
	else
		for _,rodzaj in pairs(self.rodzaje) do
			self.stan[rodzaj]=0
		end
		Msmudlet.wiedza:save()
	end
end

function Msmudlet.wiedza:save()
	scripts.state_store:set("msmudlet_wiedza", self.stan)
end

function Msmudlet.wiedza:show(rodzaj,poziom)
	local min,max
	local change=0
	local change_napis=""
	local ccolor="green"
	local wartosc_db=0
	for _,lvl in pairs(self.poziomy) do
		if lvl.nazwa == poziom then
			min=lvl.min
			max=lvl.max
		end
	end
	if not min or not max then
		return 0
	end
	if self.stan[rodzaj] then
		wartosc_db = self.stan[rodzaj]
		if wartosc_db < min or wartosc_db > max then
			Msmudlet.wiedza:set(rodzaj,min)
			change=min-wartosc_db
			wartosc_db = min
		end
	else
		return 0
	end
	if change < 0 then
		ccolor = "red"
		change_napis="-"..change
	elseif change==0 then
		change_napis=""
	else
		change_napis="+"..change
	end
	cecho(string.format(" - %s%% <%s>%s",wartosc_db,ccolor,change_napis))
end

function Msmudlet.wiedza:set(rodzaj,wartosc)
	self.stan[rodzaj]=wartosc
	Msmudlet.wiedza:save()
end

function Msmudlet.wiedza:init()
	Msmudlet.wiedza:load()
	if Msmudlet.wiedza.triggers.up then killTrigger(Msmudlet.wiedza.triggers.up) end
	Msmudlet.wiedza.triggers.up = tempRegexTrigger("Wydaje ci sie, ze twoja wiedza o ([Ca-z ]+) wzrosla ([a-z]+)\\.$", function()
		local add=0
		if matches[3]=="nieznacznie" then
			add=1
		elseif matches[3]=="znaczaco" then
			add=9
		end
		local rodzaj=Msmudlet.wiedza.slownik[matches[2]]
		if Msmudlet.wiedza.stan[rodzaj] then
				Msmudlet.wiedza:set(rodzaj,Msmudlet.wiedza.stan[rodzaj]+add)
		else
			Msmudlet.wiedza:set(rodzaj,1)
		end
		Msmudlet.wiedza:save()
		cecho(string.format(" - %s%% <%s>%s",Msmudlet.wiedza.stan[rodzaj],"green","+"..add))

	end)

	if self.aliases.ustaw then killAlias(self.aliases.ustaw) end
	self.aliases.ustaw = tempAlias("^/wiedza_ustaw ([Ca-z ]+)=([0-9]+)$", function ()
		local rodzaj=matches[2]
---@diagnostic disable-next-line: undefined-field
		if table.contains(self.rodzaje,rodzaj) then
			local wartosc=tonumber(matches[3])
			if wartosc < 0 or wartosc >100 then
				cecho("<firebrick>Możesz mieć wiedzy między 1, a 100%")
			else
				Msmudlet.wiedza:set(rodzaj,wartosc)
				cecho("<white> Ustawiam wartość wiedzy typu '"..rodzaj.."' na "..wartosc)
			end
		else
			cecho("<firebrick>Nie znam takiego typu wiedzy - "..rodzaj..". Masz do wyboru:\n")
			display(self.rodzaje)
		end
	end)
end

Msmudlet.wiedza:init()


function alias_func_skrypty_misc_wiedza()
	if Msmudlet.wiedza.triggers.knowledge then killTrigger(Msmudlet.wiedza.triggers.knowledge) end
	Msmudlet.wiedza.triggers.knowledge = tempRegexTrigger("([Ca-z ]+): +([a-z ]+)", function() 
		Msmudlet.wiedza:show(matches[2],matches[3])
	end,14)
	-- enableTrigger("scripts-knowledge")
	tempTimer(0.1, function() send("wiedza", false) end)
	tempTimer(1, function() killTrigger("Msmudlet.wiedza.triggers.knowledge") end)
end
