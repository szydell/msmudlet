Msmudlet.herbs = Msmudlet.herbs or {
  data_dir = getMudletHomeDir() .. "/sgw-dane",
  data_url = "https://raw.githubusercontent.com/tjurczyk/arkadia-data/master/herbs_data.json",
  handlers = {},
  aliasy = {}
}


function Msmudlet.herbs:print(window)
  if not herbs.db or table.size(herbs.db) == 0 then
    cecho(window, "\n <orange>Brak zbudowanej bazy ziol, ")
    cechoPopup(window, "<deep_sky_blue>kliknij tutaj aby zrobic '/ziola_buduj'", { [[expandAlias("/ziola_buduj")]] },
      { [[/ziola_buduj]] }, true)
    cecho(window, " \n\n")
    return
  end

  cecho(window, "\n")
  local line = ""
  for i = 1, getWindowWrap("main") do
    line = line .. "-"
  end
  cecho(window, line .. "\n")
  cecho(window,
    "  <light_slate_blue>ile <grey>|        <light_slate_blue>nazwa <grey>           | <light_slate_blue>                            działanie <grey>\n")
  cecho(window, line .. "\n")

  for _, herb_id in pairs(herbs.sorted_herb_ids) do
    local v = herbs.index[herb_id]
    if table.size(v) > 0 then
      local amount = 0
      for i, j in pairs(v) do
        amount = amount + herbs.db[i][herb_id]["amount"]
      end
      local amount_tmp = "    " .. tostring(amount)
      local name_str = string.sub("<yellow>" .. herb_id .. "                     ", 0, 31)
      local usage_str = string.sub(
        herbs.herbs_details[herb_id]["details"] .. "                                                                 ", 0,
        64)
      cecho(window, "<grey>  " .. string.sub(amount_tmp, #amount_tmp - 2, #amount_tmp) .. " | ")
      local clickable_herb_data = herbs:get_clickable_herb_data(herb_id)
      cechoPopup(window, name_str, clickable_herb_data["herb_actions"], clickable_herb_data["herb_hints"], true)
      cecho(window, " | ")
      for iii, use_element in pairs(clickable_herb_data["use_elements"]) do
        if use_element["actions"] then
          cechoPopup(window, use_element["text"], use_element["actions"], use_element["hints"], true)
        else
          cecho(window, use_element["text"])
        end
        if iii < #clickable_herb_data["use_elements"] then
          cecho(window, " | ")
        end
        -- add a single space so we don't make the rest of the line clickable
        -- I don't know how to solve it any other way.
      end
      cecho(window, "\n")
      local wystepowanie = ""
      if herbs.data.herb_id_to_use[herb_id].where then
        for _, miejsce in pairs(herbs.data.herb_id_to_use[herb_id].where) do
          wystepowanie = wystepowanie .. miejsce .. " "
        end
      end
      cecho(window, string.format("        <light_slate_blue>wystepowanie: <grey> %s \n", wystepowanie))
      local herb_info = ""
      if herbs.data.herb_id_to_use[herb_id].info and herbs.data.herb_id_to_use[herb_id].info ~= "" then
        herb_info = herbs.data.herb_id_to_use[herb_id].info
        cecho(window, string.format("        <light_slate_blue>info: <grey> %s \n", herb_info))
      end
      local sprzedaz = "nie"
      if herbs.data.herb_id_to_use[herb_id].forsell == nil then
        sprzedaz = "n/a"
      elseif herbs.data.herb_id_to_use[herb_id].forsell then
        sprzedaz = "tak"
      end
      cecho(window, string.format("        <light_slate_blue>na sprzedaż: <grey> "))
      cechoLink(sprzedaz, function() Msmudlet.herbs:change_isp(herb_id) end, "kliknij, żeby zmienić", true)
      local desired_quantity = 0
      if herbs.data.herb_id_to_use[herb_id].desired_quantity then
        desired_quantity = herbs.data.herb_id_to_use[herb_id].desired_quantity
      end
      -- Wyświetlenie desired_quantity z menu kontekstowym
      cecho(window, ", <light_slate_blue>zostawiaj: <grey>")

      -- Tworzymy menu kontekstowe z opcjami
      local options = { "0", "10", "15", "30", "50", "100", "200" }
      local actions = {}
      local hints = {}

      -- Generujemy akcje i podpowiedzi dla każdej opcji
      for _, option in ipairs(options) do
        table.insert(actions, function() Msmudlet.herbs:change_isp_desired_quantity(herb_id, tonumber(option)) end)
        table.insert(hints, "Zmień na " .. option)
      end

      -- Wyświetlamy klikalny element z menu kontekstowym
      cechoPopup(window, tostring(desired_quantity), actions, hints, true)

      cecho(window, "\n")
      cecho(window, line .. "\n")
    end
  end

  if table.size(ateam.team_names) > 0 then
    cecho(window, "\n  <yellow>Daj ziola<grey>:<pale_green>")
  end
  local idx = 1
  for teammate_name, v in pairs(ateam.team_names) do
    local teammate_clickable_data = herbs:get_clickable_teammate_data(teammate_name)
    cecho(window, " ")
    cechoPopup(window, "<pale_green>" .. teammate_name, teammate_clickable_data["teammate_actions"],
      teammate_clickable_data["teammate_hints"], true)
    if idx ~= table.size(ateam.team_names) then
      cecho(window, " <grey>|")
    end
    idx = idx + 1
  end
  cecho(window, "\n\n")
end

function Msmudlet.herbs:raport()
  if not herbs["data"] then
    return
  end

  self:print("main")
end

function Msmudlet.herbs:init()
  if self.aliasy.raport then killAlias(self.aliasy.raport) end
  self.aliasy.raport = tempAlias("^/ziola_raport$", function()
    Msmudlet.herbs:raport()
  end)
end

Msmudlet.herbs:init()


function Msmudlet.herbs:init_data()
  herbs["herbs_long_to_short"] = {}
  herbs["herbs_details"] = {}
  herbs["isp"] = {} -- individial sell preferences
  herbs.db = nil
  herbs.data_file_path = self.data_dir .. "/herbs_data.json"
  herbs.data_isp_file_path = self.data_dir .. "/herbs_data_isp.json"

  if not io.exists(herbs.data_file_path) then
    scripts:print_log("baza ziol niedostepna, skrypt do ziol nie bedzie dzialac")
    return
  end

  local file_handle = assert(io.open(herbs.data_file_path, "r"))
  local file_content = file_handle:read("*all")
  herbs["data"] = yajl.to_value(file_content)
  herbs:v2_init_herbs()
end

-- individual sell preferences
function Msmudlet.herbs:load_isp()
  if not io.exists(herbs.data_isp_file_path) then
    Msmudlet.herbs:save_isp()
  end
  local file_handle = assert(io.open(herbs.data_isp_file_path, "r"))
  local file_content = file_handle:read("*all")
  if file_content == "" then
    herbs["isp"] = {}
  else
    herbs["isp"] = yajl.to_value(file_content)
  end
  file_handle:close()
  for herb_name, herb_params in pairs(herbs["isp"]) do
    if type(herb_params) == "boolean" then
      herbs.herbs_details[herb_name]["forsell"] = herb_params
      herbs.data.herb_id_to_use[herb_name]["forsell"] = herb_params
    else
      if herb_params.forsell then
        herbs.herbs_details[herb_name]["forsell"] = herb_params.forsell
        herbs.data.herb_id_to_use[herb_name]["forsell"] = herb_params.forsell
      end
      if herb_params.desired_quantity then
        herbs.herbs_details[herb_name]["desired_quantity"] = herb_params.desired_quantity
        herbs.data.herb_id_to_use[herb_name]["desired_quantity"] = herb_params.desired_quantity
      end
    end
  end
end

-- individual sell preferences
function Msmudlet.herbs:change_isp(h)
  if herbs.herbs_details[h]["forsell"] then
    herbs.herbs_details[h]["forsell"] = false
    herbs.data.herb_id_to_use[h]["forsell"] = false
    if type(herbs["isp"][h]) == "boolean" then
      herbs["isp"][h] = nil
    end
    herbs["isp"][h] = herbs["isp"][h] or {}
    herbs["isp"][h]["forsell"] = false
    scripts:print_log("ziolo " .. h .. " nie bedzie sprzedawane gdy użyjesz komendy /ziola_sprzedaj.")
  else
    herbs.herbs_details[h]["forsell"] = true
    herbs.data.herb_id_to_use[h]["forsell"] = true
    if type(herbs["isp"][h]) == "boolean" then
      herbs["isp"][h] = nil
    end
    herbs["isp"][h] = herbs["isp"][h] or {}
    herbs["isp"][h]["forsell"] = true
    scripts:print_log("ziolo " .. h .. " zostanie sprzedane gdy użyjesz komendy /ziola_sprzedaj.")
  end
  Msmudlet.herbs:save_isp()
end

-- individual sell preferences
function Msmudlet.herbs:change_isp_desired_quantity(h, dq)
  local _tmp_forsell = nil
  if type(herbs["isp"][h]) == "boolean" then
    _tmp_forsell = herbs["isp"][h]
    herbs["isp"][h] = nil
  end
  herbs["isp"][h] = herbs["isp"][h] or {}
  herbs["isp"][h]["forsell"] = herbs["isp"][h]["forsell"] or _tmp_forsell
  herbs["isp"][h]["desired_quantity"] = dq
  herbs.herbs_details[h]["desired_quantity"] = dq
  herbs.data.herb_id_to_use[h]["desired_quantity"] = dq

  if dq > 0 then
    scripts:print_log(h .. " /ziola_sprzedaj tylko ponad " .. dq .. " sztuk.")
  else
    scripts:print_log("/ziola_sprzedaj sprzeda wszystkie sztuki ziola " .. h)
  end

  Msmudlet.herbs:save_isp()
end

-- individual sell preferences
function Msmudlet.herbs:save_isp()
  local file_handle = assert(io.open(herbs.data_isp_file_path, "w+"))
  file_handle:write(yajl.to_string(herbs["isp"]))
  file_handle:close()
end

function herbs:v2_init_herbs()
  for herb_name, herb_odmiana in pairs(herbs.data.herb_id_to_odmiana) do
    for _, herb_odmiana_val in pairs(herb_odmiana) do
      herbs.herbs_long_to_short[herb_odmiana_val] = herb_name
    end
  end
  local actions = {}
  for herb_name, herb_data in pairs(herbs.data.herb_id_to_use) do
    local herb_action_str = ""
    if herb_data.actions then
      for idx, herb_action in pairs(herb_data.actions) do
        if not table.contains({ ".", "brak" }, herb_action["action"]) then
          actions[herb_action["action"]] = true
        end
        if herb_action["effect"] ~= nil and herb_action["action"] then
          herb_action_str = herb_action_str .. herb_action["action"] .. ": " .. herb_action["effect"]
        end
        if idx ~= #herb_data.actions then
          herb_action_str = herb_action_str .. " | "
        end
      end
    end
    herbs.herbs_details[herb_name] = {}
    herbs.herbs_details[herb_name]["desc"] = herbs.data.herb_id_to_odmiana[herb_name]["mianownik"]
    herbs.herbs_details[herb_name]["acc"] = herbs.data.herb_id_to_odmiana[herb_name]["biernik"]
    herbs.herbs_details[herb_name]["details"] = herb_action_str
    herbs.herbs_details[herb_name]["forsell"] = herb_data.forsell
  end
  Msmudlet.herbs:load_isp()
  if herbs.use_alias then
    killAlias(herbs.use_alias)
  end
  herbs.use_alias = tempAlias(string.format("^/z_(%s) ([a-z_]+)(?: ([0-9]+))?$", table.concat(table.keys(actions), "|")),
    "alias_func_skrypty_herbs_zazyj_ziolo()")
end

function herbs:get_clickable_herb_data(herb_name)
  local herb_actions = { "" }
  local herb_hints = { herb_name }

  for k, v in pairs(herbs.settings.get_herb_counts) do
    if v > 1 then
      table.insert(herb_hints, "wez " .. tostring(v))
      table.insert(herb_actions, [[expandAlias("/wezz ]] .. herb_name .. [[ ]] .. v .. [[")]])
    else
      table.insert(herb_hints, "wez")
      table.insert(herb_actions, [[expandAlias("/wezz ]] .. herb_name .. [[")]])
    end
  end

  local this_herb_actions = herbs.data.herb_id_to_use[herb_name].actions

  if not this_herb_actions then
    return { herb_actions = herb_actions, herb_hints = herb_hints, use_elements = {} }
  end

  local use_elements = {}

  for k, v in pairs(this_herb_actions) do
    local use_element = {}

    if v["action"] and v["action"] ~= "" then
      use_element["text"] = v["action"] .. ": " .. v["effect"]
    else
      use_element["text"] = v["effect"]
    end

    if not v["dont_bind"] and v["action"] ~= nil and v["action"] ~= "" then
      use_element["hints"] = {}
      use_element["actions"] = {}
      table.insert(use_element["hints"], herb_name)
      table.insert(use_element["actions"], "")
      for kk, current_count in pairs(herbs.settings.use_herb_counts) do
        local use_hint = ""
        local use_action = ""
        if current_count > 1 then
          use_hint = v["action"] .. " " .. tostring(current_count)
          use_action = [[expandAlias("/z_]] .. v["action"] .. [[ ]] .. herb_name .. [[ ]] .. current_count .. [[")]]
        else
          use_hint = v["action"]
          use_action = [[expandAlias("/z_]] .. v["action"] .. [[ ]] .. herb_name .. [[")]]
        end
        table.insert(use_element["hints"], use_hint)
        table.insert(herb_hints, use_hint)
        table.insert(use_element["actions"], use_action)
        table.insert(herb_actions, use_action)
      end
    end
    table.insert(use_elements, use_element)
  end
  return { herb_actions = herb_actions, herb_hints = herb_hints, use_elements = use_elements }
end
