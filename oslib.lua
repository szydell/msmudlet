Msmudlet.os = Msmudlet.os or {
	sysop = ""
}



function Msmudlet.os:capture(cmd, raw)
	local f = assert(io.popen(cmd, 'r'))
	local s = assert(f:read('*a'))
	f:close()
	if raw then return s end
	s = string.gsub(s, '^%s+', '')
	s = string.gsub(s, '%s+$', '')
	s = string.gsub(s, '[\n\r]+', ' ')
	return s
end

function Msmudlet.os:uname()
	local uname = Msmudlet.os:capture("uname",false)
	if uname=="Linux" then
		self.sysop = "Linux"
	else
		self.sysop ="nonLinux"
	end
end

Msmudlet.os:uname()
