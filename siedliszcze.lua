Msmudlet.mapy.siedliszcze = Msmudlet.mapy.siedliszcze or {
	lokacje = {},
	lokalizatory = {}
}


function Msmudlet.mapy.siedliszcze:init()
	-- Lokacje
	self.lokacje.galeon_ladownia = 24509
	self.lokacje.nabrzeze_1 = 24492
	self.lokacje.nabrzeze_2 = 24493
	self.lokacje.nabrzeze_4 = 24495
	self.lokacje.nabrzeze_5 = 24496
	self.lokacje.nabrzeze_6 = 24497
	self.lokacje.nabrzeze_7 = 24499

	-- LOKALIZATORY
	if self.lokalizatory.ladownia then killTrigger(self.lokalizatory.ladownia) end
	self.lokalizatory.ladownia = tempTrigger("Majestatyczny ogrom i przepych panujace na gornych pokladach ustapily ciasnocie i zaduchowi niepodzielnie wladajacym pod pokladem.", function() Msmudlet.lib:centerView(self.lokacje.galeon_ladownia) end)

end

Msmudlet.mapy.siedliszcze:init()

