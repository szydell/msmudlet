#!/usr/bin/env bash

if [ -z "${msm_devdir}" ]; then
	devdir=${1:-"/home/szydell/dev/msmudlet"}
else
	devdir=$msm_devdir
fi

oldPwd=$(pwd)
cd "${devdir}" 2> /dev/null || { echo "Can't move to ${devdir}"; echo "export msm_devdir=<path_to_repo> or run this script as ./devpack.sh <path_to_repo>"; exit 1; }

zip msmudlet.zip ./*
zip msmudlet.zip ./py/pudelko.py

cd "${oldPwd}" || { echo "Can't move to ${oldPwd}'"; }
  
