-- /wybierz <co>

Msmudlet.wybierz = Msmudlet.wybierz or {
	items = {},
	itemcount = {},
	ids = {},
	tids = {}, --trigger ids
	eids = {}, --event handlers ids
	current_item = 1,
	pojemnik = "",
	value_keys = {
		"wywazenie",
		"obrazenia",
		"klute",
		"obuchowe",
		"ciete",
		"parowanie"
	}
}


function Msmudlet.wybierz:alias_func()
	-- * ( 1) falisty posrebrzany topor bojowy . . . . .(w znakomitym stanie)
	-- Przegladasz zawartosc otwartej wielkiej debowej szafy.
	-- Przegladasz zawartosc wysokich drewnianych stojakow.
	-- Msmudlet.wybierz.ids.dodaj_item_trigger = tempRegexTrigger('^([\*\!\$]) \(.(\d+)\) ([a-z ]*)[ \.]+\((.*)\)$', function() echo("catched!") end, 2)
	Msmudlet.wybierz:cleanup()
	disableTrigger("posluzy")
	-- trigger pozwalajacy na poczatkowe uzupelnienie tablicy ze sprzetem
	local dodaj_item_trigger = tempRegexTrigger("^([\\*\\!\\$\\-]) \\( ?(\\d+)\\) ([a-z \\-]*)[ \\.]+\\((.*)\\)$",
		function() Msmudlet.wybierz:dodaj_item(matches[3], matches[4], matches[2], matches[5]) end)
	table.insert(self.tids, dodaj_item_trigger)
	local koniecprzejrzyj_trigger = tempRegexTrigger("(.*Slucham\\?)",
		[[ deleteLine() Msmudlet.wybierz:opozniaj_badanie_itemu() ]], 1)
	table.insert(self.tids, koniecprzejrzyj_trigger)
	local nie_ma_przedmiotow = tempRegexTrigger(".*Nie ma tutaj takich przedmiotow\\.", [[ Msmudlet.wybierz:cleanup() ]],
		1)
	table.insert(self.tids, nie_ma_przedmiotow)
	local wyslij_idkfa = tempRegexTrigger(".*Przegladasz zawartosc.*", [[ send("idkfa",false) ]], 1)
	table.insert(self.tids, wyslij_idkfa)
	local wyslij_otworz = tempRegexTrigger("Zamkniet[ay] (.*) jest zamkniet[ay]\\.",
		function() Msmudlet.wybierz:otworz_magazyn(matches[2]) end)
	table.insert(self.tids, wyslij_otworz)
	-- handler do parsera jakosci sprzetu
	local handler = scripts.event_register:register_singleton_event_handler(self.handler, "equipmentEvaluation",
		function(_, equipment)
			self:uzupelnij_item(equipment)
		end)
	table.insert(self.eids, handler)

	-- start
	send("przejrzyj " .. self.pojemnik)
end

function Msmudlet.wybierz:otworz_magazyn(nazwa)
	nazwa = string.gsub(nazwa, "^(.*)szafa(.*)$", "%1szafe%2")
	nazwa = string.gsub(nazwa, "^(.*)skrzynia(.*)$", "%1skrzynie%2")
	tempTimer(math.random(10, 100) / 100,
		function()
			send("otworz " .. nazwa)
			send("przejrzyj " .. Msmudlet.wybierz.pojemnik)
		end)
end

function Msmudlet.wybierz:init()
	self.current_item = 1
	self.items = {}
	self.itemcount = {}
	Msmudlet.wybierz:cleanup()
	if Msmudlet.wybierz.ids.wybierz_alias then
		killAlias(Msmudlet.wybierz.ids.wybierz_alias)
	end

	Msmudlet.wybierz.ids.wybierz_alias = tempAlias("^/wybierz (.*)$", function()
		Msmudlet.wybierz.pojemnik = matches[2]
		Msmudlet.wybierz:alias_func()
	end)
end

function Msmudlet.wybierz:dodaj_item(lp, short, naprawialnosc, stan)
	short = string.gsub(short, '^%s*(.-)%s*$', '%1')
	short = string.gsub(short, "^(.+)tarcza(.*)$", "%1tarcze%2")
	short = string.gsub(short, "^(.+)kolczuga(.*)$", "%1kolczuge%2")
	short = string.gsub(short, "^(.+)karacena(.*)$", "%1karacene%2")
	short = string.gsub(short, "^(.+)brygantyna(.*)$", "%1brygantyne%2")
	short = string.gsub(short, "^(.+)tunika(.*)$", "%1tunike%2")
	short = string.gsub(short, "^(.+)zbroja(.*)$", "%1zbroje%2")
	short = string.gsub(short, "^(.+)para (.*)$", "%1pare %2")
	short = string.gsub(short, "^(.+) daga$", "%1 dage")
	short = string.gsub(short, "^(.+) maczuga(.*)$", "%1 maczuge%2")
	short = string.gsub(short, "^(.+) wekiera(.*)$", "%1 wekiere%2")
	short = string.gsub(short, "^(.+) bulawa(.*)$", "%1 bulawe%2")
	short = string.gsub(short, "^(.+) burgoneta(.*)$", "%1 burgonete%2")
	short = string.gsub(short, "^(.+) barbuta(.*)$", "%1 barbute%2")
	short = string.gsub(short, "^(.+) falcata(.*)$", "%1 falcate%2")
	short = string.gsub(short, "^(.+) szpada(.*)$", "%1 szpade%2")
	short = string.gsub(short, "^(.+) szabla(.*)$", "%1 szable%2")
	short = string.gsub(short, "^(.+) navaja(.*)$", "%1 navaje%2")
	short = string.gsub(short, "^(.+) palka(.*)$", "%1 palke%2")
	short = string.gsub(short, "^(.+) spodnica(.*)$", "%1 spodnice%2")
	short = string.gsub(short, "^(.+) wlocznia(.*)$", "%1 wlocznie%2")
	short = string.gsub(short, "^(.+) rohatyna(.*)$", "%1 rohatyne%2")
	short = string.gsub(short, "^(.+) kosa(.*)$", "%1 kose%2")
	short = string.gsub(short, "^(.+) dzida(.*)$", "%1 dzide%2")
	short = string.gsub(short, "^(.+) gizarma(.*)$", "%1 gizarme%2")
	short = string.gsub(short, "^(.+) korona(.*)$", "%1 korone%2")
	short = string.gsub(short, "^(.+) pala$", "%1 pale")
	short = string.gsub(short, "^(.+) przylbica(.*)", "%1 przylbice%2")
	short = string.gsub(short, "^(.+) halabarda$", "%1 halabarde")
	short = string.gsub(short, "^(.+) kotwica$", "%1 kotwice")
	short = string.gsub(short, "^(.+) siekierka$", "%1 siekierke")
	short = string.gsub(short, "^(.+) bajdana$", "%1 bajdane")

	if short == "kunsztowna mahakamska barda gornicza" then
		short = "kunsztowna mahakamska barde gornicza"
	elseif short == "para plytowych kolczatych naudziakow" then
		short = "plytowe kolczate naudziaki"
	elseif short == "para srebrzystych ciezkich naramiennikow" then
		short = "srebrzyste ciezkie naramienniki"
	end



	table.insert(Msmudlet.wybierz.items,
		{ ["lp"] = lp, ["short"] = short, ["naprawialnosc"] = naprawialnosc, ["stan"] = stan })
end

function Msmudlet.wybierz:opozniaj_badanie_itemu()
	tempTimer(math.random(10, 100) / 100, function() Msmudlet.wybierz:wez_item() end)
end

function Msmudlet.wybierz:wez_item()
	if Msmudlet.wybierz.items[Msmudlet.wybierz.current_item] then
		local wez_item_trigger = tempRegexTrigger("Wybierasz .* ze? (.*)(z okuciami)?\\.$",
			function() Msmudlet.wybierz:zbadaj_i_odloz_item(matches[2]) end, 1)
		table.insert(self.tids, wez_item_trigger)
		send("wybierz " ..
			Msmudlet.wybierz.items[Msmudlet.wybierz.current_item].lp ..
			". " .. Msmudlet.wybierz.items[Msmudlet.wybierz.current_item].short)
	else
		Msmudlet.wybierz:wygeneruj_raport()
	end
end

function Msmudlet.wybierz:zbadaj_i_odloz_item(pojemnik)
	local srebrna_bron = tempRegexTrigger(
		"Do wykonania tej broni uzyto srebra, bedzie wiec ona skuteczna przeciw wrogom odpornym na zwykle obrazenia\\.",
		function() Msmudlet.wybierz.items[Msmudlet.wybierz.current_item - 1]["srebro"] = "tak" end, 1)
	table.insert(self.tids, srebrna_bron)
	local magiczna_bron = tempRegexTrigger(
		"Sadzac po delikatnym drzeniu w broni tej zostala zakleta jakas magia, bedzie wiec ona skuteczna przeciw wrogom odpornym na zwykle obrazenia\\.",
		function() Msmudlet.wybierz.items[Msmudlet.wybierz.current_item - 1]["magia"] = "tak" end, 1)
	table.insert(self.tids, magiczna_bron)
	send("ocen " .. Msmudlet.wybierz.items[Msmudlet.wybierz.current_item].short)
	send("wloz " .. Msmudlet.wybierz.items[Msmudlet.wybierz.current_item].short .. " do " .. pojemnik)
end

function Msmudlet.wybierz:uzupelnij_item(equipment)
	-- lp,short,naprawialnosc,stan,"is_armor","bron","chwyt","obrazenia","wywazenie","skutecznosc"
	-- lp,short,naprawialnosc,stan,"is_armor","zbroja","typ","klute","ciete","obuchowe","parowanie" (typ=lekka,srednia,ciezka,tarcza,puklerz. tarcza ma wszystko, puklerz tylko parowanie)

	if equipment.typSprzetu == "puklerz" and not self.items[Msmudlet.wybierz.current_item].short:match("puklerz") and not self.items[Msmudlet.wybierz.current_item].short:match("rondele") then
		self.items[Msmudlet.wybierz.current_item]["typ"] = "tarcza"
		equipment.is_armor = true
		equipment.typSprzetu = "tarcza"
	else
		self.items[Msmudlet.wybierz.current_item]["typ"] = equipment.typSprzetu
	end

	self.items[Msmudlet.wybierz.current_item]["is_armor"] = equipment.is_armor



	if equipment.is_armor then
		if equipment.klute and equipment.klute.value then
			self.items[Msmudlet.wybierz.current_item]["klute"] = equipment.klute.value
		else
			self.items[Msmudlet.wybierz.current_item]["klute"] = 0
		end
		if equipment.ciete and equipment.ciete.value then
			self.items[Msmudlet.wybierz.current_item]["ciete"] = equipment.ciete.value
		else
			self.items[Msmudlet.wybierz.current_item]["ciete"] = 0
		end
		if equipment.obuchowe and equipment.obuchowe.value then
			self.items[Msmudlet.wybierz.current_item]["obuchowe"] = equipment.obuchowe.value
		else
			self.items[Msmudlet.wybierz.current_item]["obuchowe"] = 0
		end
		if equipment.typSprzetu == "tarcza" then
			self.items[Msmudlet.wybierz.current_item]["parowanie"] = equipment.parowanie.value
		end
	else
		if equipment.typSprzetu == "puklerz" then
			self.items[Msmudlet.wybierz.current_item]["parowanie"] = equipment.parowanie.value
		else
			self.items[Msmudlet.wybierz.current_item]["chwyt"] = equipment.chwytanie
			self.items[Msmudlet.wybierz.current_item]["obrazenia"] = equipment.obrazenia
			self.items[Msmudlet.wybierz.current_item]["wywazenie"] = equipment.wywazenie.value
			self.items[Msmudlet.wybierz.current_item]["skutecznosc"] = equipment.parowanie.value
		end
	end

	local sum, count = 0, 0
	if equipment.typSprzetu == "puklerz" then
		self.items[Msmudlet.wybierz.current_item]["waga"] = equipment.parowanie.value
	elseif equipment.typSprzetu == "tarcza" then
		for _, value_key in pairs(self.value_keys) do
			if self.items[Msmudlet.wybierz.current_item] and self.items[Msmudlet.wybierz.current_item][value_key] then
				if value_key == "parowanie" then
					sum = sum + self.items[Msmudlet.wybierz.current_item][value_key] * 4
				else
					sum = sum + self.items[Msmudlet.wybierz.current_item][value_key]
				end
				count = count + 1
			end
		end
		self.items[Msmudlet.wybierz.current_item]["waga"] = sum / count
	else
		for _, value_key in pairs(self.value_keys) do
			if equipment[value_key] and equipment[value_key].value then
				if value_key == "wywazenie" then
					sum = sum + equipment[value_key].value * 1.4
				else
					sum = sum + equipment[value_key].value
				end
				count = count + 1
			end
		end
		Msmudlet.wybierz.items[Msmudlet.wybierz.current_item]["waga"] = sum / count
	end

	Msmudlet.wybierz.current_item = Msmudlet.wybierz.current_item + 1
	Msmudlet.wybierz:opozniaj_badanie_itemu()
end

function Msmudlet.wybierz:cleanup()
	-- czysc / inicjuj zmienne
	self.current_item = 1
	self.items = {}
	self.itemcount = {}
	-- czysc  triggery i event handlery
	if self.eids then
		for i = 1, #self.eids do
			scripts.event_register:kill_event_handler(self.eids[i])
		end
	end
	self.eids = {}
	if self.tids then
		for i = 1, #self.tids do
			killTrigger(self.tids[i])
		end
	end
	self.tids = {}
	enableTrigger("posluzy")
end

function Msmudlet.wybierz:wygeneruj_raport()
	---@diagnostic disable-next-line: undefined-field
	local ts = table.size(Msmudlet.wybierz.items)
	if ts > 1 then
		for i = 2, ts do
			if self.items[i].typ ~= self.items[i - 1].typ then
				print("Porownywanie sprzetu roznego typu nie ma sensu.")
				Msmudlet.wybierz:cleanup()
				return
			end
		end
	end

	-- tutaj trzeba zrobic inverta numeru danego sprzetu.
	-- itemcount

	for k, v in pairs(self.items) do
		if self.itemcount[v.short] then
			self.itemcount[v.short].count = self.itemcount[v.short].count + 1
		else
			self.itemcount[v.short] = {}
			self.itemcount[v.short].count = 1
		end
	end

	-- gen invtables
	for k, v in pairs(self.itemcount) do
		local invtable = {}
		for i = 1, v.count do
			invtable[i] = v.count + 1 - i
		end
		self.itemcount[k].invtable = invtable
	end

	-- set proper id
	for k, v in pairs(self.items) do
		self.items[k].lp = self.itemcount[v.short].invtable[tonumber(v.lp)]
	end

	table.sort(self.items, function(a, b)
		if a.waga < b.waga then
			return true
		else
			return false
		end
	end)

	-- display(Msmudlet.wybierz.items)


	if self.items[1].is_armor then
		if self.items[1].typ == "tarcza" then
			echo(
				"+-------+----+------------------------------------------+--------+-----------+-------+-------+-------+\n")
			cecho(string.format(
				"| Kowal | Lp | <yellow>%40.40s<reset> | <white>Jakość<reset> | <white>Parowanie<reset> | Klute | Ciete | Obuch |\n",
				self.pojemnik))
			echo(
				"+-------+----+------------------------------------------+--------+-----------+-------+-------+-------+\n")
		else
			echo("+-------+----+------------------------------------------+--------+-------+-------+-------+\n")
			cecho(string.format(
				"| Kowal | Lp | <yellow>%40.40s<reset> | <white>Jakość<reset> | Klute | Ciete | Obuch |\n", self.pojemnik))
			echo("+-------+----+------------------------------------------+--------+-------+-------+-------+\n")
		end
		for k, v in pairs(self.items) do
			local nap_kol = "white"
			if v.naprawialnosc == "!" then
				nap_kol = "red"
			elseif v.naprawialnosc == "$" then
				nap_kol = "yellow"
			end
			if v.typ == "tarcza" then
				local jakosc = string.format("%.2f", v.waga)
				cechoLink(
					string.format(
						"|   <%s>%s<reset>   | <white>%2s<reset> | <white>%40.40s <reset>|<white> %6s <reset>| %6s/12 | %2s/12 | %2s/12 | %2s/12 |\n",
						nap_kol, v.naprawialnosc, v.lp, v.short, jakosc, v.parowanie, v.klute, v.ciete, v.obuchowe),
					function() send("wybierz " .. v.lp .. ". " .. v.short) end, "wybierz " .. v.lp .. ". " .. v.short, true)
			else
				local srednia = string.format("%.2f", v.waga)
				cechoLink(
					string.format(
						"|   <%s>%s<reset>   | <white>%2s<reset> | <white>%40.40s <reset>|<white> %6s <reset>| %2s/12 | %2s/12 | %2s/12 |\n",
						nap_kol, v.naprawialnosc, v.lp, v.short, srednia, v.klute, v.ciete, v.obuchowe),
					function() send("wybierz " .. v.lp .. ". " .. v.short) end, "wybierz " .. v.lp .. ". " .. v.short, true)
			end
		end
		if self.items[1].typ == "tarcza" then
			echo(
				"+-------+----+------------------------------------------+--------+-----------+-------+-------+-------+\n")
		else
			echo("+-------+----+------------------------------------------+--------+-------+-------+-------+\n")
		end
	else
		if self.items[1].typ == "puklerz" then
			echo("+-------+----+------------------------------------------+-----------+\n")
			cecho(string.format("| Kowal | Lp | <yellow>%40.40s<reset> | <white>Parowanie<reset> |\n", self.pojemnik))
			echo("+-------+----+------------------------------------------+-----------+\n")
		else
			echo(
				"+-------+----+------------------------------------------+--------+-----------------+-------------------------+---------+--------+\n")
			cecho(string.format(
				"| Kowal | Lp | <yellow>%40.40s<reset> | <white>Jakość<reset> |      Chwyt      |        Obrażenia        | Celność |  Siła  |\n",
				self.pojemnik))
			echo(
				"+-------+----+------------------------------------------+--------+-----------------+-------------------------+---------+--------+\n")
		end
		for k, v in pairs(self.items) do
			local nap_kol = "white"
			if v.naprawialnosc == "!" then
				nap_kol = "red"
			elseif v.naprawialnosc == "$" then
				nap_kol = "yellow"
			end
			if v.typ == "puklerz" then
				cechoLink(
					string.format(
						"|   <%s>%s<reset>   | <white>%2s<reset> | <white>%40.40s <reset>|<white> %6s/12 <reset>|\n", nap_kol,
						v.naprawialnosc, v.lp, v.short, v.waga), function() send("wybierz " .. v.lp .. ". " .. v.short) end,
					"wybierz " .. v.lp .. ". " .. v.short, true)
			else
				local bkolor = "white"
				if v.srebro == "tak" then
					bkolor = "cyan"
				end
				if v.magia == "tak" then
					bkolor = "magenta"
				end
				cechoLink(
					string.format(
						"|   <%s>%s<reset>   | <white>%2s<reset> | <" ..
						bkolor .. ">%40.40s <reset>|<white> %6s <reset>| %15s | %23s | %4s/14 | %3s/14 |\n", nap_kol,
						v.naprawialnosc, v.lp, v.short, v.waga, v.chwyt, v.obrazenia, v.wywazenie, v.skutecznosc),
					function() send("wybierz " .. v.lp .. ". " .. v.short) end, "wybierz " .. v.lp .. ". " .. v.short, true)
			end
		end
		if self.items[1].typ == "puklerz" then
			echo("+-------+----+------------------------------------------+-----------+\n")
		else
			echo(
				"+-------+----+------------------------------------------+---------+-----------------+-------------------------+---------+--------+\n")
		end
	end

	cecho("<white>Przedmioty zostały posortowane od najgorszego, do najlepszego. Kliknij by wziac.\n")


	Msmudlet.wybierz:cleanup()
end

Msmudlet.wybierz:init()
