
-- 

function scripts.inv.collect:killed_action()
	if scripts.inv.collect["current_mode"] ~= 7 or table.size(scripts.inv.collect.extra) > 0 then
		scripts.inv.collect.check_body = true
		if Msmudlet.autko.grab_status == "off" then
			cecho("\n<" .. scripts.ui:get_bind_color_backward_compatible() .. ">[bind <yellow>" .. scripts.keybind:keybind_tostring("collect_from_body") .. "<" .. scripts.ui:get_bind_color_backward_compatible() .. "> wez z ciala\n")
		else
			cecho("\n<" .. scripts.ui:get_bind_color_backward_compatible() .. ">Autko -> <" .. scripts.ui:get_bind_color_backward_compatible() .. "> Grabie jak grabiezca.\n")
			Msmudlet.autko:kolejkuj("grabie", "normal", function() scripts.inv.collect:key_pressed(false) end, 1, true)
		end
	end
end

function scripts.inv.collect:team_killed_action(name)
	if scripts.inv.collect["current_mode"] ~= 4 and scripts.inv.collect["current_mode"] ~= 5
					and scripts.inv.collect["current_mode"] ~= 6 and table.size(scripts.inv.collect.extra) == 0 then
			return
	end

	if ateam.team_names[name] then
		scripts.inv.collect.check_body = true
		if Msmudlet.autko.grab_status == "off" then
			cecho("\n<" .. scripts.ui:get_bind_color_backward_compatible() .. ">[bind <yellow>ctrl+3]<" .. scripts.ui:get_bind_color_backward_compatible() .. "> wez z ciala\n")
		else
			cecho("\n<" .. scripts.ui:get_bind_color_backward_compatible() .. ">Autko -> <" .. scripts.ui:get_bind_color_backward_compatible() .. "> Grabie jak grabiezca.\n")
			Msmudlet.autko:kolejkuj("grabie", "normal", function() scripts.inv.collect:key_pressed(false) end, 1, true)
		end
	end
end

