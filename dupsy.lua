---@diagnostic disable: redundant-parameter
Msmudlet.dupsy = Msmudlet.dupsy or {
  aliasy = {},
  triggery = {},
  bindy = {},
  funkcje = {}
}

function Msmudlet.dupsy:init()
  -- ob plecak
  if self.aliasy.plecak then killAlias(self.aliasy.plecak) end
  self.aliasy.plecak = tempAlias("^/plecak$",
    function() sendAll("otworz swoj plecak", "ob swoj plecak", "zamknij swoj plecak") end)

  -- ob.torbe
  if self.aliasy.torba then killAlias(self.aliasy.torba) end
  self.aliasy.torba = tempAlias("^/torba$",
    function() sendAll("otworz swoja torbe", "ob swoja torbe", "zamknij swoja torbe") end)

  -- ob.sakiewke
  if self.aliasy.sakiewka then killAlias(self.aliasy.sakiewka) end
  self.aliasy.sakiewka = tempAlias("^/sakiewka$",
    function() sendAll("otworz swoja sakiewke", "ob swoja sakiewke", "zamknij swoja sakiewke") end)

  -- ob.sakwe
  if self.aliasy.sakwa then killAlias(self.aliasy.sakwa) end
  self.aliasy.sakwa = tempAlias("^/sakwa$",
    function() sendAll("otworz swoja sakwe", "ob swoja sakwe", "zamknij swoja sakwe") end)

  -- napij sie z buklaka
  if self.aliasy.buk then killAlias(self.aliasy.buk) end
  self.aliasy.buk = tempAlias("^/buk$", function() send("napij sie z buklaka") end)

  -- porownaj bojowki
  if self.aliasy.porownaj then killAlias(self.aliasy.porownaj) end
  self.aliasy.porownaj = tempAlias("^/porownaj (.*)$",
    function()
      send("porownaj sile z " .. matches[2])
      send("porownaj zrecznosc z " .. matches[2])
      send("porownaj wytrzymalosc z "
        .. matches[2])
    end)

  -- sprawdz kierunek
  if self.aliasy.kompas then killAlias(self.aliasy.kompas) end
  self.aliasy.kompas = tempAlias("^/kompas$",
    function()
      send("otworz gnomia torbe")
      send("wez kompas z torby")
      send("sprawdz kierunek")
      send("wloz kompasy do gnomiej torby")
      send("zamknij gnomia torbe")
    end)


  -- dpl
  if self.aliasy.dpl then killAlias(self.aliasy.dpl) end
  self.aliasy.dpl = tempAlias("^/dpl ([a-z ,0-9]+)$", function()
    if string.find(matches[2], 'monet') then
      scripts.inv:put_into_bag({ matches[2] }, "money", 1)
    elseif string.find(matches[2], 'kamien') then
      scripts.inv:put_into_bag({ matches[2] }, "gems", 1)
    else
      alias_func_skrypty_inventory_bags_wdp()
    end
  end)

  -- zpl

  if self.aliasy.zpl then killAlias(self.aliasy.zpl) end
  self.aliasy.zpl = tempAlias("^/zpl ([a-z ,0-9]+)", function()
    if string.find(matches[2], 'monet') then
      scripts.inv:get_from_bag({ matches[2] }, "money", 1)
    elseif string.find(matches[2], 'kamien') then
      scripts.inv:get_from_bag({ matches[2] }, "gems", 1)
    else
      alias_func_skrypty_inventory_bags_wzp()
    end
  end)

  -- alias /kokony
  function self.funkcje.kokon()
    if self.aliasy.vars_ile_kokonow == nil then
      return
    end
    local ilei
    ilei = scripts.counted_string_to_int[self.aliasy.vars_ile_kokonow]

    for i = 1, ilei, 1 do
      if i == 1 then
        send("rozerwij kokon")
      else
        send("rozerwij " .. i .. ". kokon")
      end
      send("wez wszystko z " .. i .. ". kokonu")
    end
    self.aliasy.vars_ile_kokonow = nil
  end -- function kokon

  if self.aliasy.kokony then killAlias(self.aliasy.kokony) end
  self.aliasy.kokony = tempAlias("^/kokony$", function()
    self.triggery.kokony = tempRegexTrigger("^.*Doliczyl.s sie ([a-z]+) sztuk(|i)\\.$",
      function()
        self.aliasy.vars_ile_kokonow = matches[2]
        Msmudlet.autko:kolejkuj("rozerwij kokon", "fast",
          function() Msmudlet.dupsy.funkcje.kokon() end, 1, true)
      end, 1)
    self.triggery.czysc_kokony = tempRegexTrigger("^.*Nie widzisz tu niczego takiego\\.$",
      function() killTrigger(Msmudlet.dupsy.triggery.kokony) end, 1)
    tempTimer(4, [[ killTrigger("]] .. Msmudlet.dupsy.triggery.kokony .. [[") ]])
    tempTimer(4, [[ killTrigger("]] .. Msmudlet.dupsy.triggery.czysc_kokony .. [[") ]])
    send("policz kokony")
  end)
  -- koniec alias /kokony


  -- alias /zbierz
  function self.funkcje:zbcial()
    if Msmudlet.dupsy.aliasy.vars_zbierz_co == nil then
      return
    end
    if Msmudlet.dupsy.aliasy.vars_zbierz_ile == nil then
      return
    end
    local ilei = scripts.counted_string_to_int[Msmudlet.dupsy.aliasy.vars_zbierz_ile]
    for i = 1, ilei, 1 do
      if i == 1 then
        send("wez " .. Msmudlet.dupsy.aliasy.vars_zbierz_co .. " z ciala")
      else
        send("wez " .. Msmudlet.dupsy.aliasy.vars_zbierz_co .. " z " .. i .. ". ciala")
      end
    end
    Msmudlet.dupsy.aliasy.vars_zbierz_co = nil
    Msmudlet.dupsy.aliasy.vars_zbierz_ile = nil
  end -- function zbcial

  if self.aliasy.zbierz then killAlias(self.aliasy.zbierz) end
  self.aliasy.zbierz = tempAlias("^/zbierz (.*)$", function()
    self.aliasy.vars_zbierz_co = matches[2]
    self.triggery.zbierz = tempRegexTrigger("^.*Doliczyl.s sie ([a-z]+) sztuk(|i)\\.$",
      function()
        Msmudlet.dupsy.aliasy.vars_zbierz_ile = matches[2]
        Msmudlet.autko:kolejkuj("zbierz z cial", "fast",
          function() Msmudlet.dupsy.funkcje:zbcial() end, 1, true)
      end, 1)
    self.triggery.czysc_zbierz = tempRegexTrigger("^.*Nie widzisz tu niczego takiego\\.$",
      function() killTrigger(Msmudlet.dupsy.triggery.zbierz) end, 1)
    tempTimer(4, [[ killTrigger("]] .. Msmudlet.dupsy.triggery.zbierz .. [[") ]])
    tempTimer(4, [[ killTrigger("]] .. Msmudlet.dupsy.triggery.czysc_zbierz .. [[") ]])
    send("policz ciala")
  end)
  -- koniec alias /zbierz


  -- alias /cial
  function self.funkcje.cial()
    if self.aliasy.vars_cial_ile == nil then
      return
    end
    local ilei = scripts.counted_string_to_int[self.aliasy.vars_cial_ile]
    for i = 1, ilei, 1 do
      if i == 1 then
        send("ob cialo")
      else
        send("ob " .. i .. ". cialo")
      end
    end
    self.aliasy.vars_cial_ile = nil
  end -- function cial

  if self.aliasy.cial then killAlias(self.aliasy.cial) end
  self.aliasy.cial = tempAlias("^/cial$", function()
    self.triggery.cial = tempRegexTrigger("^.*Doliczyl.s sie ([a-z]+) sztuk(|i)\\.$",
      function()
        Msmudlet.dupsy.aliasy.vars_cial_ile = matches[2]
        Msmudlet.autko:kolejkuj("obejrzyj ciala", "fast",
          function() Msmudlet.dupsy.funkcje.cial() end, 1, true)
      end, 1)
    self.triggery.czysc_cial = tempRegexTrigger("^.*Nie widzisz tu niczego takiego\\.$",
      function() killTrigger(Msmudlet.dupsy.triggery.cial) end, 1)
    tempTimer(4, [[ killTrigger("]] .. Msmudlet.dupsy.triggery.cial .. [[") ]])
    tempTimer(4, [[ killTrigger("]] .. Msmudlet.dupsy.triggery.czysc_cial .. [[") ]])
    send("policz ciala")
  end)
  -- koniec alias /cial

  -- k -> kondycja wszystkich
  if self.aliasy.kw then killAlias(self.aliasy.kw) end
  self.aliasy.kw = tempAlias("^/k$", function() sendAll("kondycja wszystkich") end)


  -- kufer_pokaz -> wyjmij wszystkie flaki z kuferka
  if self.aliasy.kufer_pokaz then killAlias(self.aliasy.kufer_pokaz) end
  self.aliasy.kufer_pokaz = tempAlias("^/kufer_pokaz$",
    function()
      sendAll("otworz kufer", "wez szczatki z kufra", "odloz je", "wez szczatki z kufra", "odloz je",
        "wez bronie z kufra;odloz je", "wez wszystkie zbroje z kufra", "odloz je", "wez wszystkie zbroje z kufra",
        "odloz je", "wez ubrania z kufra", "odloz je", "wez pochwy z kufra", "odloz je", "wez kielich z kufra",
        "odloz go", "wez wszystkie bizuterie z kufra", "odloz je", "wez pek z kufra", "odloz go", "wez fajki z kufra",
        "odloz je", "wez plaskorzezbe z kufra", "odloz ja", "wez luske z kufra", "odloz luske", "zamknij kufer")
    end)

  -- kufer_spakuj -> schowaj wszystkie flaki do kuferka
  if self.aliasy.kufer_spakuj then killAlias(self.aliasy.kufer_spakuj) end
  self.aliasy.kufer_spakuj = tempAlias("^/kufer_spakuj$",
    function()
      sendAll("otworz kufer", "wez szczatki", "wloz je do kufra", "wez szczatki", "otworz kufer",
        "wloz szczatki do kufra", "wez bronie", "wloz je do kufra", "wez wszystkie zbroje", "wloz je do kufra",
        "wez wszystkie ubrania", "wloz je do kufra", "wez pochwy", "wloz je do kufra", "wez kielichy", "wloz je do kufra",
        "wez wszystkie bizuterie", "wloz je do kufra", "wez pek", "wloz go do kufra", "wez fajki", "wloz je do kufra",
        "wez plaskorzezbe", "wloz ja do kufra", "wez wszystkie zbroje", "wloz je do kufra", "wez luske",
        "wloz ja do kufra", "zamknij kufer")
    end)




  -- ott -> ob tabliczki, tablice, etc
  if self.aliasy.ott then killAlias(self.aliasy.ott) end
  self.aliasy.ott = tempAlias("^ott$",
    function() sendAll("ob tablice", "ob tabliczke", "przeczytaj tablice", "przeczytaj tabliczke", "ob menu") end)

  -- /gustiks -> zamawianie zeru w mesie u Gustiksa
  if self.aliasy.gustiks then killAlias(self.aliasy.gustiks) end
  self.aliasy.gustiks = tempAlias("^/gustiks$", function()
    scripts.inv:get_from_bag({ "monety" }, "money", 1)
    sendAll("usiadz przy drugim ciemnym prostokatnym stoliku", "zamow jajecznice", "zamow jajecznice",
      "zamow jajecznice",
      "zamow jablko", "zamow jablko", "zamow jablko", "zamow jablko", "zamow jablko", "zamow jablko", "wstan")
    scripts.inv:put_into_bag({ "monety" }, "money", 1)
  end)

  -- /gnzbuduj (co?)
  if self.aliasy.gnzbuduj then killAlias(self.aliasy.gnzbuduj) end
  self.aliasy.gnzbuduj = tempAlias("^/gnzbuduj ([0-9]+) ([0-9a-z A-Z]+)$", function()
    local ile = matches[2]
    local co = matches[3]
    if co == "Kompas" then
      local czas = 32
      for i = 0, ile - 1 do
        Msmudlet.autko:kolejkuj("/gnzbuduj", czas * i * 10 + 5, function()
          cecho("\n     <white>Buduje <green>Kompas<white> " .. i + 1 .. " z " .. ile .. "\n\n")
          sendAll("otworz szafe", "wybierz male poreczne cazki", "wybierz solidne stalowe szczypce")
          sendAll("wybierz cienka blaszke", "wybierz cienka igle", "wybierz maly korek", "wybierz mala srubke",
            "wybierz mala srubke", "wybierz okragle szkielko")
          send("gnzbuduj Kompas")
        end, 100, true)
      end
      Msmudlet.autko:kolejkuj("/gnzbuduj odloz narzedzia", czas * ile * 10 + 15,
        function() sendAll("otworz szafe", "wloz narzedzia do szafy") end, 1,
        true)
    elseif co == "205 Stalowa brosza w ksztalcie wloczni" then
      local czas = 62
      for i = 0, ile - 1 do
        Msmudlet.autko:kolejkuj("/gnzbuduj", czas * i * 10 + 5, function()
          cecho("\n     <white>Buduje <green>205 Stalowa brosza w ksztalcie wloczni<white> " ..
            i + 1 .. " z " .. ile ..
            "\n\n")
          sendAll("otworz szafe", "wybierz male poreczne cazki", "wybierz solidne stalowe szczypce",
            "wybierz metalowy slusarski bukfel", "wybierz smukly poreczny pilnik ")
          sendAll("wybierz cienka blaszke", "wybierz cienka igle", "wybierz krotki cynowy drucik",
            "wybierz gietki precik",
            "wybierz szarosrebrna gladka sztabke stali")
          send("gnzbuduj 205 Stalowa brosza w ksztalcie wloczni")
        end, 100, true)
      end
      Msmudlet.autko:kolejkuj("/gnzbuduj odloz narzedzia", czas * ile * 10 + 15,
        function() sendAll("otworz szafe", "wloz narzedzia do szafy") end, 1,
        true)
    elseif co == "209 Szczurza spinka" then
      local czas = 56
      for i = 0, ile - 1 do
        Msmudlet.autko:kolejkuj("/gnzbuduj", czas * i * 10 + 5, function()
          cecho("\n     <white>Buduje <green>209 Szczurza spinke<white> " .. i + 1 .. " z " .. ile ..
            "\n\n")
          sendAll("otworz szafe", "wybierz solidne stalowe szczypce", "wybierz smukla dluga igle",
            "wybierz poreczny lekki mlotek", "wybierz krotki grawerski rylec")
          sendAll("wybierz cienka blaszke", "wybierz cienka igle", "wybierz krotki cynowy drucik",
            "wybierz prosty metalowy haczyk", "wybierz niewielka sprezynke",
            "wybierz rudawa polyskliwa sztabke miedzi")
          send("gnzbuduj 209 Szczurza spinka")
        end, 100, true)
      end
      Msmudlet.autko:kolejkuj("/gnzbuduj odloz narzedzia", czas * ile * 10 + 15,
        function() sendAll("otworz szafe", "wloz narzedzia do szafy") end, 1,
        true)
    elseif co == "197 Matowy skorzany pas z bandolierem" then
      local czas = 40
      for i = 0, ile - 1 do
        Msmudlet.autko:kolejkuj("/gnzbuduj", czas * i * 10 + 5, function()
          cecho("\n     <white>Buduje <green>197 Matowy skorzany pas z bandolierem<white> " ..
            i + 1 .. " z " .. ile ..
            "\n\n")
          sendAll("otworz szafe", "wybierz spore mocne imadlo", "wybierz smukla dluga igle",
            "wybierz poreczny lekki mlotek", "wybierz lsniace wygodne nozyczki",
            "wybierz solidne stalowe szczypce")
          sendAll("wybierz 4 cienkie blaszki", "wybierz 4 krotkie druciki", "wybierz cienka igle",
            "wybierz duzy plat skory", "wybierz metalowa ramke", "wybierz mala srubke")
          send("gnzbuduj 197 Matowy skorzany pas z bandolierem")
        end, 100, true)
      end
      Msmudlet.autko:kolejkuj("/gnzbuduj odloz narzedzia", czas * ile * 10 + 15,
        function() sendAll("otworz szafe", "wloz narzedzia do szafy") end, 1,
        true)
    else
      echo("Nie mam listy komponentow dla tego wynalazku. Buduj recznie.")
    end
  end)

  if self.aliasy.gnzbuduj_pomoc then killAlias(self.aliasy.gnzbuduj_pomoc) end
  self.aliasy.gnzbuduj_pomoc = tempAlias("^/gnzbuduj$", function()
    cecho("+-----------------------------------------------------------------------------------+\n")
    cecho("| <yellow>WAU<grey>                                                                               |\n")
    cecho("|                                                                                   |\n")
    cecho(
      "| <light_slate_blue>/gnzbuduj <ile_sztuk> <nazwa wynalazku z listy poniżej>    <grey>                       |\n")
    cecho("|                                                                                   |\n")
    cecho("| <grey> Lista wynalazków:                                                                |\n")
    cecho("| <grey>  - Kompas                                                                        |\n")
    cecho("| <grey>  - 197 Matowy skorzany pas z bandolierem                                         |\n")
    cecho("| <grey>  - 205 Stalowa brosza w ksztalcie wloczni                                        |\n")
    cecho("| <grey>  - 209 Szczurza spinka                                                           |\n")
    cecho("|                                                                                   |\n")
    cecho("|                                                                                   |\n")
    cecho("| <grey> Działa tylko w 'Laboratorium testowania wynalazków!'                             |\n")
    cecho("| <grey> Nowe wynalazki do skryptów dodaje Szydell.                                       |\n")
    cecho("|                                                                                   |\n")
    cecho("+-----------------------------------------------------------------------------------+\n")
  end)


  if self.aliasy.pakuj_ziola then killAlias(self.aliasy.pakuj_ziola) end
  self.aliasy.pakuj_ziola = tempAlias("^/zio1$", function()
    herbs:do_pre_actions()
    sendAll("odloz osty", "odloz trawy", "otworz woreczki")

    local bags_amount = 15
    if herbs.bags_amount then
      bags_amount = herbs.bags_amount
    end

    for i = 1, bags_amount do
      sendAll("wloz ziola do " .. i .. ". woreczka")
    end
    sendAll("zamknij woreczki")
    herbs:do_post_actions()
  end)

  if self.aliasy.wypakuj_ziola then killAlias(self.aliasy.wypakuj_ziola) end
  self.aliasy.wypakuj_ziola = tempAlias("^/zio0$", function()
    herbs:do_pre_actions()
    sendAll("otworz woreczki")
    local bags_amount = 15
    if herbs.bags_amount then
      bags_amount = herbs.bags_amount
    end

    for i = 1, bags_amount do
      sendAll("wez ziola z " .. i .. ". woreczka")
    end
    sendAll("zamknij woreczki")
    herbs:do_post_actions()
  end)

  -- sprzedawanie ziół
  if self.aliasy.sprzedaj_ziola then killAlias(self.aliasy.sprzedaj_ziola) end
  self.aliasy.sprzedaj_ziola = tempAlias("^/ziola_sprzedaj$", function()
    if not herbs.db or table.size(herbs.db) == 0 then
      cecho("\n <orange>Brak zbudowanej bazy ziol, ")
      cechoPopup("<deep_sky_blue>kliknij tutaj aby zrobic '/ziola_buduj'",
        { [[expandAlias("/ziola_buduj")]] },
        { [[/ziola_buduj]] }, true)
      cecho(" \n\n")
      return
    end
    local to_sell = {}
    local biernik = { 2, 3, 4 }

    for _, herb_id in pairs(herbs.sorted_herb_ids) do
      local dq = 0
      if herbs.data.herb_id_to_use[herb_id].desired_quantity then
        dq = herbs.data.herb_id_to_use[herb_id].desired_quantity
      end
      if herbs.herbs_details[herb_id].forsell and herbs.counts[herb_id] - dq > 0 then
        table.insert(to_sell, herb_id)
      end
    end
    if table.size(to_sell) > 0 then
      herbs:do_pre_actions()
      for _, herb_id in pairs(to_sell) do
        local dq = 0
        if herbs.data.herb_id_to_use[herb_id].desired_quantity then
          dq = herbs.data.herb_id_to_use[herb_id].desired_quantity
        end
        local herb_count = herbs.counts[herb_id] - dq
        local herb_name = herbs:get_case(herb_id, herb_count)
        herbs:get_herbs(herb_id, tonumber(herb_count))
      end
      send("sprzedaj ziola")
      herbs:do_post_actions()
    else
      cecho("\n <orange>Brak ziol na sprzedaz.")
    end
  end)

  -- przygotowanie ziół do sprzedaży
  if self.aliasy.ziola_przygotuj then killAlias(self.aliasy.ziola_przygotuj) end
  self.aliasy.ziola_przygotuj = tempAlias("^/ziola_przygotuj$", function()
    if not herbs.db or table.size(herbs.db) == 0 then
      cecho("\n <orange>Brak zbudowanej bazy ziol, ")
      cechoPopup("<deep_sky_blue>kliknij tutaj aby zrobic '/ziola_buduj'",
        { [[expandAlias("/ziola_buduj")]] },
        { [[/ziola_buduj]] }, true)
      cecho(" \n\n")
      return
    end
    local to_sell = {}
    local biernik = { 2, 3, 4 }

    for _, herb_id in pairs(herbs.sorted_herb_ids) do
      local dq = 0
      if herbs.data.herb_id_to_use[herb_id].desired_quantity then
        dq = herbs.data.herb_id_to_use[herb_id].desired_quantity
      end
      if herbs.herbs_details[herb_id].forsell and herbs.counts[herb_id] - dq > 0 then
        table.insert(to_sell, herb_id)
      end
    end
    if table.size(to_sell) > 0 then
      herbs:do_pre_actions()
      for _, herb_id in pairs(to_sell) do
        local dq = 0
        if herbs.data.herb_id_to_use[herb_id].desired_quantity then
          dq = herbs.data.herb_id_to_use[herb_id].desired_quantity
        end
        local herb_count = herbs.counts[herb_id] - dq
        local herb_name = herbs:get_case(herb_id, herb_count)
        herbs:get_herbs(herb_id, tonumber(herb_count))
      end
      herbs:do_post_actions()
    else
      cecho("\n <orange>Brak ziol na sprzedaz.")
    end
  end)

  function self.funkcje.rozwiaz_hamiltona(item, cyfry)
    local pudelko = getMudletHomeDir() .. "/plugins/msmudlet/py/pudelko.py"
    --local h = spawn(display, "python", pudelko,item,cyfry)
    local h = spawn(Msmudlet.dupsy.funkcje.wyswietl_hamiltona, "python", pudelko, item, cyfry)
    if h ~= nil then
      h.close()
    end
  end

  function self.funkcje.wyswietl_hamiltona(wynik)
    if wynik == "Brak rozwiazania. Sprawdz poprawnosc wprowadzonych wierzcholkow.\n" then
      cecho("<blue>----><yellow> " .. wynik .. "<grey>")
    else
      cechoLink("<blue>----><grey> " .. wynik, function() sendAll(wynik) end, "kliknij", true)
    end
  end

  if self.aliasy.pudelko then killAlias(self.aliasy.pudelko) end
  self.aliasy.pudelko = tempAlias("^/pudelko ([0-9]{16})$", function()
    local cyfry = matches[2]
    Msmudlet.dupsy.funkcje.rozwiaz_hamiltona("pudelko", cyfry)
  end)

  if self.aliasy.zegar_golemow then killAlias(self.aliasy.zegar_golemow) end
  self.aliasy.zegar_golemow = tempAlias("^/zegar_golemow ([0-9]{12})$", function()
    local cyfry = matches[2]
    Msmudlet.dupsy.funkcje.rozwiaz_hamiltona("zegar_golemow", cyfry)
  end)

  -- alias /ethel - sprawdza stan wiedzy o różnych istotach
  if self.aliasy.ethel then killAlias(self.aliasy.ethel) end
  self.aliasy.ethel = tempAlias("^/ethel$",
    function()
      sendAll(
        "wiedza o chaosie i jego tworach",
        "wiedza o goblinoidach",
        "wiedza o golemach",
        "wiedza o istotach demonicznych",
        "wiedza o jaszczuroludziach",
        "wiedza o magii i jej tworach",
        "wiedza o nieumarlych",
        "wiedza o pajakach i pajakowatych",
        "wiedza o ryboludziach",
        "wiedza o smokach i smokowatych",
        "wiedza o starszych rasach",
        "wiedza o stworach pokoniunkcyjnych",
        "wiedza o szczuroludziach",
        "wiedza o wampirach"
      )
    end)

  -- rozrzewanie generatora liczb pseudolosowych
  math.randomseed(getEpoch() * 1000)
  for i = 1, 100 do
    math.random()
  end
end

Msmudlet.dupsy:init()

-- globalny 'error-catch' na błędy ściągania plików
function DownloadErrorEventHandler(event, errorFound, localFilename, usedUrl)
  cecho("fuction downloadErrorEventHandler, " .. errorFound)
  debugc("fuction downloadErrorEventHandler, " .. errorFound) --display to error screen in editor
end                                                           --function downloadErrorEventHandler

registerAnonymousEventHandler("sysDownloadError", "DownloadErrorEventHandler")
