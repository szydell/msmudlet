Msmudlet.zaslony = Msmudlet.zaslony or {
  druzyna = {}
}

-- ["ateam.my_id"] - moje id
-- ateam.objs - obiekty typu gracze, enpece itd. Nie tylko z lokacji na której jestem

-- przykładowo endriaga bije mnie:
-- [516043] = {
--	attack_num = 512637,
--	attack_target = false,
--	avatar_target = true,
-- 	defense_target = false,
-- 	desc = "ogromna agresywna endriaga",
-- 	enemy = true,
-- 	hidden = false,
-- 	hp = 0,
-- 	living = true,
--	team = false,
-- 	team_leader = false
-- }

-- ateam.objs[ateam.my_id] (Ja bije endriage):
-- {
--	attack_num = 516043,
--  avatar = true,
--  can_see_in_room = true,
--  defense_target = false,
--  editing = false,
--  hidden = false,
--  hp = 6,
--  living = true,
--  paralyzed = false,
--  team = false,
--  team_leader = false
-- }

-- członek drużyny"
-- [520130] = {
--	attack_num = 520397,
--	attack_target = false,
--	avatar_target = false,
--	defense_target = false,
--	desc = "Ulik",
--	enemy = false,
--	hidden = false,
--	hp = 5,
--	living = true,
--	team = true,
--	team_leader = false
-- },

-- hp=6 - świetna
-- hp=0 - ledwo

function Msmudlet.zaslony:zaslon()
  if not ateam.objs[ateam.my_id].team then -- nie licz zasłon gdy nie jesteś w drużynie
    return
  end

  ---@diagnostic disable-next-line: undefined-field
  if table.size(ateam.team_enemies) < 1 then -- nie licz zasłon kiedy nie walczymy
    return
  end


  local druzyna = {}

  local me = {
    hp = 0,
    enemies = {},
    defense_target = false
  }

  local kogo_zaslonic = {
    id = nil,
    max_priorytet_zaslony = 0
  }

  local za_kogo_wycofac = {
    id = nil,
    max_priorytet_wycofania = 0
  }

  --
  for _, v in pairs(gmcp.objects.nums) do
    if v == ateam.my_id then
      me.hp = ateam.objs[v].hp
      me.defense_target = ateam.objs[v].defense_target
    else
      if ateam.objs[v].team then
        druzyna[v] = {}
        druzyna[v].hp = ateam.objs[v].hp
        druzyna[v].desc = ateam.objs[v].desc
      end
    end
  end

  for id, v in pairs(ateam.objs) do
    ---@diagnostic disable-next-line: undefined-field
    if table.contains(gmcp.objects.nums, id) and v.enemy then
      if v.attack_num == ateam.my_id then
        table.insert(me.enemies, id)
      else
        if druzyna[v.attack_num] then
          if not druzyna[v.attack_num].enemies then
            druzyna[v.attack_num].enemies = {}
          end
          table.insert(druzyna[v.attack_num].enemies, id)
        end
      end
    end
  end

  local my_enemies_cnt = 0
  if me.enemies then
---@diagnostic disable-next-line: cast-local-type
    my_enemies_cnt = table.size(me.enemies)
  end

  for id, v in pairs(druzyna) do
    local teammate_enemies = 0
    if v.enemies then
---@diagnostic disable-next-line: cast-local-type
      teammate_enemies = table.size(v.enemies)
    end

    local modifier = 0

    if me.defense_target then
      modifier = -1
    end

    druzyna[id].priorytet_zaslony = teammate_enemies * 0.7 - my_enemies_cnt * 0.7 +
        me.hp - v.hp + modifier
    if Msmudlet.postac.zawod == "Legionista" then
      druzyna[id].priorytet_wycofki = tonumber(string.format('%.3f', teammate_enemies * 0.7 - my_enemies_cnt * 0.7 +
        me.hp - v.hp + modifier + 0.7))
    end
    -- echo("\nStatus wycofki po bazowym wyliczeniu:" .. self.druzyna[id].priorytet_wycofki .. "\n\n\n")

    if ateam.objs[id].defense_target then
      druzyna[id].priorytet_zaslony = druzyna[id].priorytet_zaslony + 0.7
      if Msmudlet.postac.zawod == "Legionista" then
        druzyna[id].priorytet_wycofki = druzyna[id].priorytet_wycofki + 0.7
      end
    end
    if ateam.objs[id].paralyzed then
      druzyna[id].priorytet_zaslony = druzyna[id].priorytet_zaslony + 0.3
      if Msmudlet.postac.zawod == "Legionista" then
        druzyna[id].priorytet_wycofki = druzyna[id].priorytet_wycofki + 1
      end
    end
    for _, enemy_id in pairs(me.enemies) do -- nie spiesz się z zasłoną gdy tankujesz trolla
      if string.find(ateam.objs[enemy_id].desc, "troll") then
        druzyna[id].priorytet_zaslony = druzyna[id].priorytet_zaslony - 0.7
        if Msmudlet.postac.zawod == "Legionista" then -- gdy tankujesz trolla, to dolicz to do wycofek
          druzyna[id].priorytet_wycofki = druzyna[id].priorytet_wycofki - 0.7
        end
      end
    end


    if v.enemies and Msmudlet.postac.zawod == "Legionista" then
      for _, enemy_id in pairs(v.enemies) do -- tankujący trolla ma 2x gorzej.
        if string.find(ateam.objs[enemy_id].desc, "troll") then
          druzyna[id].priorytet_wycofki = druzyna[id].priorytet_wycofki + 0.7
        end
      end
    end

    if teammate_enemies == 0 then -- nie zasłaniaj gdy teammate nie ma wrogów
      druzyna[id].priorytet_zaslony = 0
    end


    if kogo_zaslonic.max_priorytet_zaslony < druzyna[id].priorytet_zaslony then
      kogo_zaslonic.max_priorytet_zaslony = druzyna[id].priorytet_zaslony
      kogo_zaslonic.id = id
    end
    if Msmudlet.postac.zawod == "Legionista" and za_kogo_wycofac.max_priorytet_wycofania > druzyna[id].priorytet_wycofki then
      za_kogo_wycofac.max_priorytet_wycofania = druzyna[id].priorytet_wycofki
      za_kogo_wycofac.id = id
    end
  end

  -- echo("\n\nStatus zasłony:" .. kogo_zaslonic.max_priorytet_zaslony .. "\n")
  -- echo("Wartość wycofki:" .. za_kogo_wycofac.max_priorytet_wycofania .. "\n\n")


  if kogo_zaslonic.id and kogo_zaslonic.max_priorytet_zaslony >= 1 and table.size(ateam.team_enemies) > 0 then
    ateam:za_func(ateam.team[kogo_zaslonic.id])
    return
  end

  if Msmudlet.postac.zawod == "Legionista" then
    if za_kogo_wycofac.id and za_kogo_wycofac.max_priorytet_wycofania <= -1 and my_enemies_cnt > 0  then
      ateam:w_func(ateam.team[za_kogo_wycofac.id])
      return
    end
  end
end
