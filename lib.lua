Msmudlet = Msmudlet or {}

Msmudlet.lib = Msmudlet.lib or {}

function Msmudlet.lib:getRoomID(area,room)
    local at=getAreaTable()
    local rooms=getAreaRooms(at[area])
    for _, id in pairs(rooms) do
      if getRoomName(id) == room then
        return id
      end
    end
    return -1
    end
    
function Msmudlet.lib:centerView(id)
	if id > 0 then
		amap:set_position(id,false)
	end
end


-- base64decode
function Msmudlet.lib:base64decode(data)
  local b='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
	data = string.gsub(data, '[^'..b..'=]', '')
	return (data:gsub('.', function(x)
			if (x == '=') then return '' end
			local r,f='',(b:find(x)-1)
			for i=6,1,-1 do r=r..(f%2^i-f%2^(i-1)>0 and '1' or '0') end
			return r;
	end):gsub('%d%d%d?%d?%d?%d?%d?%d?', function(x)
			if (#x ~= 8) then return '' end
			local c=0
			for i=1,8 do c=c+(x:sub(i,i)=='1' and 2^(8-i) or 0) end
			return string.char(c)
	end))
end