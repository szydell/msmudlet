Msmudlet.updater = Msmudlet.updater or {
	file = getMudletHomeDir() .. "/plugins/msmudlet/releases",
	url = "https://gitlab.com/api/v4/projects/szydell%2Fmsmudlet/releases",
	handlers = {},
	header = {},
	aliasy = {},
	plugin_zip = "",
	plugin_name = "msmudlet"
}

function Msmudlet.updater:check()
	if self.handlers.error then killAnonymousEventHandler(self.handlers.error) end
	if self.handlers.done then killAnonymousEventHandler(self.handlers.done) end

	self.handlers.done = registerAnonymousEventHandler('sysGetHttpDone', function(event, rurl, response)
		if rurl == Msmudlet.updater.url then Msmudlet.updater:handle(response) else return true end
	end, true)

	self.handlers.error = registerAnonymousEventHandler('sysGetHttpError', function(event, response, rurl)
		if rurl == Msmudlet.updater.url then
			cecho(
				"<red> Wystapił błąd przy ściąganiu listy wersji pluginu. Zrestartuj mudleta i/lub spróbuj ponownie później.\n")
		else
			return true
		end
	end, true)

	getHTTP(self.url, self.header)
end

function Msmudlet.updater:handle(response)
	local contents = yajl.to_value(response)
	local tag = contents[1].tag_name
	if Msmudlet.version ~= tag then
		echo("\n")
		cecho("<CadetBlue>(skrypty)<tomato>: Dla pluginu <white>msmudlet<tomato> dostępna jest aktualizacja. Kliknij ")
		cechoLink("<green>tutaj", function() Msmudlet.updater:update(tag, contents[1].assets.links[1].direct_asset_url) end,
			"Aktualizuj", true)
		cecho(" <tomato>aby pobrać wersję <white>" .. tag .. "<reset>.\n")
		echo("\n")
		cecho("<white>Zmiany:<reset>\n")
		local i = 1
		while contents[i].tag_name ~= Msmudlet.version do
			cecho("<white> " .. contents[i].tag_name .. "<reset>\n" .. contents[i].description .. "\n")
			i = i + 1
		end
		cecho("<white>---<reset>\n")
	else
		cecho("<CadetBlue>(msmudlet)<white> Masz ściągniętą najnowszą wersję pluginu msmudlet (" .. Msmudlet.version .. ")\n")
	end
end

function Msmudlet.updater:update(tag, url)
	scripts.plugins_installer:install_from_url(url)
	tempTimer(5, function() cecho("<red>Plugin <white>msmudlet <red>został zaktualizowany. Zrestartuj mudleta.\n") end)
end

tempTimer(6, function() Msmudlet.updater:check() end)

function Msmudlet.updater:dev()
	local msm_config_file = getMudletHomeDir() .. "/msm_dev.json"
	local file = io.open(msm_config_file, "rb")
	if file then
		Msm_dev_config = yajl.to_value(file:read("*a"))
		file:close()
	else
		cecho("<red>Stworz plik " .. getMudletHomeDir() .. "/msm_dev.json. Przykładowy plik w repo.\n")
		error("Stworz plik " .. getMudletHomeDir() .. "/msm_dev.json. Przykładowy plik w repo.")
	end
	cecho("<CadetBlue>(msmudlet)<yellow> Sciezka do pliku: " .. Msm_dev_config.dev_path .. "/msmudlet.zip")

	self.plugin_zip = Msm_dev_config.dev_path .. "/msmudlet.zip"
	local reference = os.time() .. self.plugin_name

	scripts.event_register:register_event_handler("sysUnzipDone", function(event, ...)
		Msmudlet.updater:handle_dev_unzip(event, Msmudlet.updater.plugin_name, reference, ...)
	end, true)
	scripts.event_register:register_event_handler("sysUnzipError", function(event, ...)
		Msmudlet.updater:handle_dev_unzip(event, Msmudlet.updater.plugin_name, reference, ...)
	end, true)
	unzipAsync(self.plugin_zip, scripts.plugins_installer.plugin_directory .. reference)
end

function Msmudlet.updater:init()
	if self.aliasy.sprawdz then killAlias(self.aliasy.sprawdz) end
	self.sprawdz = tempAlias("^/msmudlet_sprawdz$", function()
		cecho("\n\n<CadetBlue>(msmudlet)<yellow> Sprawdzam dostępność nowej wersji skryptów.\n\n")
		Msmudlet.updater:check()
	end)
	if self.aliasy.dev then killAlias(self.aliasy.dev) end
	self.aliasy.dev = tempAlias("^/msmudlet_dev$", function()
		cecho("\n\n<CadetBlue>(msmudlet)<yellow> Laduje skrypty z lokalnego repozytorium.\n\n")
		Msmudlet.updater:dev()
	end)
end

function Msmudlet.updater:handle_dev_unzip(event, plugin_name, reference, ...)
	if event == "sysUnzipDone" then
		if reference then
			local base_name = scripts.plugins_installer.plugin_directory .. reference
			local files = lfs.list_files(base_name)
			if table.size(files) == 1 then
				os.rename(base_name .. "/" .. files[1], scripts.plugins_installer.plugin_directory .. plugin_name)
				scripts.installer.delete_dir(base_name)
			elseif table.size(files) > 1 then
				os.rename(scripts.plugins_installer.plugin_directory .. plugin_name,
					scripts.plugins_installer.plugin_directory .. plugin_name .. "_todelete")
				os.rename(base_name, scripts.plugins_installer.plugin_directory .. plugin_name)
				scripts.installer.delete_dir(scripts.plugins_installer.plugin_directory .. plugin_name .. "_todelete")
			end
		end
		scripts:print_log("Plugin " .. plugin_name .. " rozpakowany")
		load_plugin(plugin_name)
		os.remove(self.plugin_zip)
	elseif event == "sysUnzipError" then
		scripts:print_log("Blad podczas rozpakowywania pluginu")
	end
end

Msmudlet.updater:init()
