---@diagnostic disable: redundant-parameter
Msmudlet.obexp = Msmudlet.obexp or {
  triggery = {},
  aliasy = {},
  handler = nil
}


function Msmudlet.obexp:init()
  if self.aliasy.obexp then killAlias(self.aliasy.obexp) end
  self.aliasy.obexp = tempAlias("^/obexp$", function() Msmudlet.obexp:ogladaj() end)
end

function Msmudlet.obexp:ogladaj()
  -- triggery zbiorcze
  local dom = tempRegexTrigger(
    " (drzwi|okienk[oa]|krok(iew|wie|wia)|law(a|ami|e|y)|siedzisk[oa]|stol(|y|em|owi|ach)|loz[ek](e|[oa]|iem|ami|ach)|firan)[ \\.,]",
    function() Msmudlet.autko:kolejkuj("obexp", "normal", function() Msmudlet.obexp:ob_dom() end, 100, true) end, 1)
  table.insert(self.triggery, dom)
  local dom2 = tempRegexTrigger(
    " (posadzk|schod(ki|y)|sklepienie|podlog[aie]|dywan|krzes(lo|la|el|lami)|scian|wierzej|porecz|laweczk|piec|stolik|plot)",
    function() Msmudlet.autko:kolejkuj("obexp", "normal", function() Msmudlet.obexp:ob_dom2() end, 100, true) end, 1)
  table.insert(self.triggery, dom2)
  local uslugi = tempRegexTrigger(
    " (szyld(|y|em)|dzban|kontuar|stojak|pol(ki|ce|kach|kami|eczki)|miecz|ryb[ae]|talerz|regal|gablot(|a|y|e)|biur(ek|ko|kami)|lad[aey])",
    function() Msmudlet.autko:kolejkuj("obexp", "normal", function() Msmudlet.obexp:ob_uslugi() end, 100, true) end, 1)
  table.insert(self.triggery, uslugi)
  local uslugi2 = tempRegexTrigger(" (stragan|kadz(e|i|ach|ami)|ryb)",
    function() Msmudlet.autko:kolejkuj("obexp", "normal", function() Msmudlet.obexp:ob_uslugi2() end, 100, true) end, 1)
  table.insert(self.triggery, uslugi2)
  local miasto = tempRegexTrigger(
    " (stajni|bruk|budyn(ek|ki|kiem|kowi)|kamienic|rynsztok|ulic|baszt|dom|chodnik|wiez[aey]|dach(|y|em|owi)|slup(|y|em)|studni|zabudowania|magazyn|skrzyni|beczk|okn[oa])",
    function() Msmudlet.autko:kolejkuj("obexp", "normal", function() Msmudlet.obexp:ob_miasto() end, 100, true) end, 1)
  table.insert(self.triggery, miasto)
  local trakt = tempRegexTrigger(
    " (bram(|y|a)|mur(|y|ach|ami)|pomost|rzek[ai]|jezior|jar|pol[ae]|chat(ek|ki|a|e)|[Tt]rakt|[Dd]rog[aei])",
    function() Msmudlet.autko:kolejkuj("obexp", "normal", function() Msmudlet.obexp:ob_trakt() end, 100, true) end, 1)
  table.insert(self.triggery, trakt)
  local artystyczne = tempRegexTrigger(
    " (posag|malowid(l[oa]|el|ami)|ozdob[ay]|rzezb[aey]|plaskorzezb|witraz|bizuteri[ai]|obelisk|fresk|mozaik[eia]|postument|herb|fontann)",
    function() Msmudlet.autko:kolejkuj("obexp", "normal", function() Msmudlet.obexp:ob_artystyczne() end, 100, true) end, 1)
  table.insert(self.triggery, artystyczne)
  local swiatla = tempRegexTrigger(" (kaganek|pochodni[ae]|swiec[ae]|kandelabr|zyrandol|latarni)",
    function() Msmudlet.autko:kolejkuj("obexp", "normal", function() Msmudlet.obexp:ob_swiatla() end, 100, true) end, 1)
  table.insert(self.triggery, swiatla)
  local mechanika = tempRegexTrigger(
    " (dzwigni(|e|a)|brona|klap[ae]|zawias|lancuch|falochron|uchwyt|galka|korba|korbka|lin[ay]|hak(|i))",
    function() Msmudlet.autko:kolejkuj("obexp", "normal", function() Msmudlet.obexp:ob_mechanike() end, 100, true) end, 1)
  table.insert(self.triggery, mechanika)
  local stoiska = tempRegexTrigger(
    " (bud(ka|ek|ami)|namiot(|y|ami|ow)|kram(|y|ami|ow)|kosz|donic[ae](|mi)|bukiet(|ami|y|ow)|kwiat)",
    function() Msmudlet.autko:kolejkuj("obexp", "normal", function() Msmudlet.obexp:ob_stoiska() end, 100, true) end, 1)
  table.insert(self.triggery, stoiska)
  local las = tempRegexTrigger(" (pniak|drzew|roslin|szyszk|chwast)",
    function() Msmudlet.autko:kolejkuj("obexp", "normal", function() Msmudlet.obexp:ob_las() end, 100, true) end, 1)
  table.insert(self.triggery, las)
  local tablice = tempRegexTrigger(" (tablic|list)",
    function() Msmudlet.autko:kolejkuj("obexp", "normal", function() Msmudlet.obexp:ob_tablice() end, 100, true) end, 1)
  table.insert(self.triggery, tablice)
  local nieumarli = tempRegexTrigger(" (kurhan|grobowiec|sarkofag)",
    function() Msmudlet.autko:kolejkuj("obexp", "normal", function() Msmudlet.obexp:ob_nieumarli() end, 100, true) end, 1)
  table.insert(self.triggery, nieumarli)
  local architektoniczne = tempRegexTrigger(" (strop(|ie|em)|filar)",
    function()
      Msmudlet.autko:kolejkuj("obexp", "normal", function() Msmudlet.obexp:ob_architektoniczne() end, 100, true) end, 1)
  table.insert(self.triggery, architektoniczne)

  local gaguj = tempRegexTrigger(".*Nie zauwazasz niczego takiego\\.", function() deleteLine() end)
  table.insert(self.triggery, gaguj)


  send("spojrz")

  -- handler do czyszczenia triggerów, po zmianie lokacji
  self.handler = scripts.event_register:force_register_event_handler(self.handler, "amapNewLocation",
    function(event, loc)
      Msmudlet.obexp:czysc()
    end)
  -- timer do czyszczenia triggerów, po 5s
  tempTimer(5, function() Msmudlet.obexp:czysc() end)
end

function Msmudlet.obexp:ob_dom()
  sendAll("ob drzwi", "ob okienko", "ob okienka", "ob krokiew", "ob krokwie", "ob lawe", "ob lawy", "ob siedzisko",
    "ob siedziska", "ob stol", "ob stoly", "ob lozko", "ob lozka", "ob koc", "ob koce", "ob firany")
end

function Msmudlet.obexp:ob_dom2()
  sendAll("ob schodki", "ob schody", "ob sklepienie", "ob podloge", "ob dywan", "ob dywany", "ob krzeslo", "ob krzesla",
    "ob sciany", "ob wierzeje", "ob porecz", "ob porecze", "ob laweczke", "ob piec")
  sendAll("ob stolik", "ob plot", "ob plotek", "ob posadzke")
end

function Msmudlet.obexp:ob_uslugi()
  sendAll("ob brone", "ob dzban", "ob dzbany", "ob kontuar", "ob polke", "ob polki", "ob poleczki", "ob stojak",
    "ob stojaki", "ob szyld", "ob szyldy")
  sendAll("ob miecz", "ob rybe", "ob talerz", "ob talerze", "ob regal", "ob regaly", "ob gablote", "ob gabloty",
    "ob biurko", "ob biurka", "ob lade")
end

function Msmudlet.obexp:ob_uslugi2()
  sendAll("ob stragan", "ob stragany", "ob kadz", "ob kadzie", "ob rybe", "ob ryby")
end

function Msmudlet.obexp:ob_tablice()
  sendAll("ob tablice", "ob tabliczke", "ob liste", "ob list")
end

function Msmudlet.obexp:ob_miasto()
  sendAll("ob stajnie", "ob bruk", "ob budynek", "ob budynki", "ob kamienice", "ob rynsztok", "ob rynsztoki",
    "ob ulice", "ob baszte", "ob dom", "ob domy", "ob chodnik", "ob chodniki", "ob wieze", "ob dach", "ob dachy")
  sendAll("ob slup", "ob slupy", "ob studnie", "ob zabudowania", "ob magazyn", "ob magazyny", "ob skrzynie",
    "ob beczke", "ob beczki", "ob okno", "ob okna")
end

function Msmudlet.obexp:ob_trakt()
  sendAll("ob brame", "ob bramy", "ob mur", "ob mury", "ob pomost", "ob rzeke", "ob jezioro", "ob jeziorko", "ob jar",
    "ob pole", "ob pola", "ob chate", "ob chatke", "ob chaty", "ob chatki", "ob droge", "ob trakt", "ob krzaki")
end

function Msmudlet.obexp:ob_artystyczne()
  sendAll("ob malowidlo", "ob malowidla", "ob ozdobe", "ob ozdoby", "ob rzezbe", "ob rzezby", "ob plaskorzezbe",
    "ob plaskorzezby", "ob witraz", "ob witraze", "ob bizuterie", "ob obelisk", "ob obeliski", "ob fresk",
    "ob freski")
  sendAll("ob mozaike", "ob mozaiki", "ob posag", "ob posagi", "ob postument", "ob postumenty", "ob herb", "ob herby",
    "ob fontanne")
end

function Msmudlet.obexp:ob_swiatla()
  sendAll("ob kaganek", "ob kaganki", "ob kandelabr", "ob kandelabry", "ob pochodnie", "ob swiece", "ob zyrandol",
    "ob latarnie")
end

function Msmudlet.obexp:ob_stoiska()
  sendAll("ob budke", "ob budki", "ob namiot", "ob namioty", "ob kram", "ob kramy", "ob kosz", "ob kosze", "ob donice",
    "ob bukiet", "ob bukiety", "ob kwiat", "ob kwiaty")
end

function Msmudlet.obexp:ob_mechanike()
  sendAll("ob brone", "ob dzwignie", "ob klape", "ob zawias", "ob lancuch", "ob lancuchy", "ob falochron", "ob uchwyt",
    "ob galke", "ob korbe", "ob korbke", "ob liny", "ob hak", "ob haki")
end

function Msmudlet.obexp:ob_las()
  sendAll("ob pniak", "ob pniaki", "ob drzewo", "ob drzewa", "ob rosline", "ob rosliny", "ob szyszki", "ob chwasty")
end

function Msmudlet.obexp:ob_nieumarli()
  sendAll("ob kurhan", "ob grobowiec", "ob sarkofag", "ob sarkofagi")
end

function Msmudlet.obexp:ob_architektoniczne()
  sendAll("ob filar", "ob filary", "ob strop")
end

function Msmudlet.obexp:czysc()
  -- czysc triggery i event handlery
  if self.handler then
    scripts.event_register:kill_event_handler(self.handler)
  end
  self.handler = nil

  if self.triggery then
    for i = 1, #self.triggery do
      killTrigger(self.triggery[i])
    end
  end
  self.triggery = {}
end

Msmudlet.obexp:init()
