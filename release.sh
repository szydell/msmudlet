#!/usr/bin/env bash

readonly VERSION="2.1"


_tmp=$(git tag --list v"$VERSION".'*' | sort -V | tail -1)
release=${_tmp##*.}

if [ "z$release" == "z" ]; then
        release=1
else
        if ! [[ "$release" =~ ^[0-9]+$ ]]; then
                echo "Release should be a number"
                exit 2
        fi
        # increment release number
        ((release++))
fi

echo "Release: v${VERSION}.${release}"
sed -i "s#Msmudlet.version = .*#Msmudlet.version = \"v${VERSION}.${release}\"#" version.lua
git add version.lua
git commit -m"bump version to v${VERSION}.${release}"


git tag -a "v${VERSION}.${release}" -m"*$(git log "$(git describe --tags --abbrev=0)"..HEAD --pretty=format:"  - %s (@%cn)" | grep -v "bump version to" | grep -v "Merge branch" | grep -v "See merge request" | sed '/^[[:space:]]*$/d' | sort -h | uniq)" && git push && git push --tags
